### Working with the data
Example usage of the IO class also available in [example_scripts/IO_usage](../analysis_julien/example_scripts/IO_usage.ipynb)

```python
from analysis_julien import IO
datasetpath = 'C:/tmp'  # path where you downloaded the dataset
session = IO(path=datasetpath)

# display available data
session.listsessions()

# load first session (in place) 
# - by int
session.load_session(0)

# - or by name
session.load_session('rat-16-181012')

# display available methods
session?

# and a description
session.describe()
```