"""
Generate an overview of spikefield quality metrics

02/10/20
"""

from analysis_julien import IO
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_cfg
import pandas as pd
import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots
from plotly.colors import sequential as seq
import analysis_julien.core.get_clean_lfps as gcl
import analysis_julien.Thijs.tools as tools


resultdir = cfg.resultdir_quality
if not resultdir.is_dir():
    resultdir.mkdir(parents=True)


io = IO(path=cfg.datadir)
lfpframe_all = tools.load_data('lfpframe_all')
if lfpframe_all is None:
    lfpframe_all = pd.DataFrame()
    progbar = tools.PBar(text=f'getting lfpmeta', n_ticks=len(io.listsessions()), n_counts=len(io.listsessions()))
    for sid in io.listsessions():

        io.load_session(sid)
        loaded_lfpframe = io.LFPFrame
        for i, r in loaded_lfpframe.iterrows():
            if r.tetrode in io.UnitsFrame.tetrode.values:
                uids = io.selectUnits(tetrode=r.tetrode)
                loaded_lfpframe.loc[i, 'n_units'] = len(uids)
            else:
                loaded_lfpframe.loc[i, 'n_units'] = 0

        lfpframe_all = pd.concat([lfpframe_all, io.LFPFrame])
        progbar.print_1()
    progbar.close()
    for i, r in lfpframe_all.iterrows():
        lfpframe_all.at[i, 'sid'] = i.split('_')[1]

    lfpframe_all = gcl.get_clean_lfp_ids(lfpframe_all)
    tools.save_data(data=lfpframe_all, name='lfpframe_all')

red = seq.Reds[-4]
green = seq.Greens[-4]
lfpframe_all['clr'] = green
lfpframe_all.loc[lfpframe_all.is_noise, 'clr'] = red


fig = make_subplots(rows=2, cols=2,
                    subplot_titles=[
                        f'amp (thr: {lfp_cfg.max_amplitude}) vs amp 50hz (thr: {lfp_cfg.max_50hz})',
                        f'deviation (thr: {lfp_cfg.max_deviation}) vs amp 50hz (thr: {lfp_cfg.max_50hz})',
                        f'variance (thr: -) vs amp 50hz (thr: {lfp_cfg.max_50hz})',
                        f'nr units (thr: -) vs amp 50hz (thr: {lfp_cfg.max_50hz})',
                    ])

for sid in lfpframe_all.sid.unique():
    lfpframe_loop = lfpframe_all.loc[lfpframe_all.sid == sid]
    fig.add_trace(
        go.Scatter(
            mode='markers',
            hovertext=lfpframe_loop.index,
            x=np.log(lfpframe_loop.amplitude_50hz),
            # y=np.log(df.amplitude_50hz),
            y=lfpframe_loop.amplitude,
            marker=dict(color=lfpframe_loop.clr),
            legendgroup=sid,
            name=f'{sid}'
        ), row=1, col=1,
    )
    fig.update_xaxes(title_text='amp 50hz', row=1, col=1)
    fig.update_yaxes(title_text='amp', row=1, col=1)

    fig.add_trace(
        go.Scatter(
            mode='markers',
            x=np.log(lfpframe_loop.amplitude_50hz),
            y=lfpframe_loop.deviation,
            marker=dict(color=lfpframe_loop.clr),
            legendgroup=sid,
            name=f'{sid}',
        ), row=1, col=2)
    fig.update_xaxes(title_text='amp 50hz', row=1, col=2)
    fig.update_yaxes(title_text='dev', row=1, col=2)

    fig.add_trace(
        go.Scatter(
            mode='markers',
            x=np.log(lfpframe_loop.amplitude_50hz),
            y=lfpframe_loop.variance,
            marker=dict(color=lfpframe_loop.clr),
            legendgroup=sid,
            name=f'{sid}',
        ), row=2, col=1)
    fig.update_xaxes(title_text='amp 50hz', row=2, col=1)
    fig.update_yaxes(title_text='var', row=2, col=1)

    fig.add_trace(
        go.Scatter(
            mode='markers',
            x=np.log(lfpframe_loop.amplitude_50hz),
            y=lfpframe_loop.n_units,
            marker=dict(color=lfpframe_loop.clr),
            legendgroup=sid,
            name=f'{sid}',
        ), row=2, col=2)
    fig.update_xaxes(title_text='amp 50hz', row=2, col=2)
    fig.update_yaxes(title_text='# units', row=2, col=2)

fig.update_layout(title_text='Overview of spikefield quality metrics')
savename = (resultdir / 'lfp_quality_metrics.html').as_posix()
fig.write_html(savename)

# Zoom in for static picture
fig.update_yaxes(range=[0, 5], row=1, col=1)
fig.update_yaxes(range=[-100, 100], row=1, col=2)
fig.update_yaxes(range=[0, 10], row=2, col=1)
# fig.update_yaxes(range=[0, 5], row=1, col=1)
fig.update_layout(width=1200, height=1200)

savename = (resultdir / 'lfp_quality_metrics.png').as_posix()
# plotly.offline.plot(fig)
fig.write_image(savename)
