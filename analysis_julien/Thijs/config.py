import quantities as qt
import pathlib
from plotly.colors import diverging as div
from plotly.colors import sequential as seq
RESET_DUMP = False

datadir = pathlib.Path(r'C:\surfdrive\time2fire')  # path containing dataset
resultdir = datadir / 'results'  # save results
resultdir_quality = resultdir / 'quality'
dumpdir = datadir / 'dump'  # dump serialized data

unit_quality_thresholds = dict(
    snr=[2.5, '>='],
    isi_violation=[0.5, '<='],
    sq_isiv=[5, '<='],
    waveform_amplitude=[50, '>='],
    isolation_distance=[5, '>='],
    firing_rate=[0.1, '>=']
)


short_broad_spike_threshold = 0.5*qt.ms  # based on bimodal distribution in PTV
slow_fast_firing_threshold = 5*qt.Hz

unit_classification_thresholds = dict(
    excitatory=dict(
        # repolarization_slope=[1500000, '<='],
        peak_to_valley=[0.4*qt.ms, '>='],
        waveform_amplitude=[50, '>=']),
    inhibitory=dict(
        peak_to_valley=[0.4*qt.ms, '<'],
        # repolarization_slope=[1500000, '>'],
        waveform_amplitude=[50, '>='],
    )
)

# unit_classification_criteria = dict(
#     inhibitory=dict(
#         peak_to_valley=[0.4*qt.ms, '<'],    # based on bimodal distribution of ptv
#         repolarization_time=[0.1*qt.ms, '<'],
#         isi_peak_time=None,
#         CV_isi=[1, '>'],
#         LV=[1, '<'],
#         firing_rate=[5*qt.Hz, '>'],
#         fano_factor=[1, '>']
#     ),
# )

# Generated using:
# https://coolors.co/d1e3dd-f7934c-cc5803-651899-1f1300
object_colors = {
    1: '#CC5803',  # orange
    -1: '#651899',  # purple
}

correct_colors = {
    1: '#04724D',  # green,
    0: '#DB504A',  # red
}

modality_colors = dict(
    T='#1EAE5A',  # aqua marine
    V='#159CB7',  # blue
    # M='#D45113',  # portland orange
    M='#FDCA65',  # yellowgold
    P='#776472',  # old lavender
)

all_colors=dict(
    object=object_colors,
    modality=modality_colors,
    correct=correct_colors,
    gray='#b2b2b2'
)

epoch_colours = dict(
    baseline='#D4B483',
    sampling='#586994',
    response='#DD6031',
    reward='#2A9D8F'
)

celltype_colors = dict(
    inhibitory=div.balance[2],
    excitatory=div.balance[-2],
)

recording_groups = {
    'rat-16': dict(
        HPC=['PYR', 'CA1', 'CA2', 'CA3', 'HPC_DV'],
        PRH=['TEA', 'LEC', 'PR'],
        VIS=['V2'],
        BR=['Barrel', 'BR']
    )
}

area_colours = dict(
    PRH='#57467B',
    BR='#1EAE5A',
    VIS='#159CB7',
    HPC='#F5A65B ',
)
