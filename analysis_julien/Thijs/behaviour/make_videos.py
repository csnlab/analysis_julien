from analysis_julien import Io
from analysis_julien.Thijs import config as cfg
import numpy as np
import cv2
import analysis_julien.Thijs.tools as tools
from pathlib import Path

nr_frames_before_event = 10  # nr of frames to mark around event
nr_frames_before_event_touch = 5  # nr of frames to mark around event
n_vids_per_session = 5

# Define tracker signals to plot
signals_to_plot = [
    'nosetip',
]
segments = ['base', 'mid', 'far']
nrs = ['1', '2', '3']
for seg in segments:
    for nr in nrs:
        signals_to_plot.append(f'whisk_left_{nr}_{seg}')
        signals_to_plot.append(f'whisk_right_{nr}_{seg}')

datapath = Path(r'E:\hbp-data-002061')  # Path to read the video data from (full data disk)
assert datapath.is_dir()


def get_vidname(trial_id, vname):
    # Function to generate a videoname based on trial id
    # print(tid)
    animal = trial_id.split('_')[1]
    date = trial_id.split('_')[2]
    dflipped = f'{date[4:]}{date[2:4]}{date[:2]}'
    return datapath / 'data-raw' / animal / f'exp-{dflipped}' / 'video' / vname / 'ImgA.avi'


# Load data
# io = Io(path=cfg.datadir)
io = Io(path=r'E:\hbp-data-002061\dataset')
# sids = ['rat-26_190626', 'rat-26_190627', 'rat-26_190702']
for sid in io.session_ids:
    # if sid not in sids:
    #     continue
    # if 'rat-14' in sid or 'rat-16' in sid or 'rat-27' in sid:
    #     continue
    print(f'Starting session: {sid}')
    io.load_session(sid)

    # Select trials to plot data from
    rand_idx = np.random.randint(0, len(io.trial_ids), 2, dtype=np.int)
    tids = [io.trial_ids[ri] for ri in rand_idx]

    for tid in tids:
        trial_modality = io.get_meta(tid, 'modality')

        # Load a test signal for reference
        signal = io.get_object(f'{signals_to_plot[0]}_x')

        # Generate the filename to read, and to save
        sig_idx = np.where(signal.annotations['trial_id'] == tid)[0]
        vidname = np.unique(signal.annotations['video_names'][sig_idx])[0]
        full_vidname = get_vidname(tid, vidname)
        vidsavename = cfg.resultdir / 'retraction_videos' / f'{tid}.mp4'

        # Make sure output dir exists
        if not vidsavename.parent.is_dir():
            vidsavename.parent.mkdir(parents=True)

        # Open read and write videos
        cap = cv2.VideoCapture(full_vidname.as_posix())
        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        out = cv2.VideoWriter(vidsavename.as_posix(),
                              cv2.VideoWriter_fourcc(*'MP4V'),
                              60, (frame_width, frame_height))

        # Aesthetic thingy
        pbar = tools.PBar(f'writing vid for {tid}', n_ticks=20, n_counts=len(sig_idx),
                          n_decimals=4)

        # Retreive signals to plot
        signals = []
        for signame in signals_to_plot:
            signals.append(
                (io.get_object(f'{signame}_x'), io.get_object(f'{signame}_y'))
            )

        # Retreive events to plot: Retrction
        retr = io.get_event_times(tid, event_name='retraction')
        if len(retr) > 0:
            retraction_idx = np.argmin(np.abs(signal.times - retr[0]))
            retraction_idx = np.arange(-nr_frames_before_event, nr_frames_before_event) + retraction_idx
        else:
            retraction_idx = []
        retraction_clr = (0, 0, 255)

        # and touch
        touch = io.get_event_times(tid, event_name='touch')
        touch_idx = []

        for t in touch:
            idx_min = np.argmin(np.abs(signal.times - t)) + np.arange(-nr_frames_before_event_touch, nr_frames_before_event_touch)
            for i in idx_min:
                touch_idx.append(i)
        touch_clr = (0, 255, 0)

        light_on = io.get_event_times(tid, event_name='visual_start')
        vstart_idx = []
        for ll in light_on:
            idx_min = np.argmin(np.abs(signal.times - ll)) + np.arange(-nr_frames_before_event, nr_frames_before_event)
            for i in idx_min:
                vstart_idx.append(i)
        light_off = io.get_event_times(tid, event_name='visual_end')
        for ll in light_off:
            idx_min = np.argmin(np.abs(signal.times - ll)) + np.arange(-nr_frames_before_event, nr_frames_before_event)
            for i in idx_min:
                vstart_idx.append(i)
        light_clr = (0, 255, 127)

        # Loop over all frames in video
        for sig_i, idx in enumerate(sig_idx):

            # Read video frame
            vid_idx = signal.annotations['idx_in_video'][idx]
            cap.set(cv2.CAP_PROP_POS_FRAMES, vid_idx)
            ret, frame = cap.read()

            # Print trial id
            cv2.putText(frame, f'{tid}', (20, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 2)
            cv2.putText(frame, f'mod: {trial_modality}', (20, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 2)

            # Print retraction event
            if idx in retraction_idx:
                cv2.putText(
                    frame,
                    f'Retraction',
                    (20, 200),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    retraction_clr,
                    2,
                )

            # Print touch event
            if idx in touch_idx:
                cv2.putText(
                    frame,
                    f'Touch',
                    (20, 260),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    touch_clr,
                    2,
                )

            # Print light event
            if idx in vstart_idx:
                cv2.putText(
                    frame,
                    f'Light',
                    (20, 230),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    light_clr,
                    2
                )

            # Print progression bars in video
            # Setup
            pbartext = '|'
            retraction_bar = ''
            touch_bar = ''
            light_bar = ''
            pbarcol = (209, 80, 0, 255)

            # Generate progress bars
            for i in range(len(sig_idx)):
                if idx in retraction_idx:
                    pbarcol = (0, 0, 255)

                if sig_idx[i] in touch_idx and i <= sig_i:
                    touch_bar += '|'
                else:
                    touch_bar += ' '

                if sig_idx[i] in retraction_idx and i <= sig_i:
                    retraction_bar += '|'
                else:
                    retraction_bar += ' '

                if sig_idx[i] in vstart_idx and i <= sig_i:
                    light_bar += '|'
                else:
                    light_bar += ' '

                if i <= sig_i:
                    pbartext += 'x'
                else:
                    pbartext += ' '
            pbartext += '|'
            retraction_bar += ' '

            # Print progress bars
            for i in range(10):
                cv2.putText(frame, touch_bar, (20, 40 + i), cv2.FONT_HERSHEY_SIMPLEX, 0.03,
                            touch_clr, 1)

            for i in range(10):
                cv2.putText(frame, retraction_bar, (20, 50 + i), cv2.FONT_HERSHEY_SIMPLEX, 0.03,
                            retraction_clr, 1)

            for i in range(5):
                cv2.putText(frame, light_bar, (20, 60 + i), cv2.FONT_HERSHEY_SIMPLEX, 0.03,
                            light_clr, 1)

            for i in range(5):
                cv2.putText(frame, pbartext, (20, 70 + i), cv2.FONT_HERSHEY_SIMPLEX, 0.03,
                            pbarcol, 1)

            # Plot tracker signals
            for i, signame in enumerate(signals_to_plot):
                x = signals[i][0][idx]
                y = signals[i][1][idx]

                if not np.isnan(x) and not np.isnan(y):
                    if 'whisk' in signame:
                        clr = (180, 180, 0)
                    else:
                        clr = (255, 0, 0)
                    frame = cv2.circle(frame, (x, y), 5, color=clr, thickness=-1)

            # Write output and print progress
            out.write(frame)
            pbar.print_1()

        cap.release()
        out.release()


