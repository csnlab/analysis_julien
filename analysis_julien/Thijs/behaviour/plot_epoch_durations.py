"""
Plot behavioural epoch durations per session

13/10/20
"""
from analysis_julien import LightIo, cfg
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import numpy as np


durations = dict(
    Fulltrial=dict(start_event='trial_start', stop_event='trial_end'),
    Initiation=dict(start_event='trial_start', stop_event='sample_start'),
    Sampling=dict(start_event='sample_start', stop_event='retraction'),
    Choice=dict(start_event='retraction', stop_event='poke'),
    ITI=dict(start_event='poke', stop_event='trial_end'),
)

conditions = dict(
    modality=['T', 'V', 'M'],
    correct=[1, 0],
    object=[1, -1]
)

category_colors = cfg.all_colors

category_names = dict(
    modality={'T': 'Tactile', 'V': 'Visual', 'M': 'Multimodal'},
    correct={1: 'correct', 0: 'wrong'},
    object={1: 'obj A', -1: 'obj B'}
)

resultdir = cfg.resultdir / 'behaviour/epoch_durations'
if not resultdir.is_dir():
    resultdir.mkdir(parents=True)

io = LightIo(path=cfg.datadir)
x_space_cat = 0.1  # space between violinplots
all_epoch_durations = dict()

# General makeup
subplot_titles = []
for k in conditions.keys():
    subplot_titles.extend([k for _ in range(len(durations.keys()))])

# Make figure per session
for sid in io.session_ids:
    io.load_session(sid)

    fig = make_subplots(
        rows=3,
        cols=5,
        subplot_titles=subplot_titles,
        shared_xaxes=False,
        vertical_spacing=0.1,
    )

    # Plot per condition and duration (duration==epoch)
    for i_cond, condition in enumerate(conditions):

        # Vars for layout
        x = 0
        tickvals = []
        ticklabels = []
        condition_categories = conditions[condition]

        for i_duration, duration in enumerate(durations.keys()):

            start_event = durations[duration]['start_event']
            stop_event = durations[duration]['stop_event']
            if i_duration == 0:
                showlegend = True
            else:
                showlegend = False

            # Load the data based on epoch and condition info
            for i_cat, category in enumerate(condition_categories):
                x += x_space_cat
                trial_specs = {condition: category, 'events_of_interest': [start_event, stop_event]}
                trial_ids = io.select_trials(**trial_specs)
                t0 = io.get_event_times(trial_ids=trial_ids, event_name=start_event, verbose=False)
                t1 = io.get_event_times(trial_ids=trial_ids, event_name=stop_event, verbose=False)
                dT = (t1-t0).rescale('s').magnitude

                # Compute epoch duration
                if duration in ['Sampling', 'Initiation']:
                    max_dt = 10  # maximum epoch duration for plotting
                else:
                    max_dt = 60
                dT = dT[dT <= max_dt]

                # Add mean
                key = (condition, duration, category, io.animal_id)
                if key not in all_epoch_durations.keys():
                    all_epoch_durations[key] = []
                all_epoch_durations[key].append(np.mean(dT))

                # Plot epoch duration distribution
                fig.add_trace(go.Violin(
                    x=[x for _ in range(dT.shape[0])],
                    y=dT,
                    legendgroup=category,
                    name=category_names[condition][category],
                    line_color=category_colors[condition][category],
                    showlegend=showlegend,
                    meanline=dict(color='black'),
                    width=x_space_cat * 0.7,
                ), row=i_cond+1, col=i_duration+1)

            # Layout
            fig.update_xaxes(
                tickmode='array',
                tickvals=[],
                title_text=duration,
                row=i_cond+1, col=i_duration+1,
            )
            if i_duration == 0:
                fig.update_yaxes(
                    title_text='time (s)',
                    row=i_cond+1, col=i_duration+1,
                )

        # Layout
        fig.update_layout(
            width=1800,
            height=1200,
            title_text=f'Duration of trial epochs, per condition ({io.session_id})',
        )

    # Save figure
    savename = resultdir / f'{io.session_id}.png'
    fig.write_image(savename.as_posix())
    print(savename)


# Make plot with distribution of means across sessions
# General makeup
for aid in io.animal_ids:
    fig = make_subplots(
        rows=3,
        cols=5,
        subplot_titles=subplot_titles,
        shared_xaxes=False,
        vertical_spacing=0.1,
    )
    for i_cond, condition in enumerate(conditions):

        # Vars for layout
        x = 0
        tickvals = []
        ticklabels = []
        condition_categories = conditions[condition]

        for i_duration, duration in enumerate(durations.keys()):

            start_event = durations[duration]['start_event']
            stop_event = durations[duration]['stop_event']
            if i_duration == 0:
                showlegend = True
            else:
                showlegend = False

            # Load the data based on epoch and condition info
            for i_cat, category in enumerate(condition_categories):
                x += x_space_cat

                key = (condition, duration, category, aid)
                dT = all_epoch_durations[key]
                # Plot epoch duration distribution
                fig.add_trace(go.Violin(
                    x=[x for _ in range(len(dT))],
                    y=dT,
                    legendgroup=category,
                    name=category_names[condition][category],
                    line_color=category_colors[condition][category],
                    showlegend=showlegend,
                    meanline=dict(color='black'),
                    width=x_space_cat * 0.7,
                ), row=i_cond + 1, col=i_duration + 1)

            # Layout
            fig.update_xaxes(
                tickmode='array',
                tickvals=[],
                title_text=duration,
                row=i_cond + 1, col=i_duration + 1,
            )
            if i_duration == 0:
                fig.update_yaxes(
                    title_text='time (s)',
                    row=i_cond + 1, col=i_duration + 1,
                )

        # Layout
        fig.update_layout(
            width=1800,
            height=1200,
            title_text=f'Duration of trial epochs, over all sessions ({aid})',
        )

    # Save figure
    savename = resultdir / f'{aid}_all_sessions.png'
    fig.write_image(savename.as_posix())
