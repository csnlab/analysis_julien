#### Downloading dataset

**Within UvA network**

The dataset is hosted on the NAS in A4.28 (accesible only within the
UvA network)

To connect with SFTP (for instance using WinScp):
* hostname: 10.10.17.235
* User name: team2fire
* Password: fire2team
* port: 22