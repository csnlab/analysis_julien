from analysis_julien import Io
from analysis_julien.Thijs import config as cfg
import pandas as pd
import numpy as np
import analysis_julien.Thijs.tools as tools
import plotly.graph_objects as go

# TODO: sort lfps by recording group instead of area

# Function to get session id from unit id
def get_session(unit_id):
    sid_out = None
    for sessid in io.session_ids:
        if unit_id.split('_')[1][:6] in sessid:
            sid_out = sessid
    return sid_out

# load stp results
stp_data_file = cfg.datadir / 'results' / 'spike_triggered_phase' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
stp_df = pd.read_csv(stp_data_file, header=0, index_col=0)

# load unit metainfo in dataset
io = Io(path=cfg.datadir, read_lfp=True)  # global io
units_df_savename = cfg.resultdir / 'units_df.csv'
units_df = pd.read_csv(units_df_savename, header=0, index_col=0)
for i, r in units_df.iterrows():
    sid = get_session(i)
    units_df.at[i, 'session'] = sid
    units_df.at[i, 'animal'] = sid.split('_')[0]

units_df = units_df.loc[units_df.animal == 'rat-16']

# Map to offset columns during visualization
plot_map = dict(
    Barrel=0,
    Visual=8,
    Hpc=16,
    Peri=24,
)

plot_map_abbr = dict(
    BR=0,
    VIS=8,
    HPC=16,
    PRH=24,
)

# Matrix to save data in, will be plotted
all_kappa_significant = np.empty((len(units_df.index), 32))  # each unit x each channel
all_kappa = np.empty((len(units_df.index), 32))  # each unit x each channel

# Name where to save the output figure
savename = cfg.resultdir / 'spikefield' / 'matrix_theta'

# Setup parameters to handle data
pbar = tools.PBar(text='running', n_ticks=20, n_counts=len(units_df.index), n_decimals=1)  # shows progress
not_found = []  # keep track of what lfp/unit pairs are found in dataset
found = []

# Variables for visualization
row_idx = -1
area_rows = []
area_names = []
session_rows = []

last_area = None  # While scanning trough the dataframe, check if current area is different
                  # compared to previous area, and if not make sure there's going to be a line drawn in plot
last_session = None

# Read lfp ids per session here, so we don't have to load data in the mainloop
lfp_ids_per_session = dict()
rec_gr_per_lfp = dict()
for session in units_df.session.unique():
    io.load_session(session, read_lfp=True)
    lfp_ids_per_session[session] = io.lfp_clean_ids

    for lid in io.lfp_clean_ids:
        rec_gr_per_lfp = io.lfp_clean_df.loc[lid].recording_group

# Group the dataframe by 'tetrode area' and 'session', will return a dataframe per loop
# unit ids index, only within that area and session
for group_name, group in units_df.groupby(['recording_group', 'session']):

    # Check if we need to draw lines for area
    if group_name[0] != last_area:
        last_area = group_name[0]
        area_names.append(group_name[0])
        area_rows.append(row_idx)

    # Check if we need to draw lines between session
    if group_name[1] != last_session:
        last_session = group_name[1]
        session_rows.append(row_idx)

    # For all units in this area & session
    for uid in group.index:
        row_idx += 1  # each unit gets 1 row
        if not units_df.loc[uid].good_unit:  # the ugly ones are skipped
            pbar.print_1()
            continue

        # Load session (TODO: loop per session to prevent having to load the data every time)
        sid = get_session(uid)

        # Now get the data for each unit & lfp pair
        for lfp_id in lfp_ids_per_session[sid]:

            # Arrange the plotted data properly, sometimes not each lead is in the data
            tetrode = lfp_id.split('_')[1]
            area = tetrode[:-1]
            channr = tetrode[-1]

            # rec_area = rec_gr_per_lfp[lfp_id]
            # assert rec_area in plot_map_abbr.keys()
            assert area in plot_map.keys()
            assert int(channr) < 9  # check there's max 8 leads in the area

            col_idx = (plot_map[area] + int(channr))-1  # column to plot value

            # Check if data for this unit & lfp exists
            df_idx = f'{uid}_{lfp_id}'
            if df_idx not in stp_df.index:
                not_found.append(df_idx)
                continue

            # Only include 'significant' locking
            all_kappa[row_idx, col_idx] = stp_df.loc[df_idx].theta_sampling_k
            if stp_df.loc[df_idx].theta_sampling_p < 0.05:
                all_kappa_significant[row_idx, col_idx] = stp_df.loc[df_idx].theta_sampling_k
            else:
                all_kappa_significant[row_idx, col_idx] = 0
            found.append(df_idx)
        pbar.print_1()


pbar.close()
all_kappa_significant[all_kappa_significant > 1] = 0  # regard these values as noise
all_kappa_significant[~np.isfinite(all_kappa_significant)] = 0  # clean the data for plotting

# Summarize how many lfp & unit pairs were found
print(f'n found: {len(found)}')
print(f'n missing: {len(not_found)}')


# ---------------------- only significant kappa's -------------------------------
# Plot the data! make figure and plot the heatmap. Cap the values at zmax=0.5 for visualization
# purposes
fig = go.Figure()
fig.add_trace(go.Heatmap(z=all_kappa_significant, zmin=0, zmax=1))

# annotate separations between sessions
for i in session_rows[1:]:
    fig.add_trace(go.Scatter(
        x=[-1, 32],
        y=[i, i],
        mode='lines',
        line=dict(color='green', dash='dot', width=2),
        showlegend=False,
    ))

for i in area_rows[1:]:
    fig.add_trace(go.Scatter(
        x=[-1, 32],
        y=[i, i],
        mode='lines',
        line=dict(color='green', width=2),
        showlegend=False,
    ))

xticks = []
xtext = []
for key, item in plot_map.items():
    fig.add_trace(go.Scatter(
        x=[item-0.5, item-0.5],
        y=[0, all_kappa.shape[0]],
        mode='lines',
        line=dict(color='green', width=2),
        showlegend=False,
    ))
    xticks.append(item + 4)
    xtext.append(key)

yticks = []
for i in range(len(area_rows)-1):
    yticks.append(((area_rows[i+1] - area_rows[i])/2) + area_rows[i])
yticks.append(((all_kappa_significant.shape[0] - area_rows[-1])/2) + area_rows[-1])

fig.update_xaxes(range=[-0.5, 31.5],
                 tickmode='array',
                 tickvals=xticks,
                 ticktext=xtext)
fig.update_yaxes(range=[0, all_kappa_significant.shape[0]],
                 tickmode='array',
                 tickvals=yticks,
                 ticktext=area_names,
                 # scaleratio=1,
                 # scaleanchor='x',
                 )

fig.update_layout(
    width=1500,
    height=1500,
)
# plotly.offline.plot(fig)
fig.write_html(savename.as_posix() + '_masked_significance.html')
fig.write_image(savename.as_posix() + '_masked_significance.png')

# ---------------------- all kappa's -------------------------------
# Plot the data! make figure and plot the heatmap. Cap the values at zmax=0.5 for visualization
# purposes
fig = go.Figure()
fig.add_trace(go.Heatmap(z=all_kappa, zmin=0, zmax=1))

# annotate separations between sessions
for i in session_rows[1:]:
    fig.add_trace(go.Scatter(
        x=[-1, 32],
        y=[i, i],
        mode='lines',
        line=dict(color='green', dash='dot', width=2),
        showlegend=False,
    ))

for i in area_rows[1:]:
    fig.add_trace(go.Scatter(
        x=[-1, 32],
        y=[i, i],
        mode='lines',
        line=dict(color='green', width=2),
        showlegend=False,
    ))

xticks = []
xtext = []
for key, item in plot_map.items():
    fig.add_trace(go.Scatter(
        x=[item-0.5, item-0.5],
        y=[0, all_kappa.shape[0]],
        mode='lines',
        line=dict(color='green', width=2),
        showlegend=False,
    ))
    xticks.append(item + 4)
    xtext.append(key)

yticks = []
for i in range(len(area_rows)-1):
    yticks.append(((area_rows[i+1] - area_rows[i])/2) + area_rows[i])
yticks.append(((all_kappa_significant.shape[0] - area_rows[-1])/2) + area_rows[-1])

fig.update_xaxes(range=[-0.5, 31.5],
                 tickmode='array',
                 tickvals=xticks,
                 ticktext=xtext)
fig.update_yaxes(range=[0, all_kappa_significant.shape[0]],
                 tickmode='array',
                 tickvals=yticks,
                 ticktext=area_names,
                 # scaleratio=1,
                 # scaleanchor='x',
                 )

fig.update_layout(
    width=1500,
    height=1500,
)
# plotly.offline.plot(fig)
fig.write_html(savename.as_posix() + '.html')
fig.write_image(savename.as_posix() + '.png')

# all_kappa_locking_only = []
# for i in range(all_kappa.shape[0]):
#     if np.any(all_kappa[i, :] > 0):
#         all_kappa_locking_only.append(all_kappa[i, :])
# all_kappa_locking_only = np.vstack(all_kappa_locking_only)
# print(all_kappa_locking_only.shape)