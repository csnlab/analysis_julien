import elephant
from analysis_julien import LightIo
import analysis_julien.Thijs.config as cfg
import numpy as np
import time
import quantities as qt
import mne

io = LightIo(path=cfg.datadir, read_lfp=True)
io.load_session(0)
sp = [io.get_object(io.unit_ids[0]), io.get_object(io.unit_ids[1])]
lfp = io.get_object(io.lfp_ids[0])
fmin = 1
fmax = 120
fstep = 4

# freqs, psd = elephant.spectral.welch_psd(lfp.time_slice(0*qt.s, 1500*qt.ms), frequency_resolution=5*qt.Hz)
psd_mt, f_mt = mne.time_frequency.psd_array_multitaper(
    lfp.magnitude.flatten(), (1/lfp.sampling_period).magnitude,
    verbose=True
)