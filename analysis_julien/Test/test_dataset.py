"""
The IO structure is displayed in docs/io_layoutmap

Thijs 08/12/20
"""

from analysis_julien import Io
from analysis_julien.Thijs import config as cfg
import unittest
import neo
import pandas as pd

# --------------------------- DATA REQUIREMENTS --------------------------------------

# Definitions the data should meet

io = Io(path=cfg.datadir)
# io = Io(path=r'E:\hbp-data-002061\dataset')

# io structure (see io_layout map)
# All required io attributes
io_attributes = (
    'data_version',
    'dataset_path',
    'animal_id',
    'session_ids',
    'unit_ids',
    'trial_ids',
    'lfp_ids',
    'lfp_clean_ids',
)

# All available IO methods
io_methods = (
    'load_session',
    'select_lfps',
    'select_trials',
    'select_units',
    'set_unit_criteria',
    'get_aligned_times',
    'get_meta',
    'get_event_times',
    'get_object'
)

# All required IO types
io_data = (
    ('block', neo.Block),
    ('lfp_clean_df', pd.DataFrame),
    ('lfp_df', pd.DataFrame),
    ('trial_df', pd.DataFrame),
    ('unit_df', pd.DataFrame)
)


# List of all required irregularly sampeled signals
segment_asigs = ()

segment_irrsigs = ['dlc_L-raw', 'dlc_R-raw']

snoutparts = ['leftnose', 'rightnose', 'nosetip']
for part in snoutparts:
    segment_irrsigs.append(f'{part}_x-raw')
    segment_irrsigs.append(f'{part}_y-raw')
    segment_irrsigs.append(f'{part}_likelihood-raw')

sides = ['left', 'right']
w_nr = ['1', '2', '3']
segments = ['base', 'mid', 'far']
for side in sides:
    for w in w_nr:
        for segment in segments:
            segment_irrsigs.append(f'whisk_{side}_{w}_{segment}_x-raw')
            segment_irrsigs.append(f'whisk_{side}_{w}_{segment}_y-raw')
            segment_irrsigs.append(f'whisk_{side}_{w}_{segment}_likelihood-raw')
            segment_irrsigs.append(f'whisk_{side}_{w}_{segment}_x-raw')
            segment_irrsigs.append(f'whisk_{side}_{w}_{segment}_y-raw')

# List all required events
segment_events = (
    'visual_start',
    'visual_end',
    'tactile_start',
    'tactile_end',
    'touch_L',
    'touch_R',
    'touch',
    'left_poke',
    'right_poke',
    'poke',
    'retraction',
    'sample_start',
    'video_trigger',
    'trial_end',
    'trial_start',
    'trial_meta'
)

# List all required spiketrain annotations
required_sp_annotations = (
    'cluster_id', 'cluster_group', 'tetrode', 'unitid',
    'id_in_kwik',
    'num_spikes', 'firing_rate', 'presence_ratio',
    'isi_violation', 'amplitude_cutoff', 'snr', 'max_drift',
    'cumulative_drift', 'isolation_distance',  # 'silhouette_score',
    'peak_to_valley', 'halfwidth', 'peak_trough_ratio',
    'repolarization_slope', 'recovery_slope',
    'l_ratio', 'd_prime', 'nn_hit_rate', 'nn_miss_rate',
    'waveform_mean', 'waveform_std',
    'x', 'y', 'z', 'area', 'full_tet_name',
    'celltype', 'recording_group',
    'sq_iso_dist', 'sq_cr', 'sq_isigood', 'good_unit'
)
available_sessions = io.session_ids
# available_sessions = [s for s in io.session_ids if 'rat-16' in s]
# available_sessions = ['rat-16_181013']


# --------------------------------------- MAGIC ZONE -------------------------------------------

class TestStructure(unittest.TestCase):
    longMessage = False

    def test_structure(self):
        for sid in available_sessions:
            with self.subTest(sid=sid):
                io.load_session(sid, read_lfp=False)

                # io structure
                for attr in io_attributes:
                    self.assertIn(attr, dir(io))

                for method in io_methods:
                    self.assertIn(method, dir(io))

                for data, dtype in io_data:
                    self.assertIn(data, dir(io))
                    self.assertIsInstance(getattr(io, data), dtype)

                # neo structure
                block = io.block
                self.assertEqual(len(block.segments), 1)
                seg = io.block.segments[0]
                irr_sig_names = [s.name for s in seg.irregularlysampledsignals]

                for signame in segment_irrsigs:
                    self.assertIn(signame, irr_sig_names, f'{signame}')

                # Check if all required events are there
                ev_names = [ev.name for ev in seg.events]
                for evname in segment_events:
                    self.assertIn(evname, ev_names)

                # Check if all required spiketrain annotations are there
                spiketrains = block.segments[0].spiketrains
                for sp in spiketrains:
                    with self.subTest(sp=sp.name):
                        for k in required_sp_annotations:
                            with self.subTest(ann=k):
                                self.assertIn(k, sp.annotations.keys())