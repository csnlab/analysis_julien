import quantities as qt
nbins = 20
min_phase_amplitude = 10  # only included phases which amplitude higher than this
stp_max_p = 0.01
stp_min_spikes = 40  # dont include epochs wth less han n spikes
stp_max_k = 20  # dont include distributions with kappa larger than 20
stp_min_k = 0.05  # dont include units with lower kappa

# Quality metrics
max_50hz = 13.5  # Maximum amplitude of log(50Hz) noise
max_deviation = 50  # Maximum deviation of signal distribution compared to other leads on tetrode
max_amplitude = 0.5  # Maximum difference of amplitude between leads
max_variance = 2

cluster_datadir = r'/data/truikes/Data_Julien'
cluster_resultdir = cluster_datadir + '/spikefield'
cluster_pickle_lfp = cluster_resultdir + '/pickle_stp1/lfp'
cluster_pickle_stp1 = cluster_resultdir + '/pickle_stp1/stp'
cluster_pickle_stp2 = cluster_resultdir + '/pickle_stp2'
cluster_csv_ppc = cluster_resultdir + '/csv_ppc'
cluster_csv_ppc_all = cluster_resultdir + '/csv_ppc_all'

bands_def = dict(
    delta=dict(lowcut=1, highcut=4),
    theta=dict(lowcut=4, highcut=10),
    alpha=dict(lowcut=9, highcut=14),
    beta=dict(lowcut=13, highcut=30),
    low_gamma=dict(lowcut=30, highcut=70),
    high_gamma=dict(lowcut=70, highcut=120)
)

epochs_def = dict(
    baseline=dict(time_before=1.5 * qt.s, time_after=-1 * qt.s, event='trial_start'),
    sampling=dict(time_before=0 * qt.s, time_after=0.5 * qt.s, event='sample_start'),
    # response=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='retraction'),
    # reward=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='poke'),
)

modalities_def = dict(
    T=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='sample_start'),
    V=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='sample_start'),
    M=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='sample_start'),
    P=dict(time_before=0 * qt.s, time_after=1 * qt.s, event='trial_start')
)