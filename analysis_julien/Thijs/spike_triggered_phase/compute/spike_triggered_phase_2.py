"""
STP processing pipeline:
First, on the cluster, run:
* spike_triggered_phase_1 (using csn_spike_triggered_phase_1.slurm)
* spike_triggered_phase_2 (using csn_spike_triggered_phase_2.slurm)

Then run the plot function (locally):
* plot_spike_triggered_phase

executed with ~/Thijs/slurm/csn_spike_triggered_phase_2.slurm
22/10/20
dataset: V3_WIP
"""

import astropy.stats as asstat
import pandas as pd
import pickle
from pathlib import Path
from analysis_julien.Thijs.tools import find_times_idx, find_array_idx
import numpy as np
import quantities as qt
from analysis_julien import LightIo
from distributed import Client
from dask_jobqueue import SLURMCluster
import os
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_config
from scipy.stats import vonmises
from dask.distributed import fire_and_forget
import time
# Path params
cluster_scaling = 100  # nr of parallel jobs on cluster
picklesavedir = lfp_config.cluster_pickle_stp2
stp1_pickledir = lfp_config.cluster_pickle_stp1  # Path containing output from spike triggered phase 1

if not os.path.isdir(picklesavedir):
    os.makedirs(picklesavedir)

nbins = lfp_config.nbins
epochs_def = lfp_config.epochs_def
modalities_def = lfp_config.modalities_def
bands_def = lfp_config.bands_def

# Setup data input/output
overwrite = False
min_phase_amplitude = lfp_config.min_phase_amplitude


def get_spc_by_epochs(input_data_filename, unit_id, lfp_id, lfp_state, epoch_times, o=False):
    """

    Parameters
    ----------
    d = data
    s = session
    n = savename

    Returns
    -------
    an array per lfp band, per epoch, containing all phases
    and amplitudes
    """

    dump_savename = f'{picklesavedir}/{unit_id}-{lfp_id}-{lfp_state}_epoch'

    if not os.path.isfile(dump_savename) or o:
        with open(input_data_filename, 'rb') as f:
            data = pickle.load(f)
        lbands = list(data['bands'].keys())

        spike_phase_coupling = dict(input_data_filename=input_data_filename, unit_id=unit_id,
                                    lfp_id=lfp_id, lfp_state=lfp_state)
        for lfp_band in lbands:
            for epoch in epoch_times.keys():
                start_times = epoch_times[epoch]['start']
                stop_times = epoch_times[epoch]['stop']

                times = data[f'times_{lfp_band}']
                phases = data[f'phases_{lfp_band}']
                amps = data[f'amps_{lfp_band}']

                prefix = f'{lfp_band}_{epoch}'

                spike_phase_coupling[f'{prefix}_phases'] = np.empty(0)
                spike_phase_coupling[f'{prefix}_amps'] = np.empty(0)
                spike_phase_coupling[f'{prefix}_times'] = np.empty(0)

                for e_start, e_stop in zip(start_times, stop_times):
                    # Find spike times this trial
                    idx = find_times_idx(times * qt.s, e_start, e_stop)

                    spike_phase_coupling[f'{prefix}_phases'] = np.append(
                        spike_phase_coupling[f'{prefix}_phases'],
                        phases[idx])
                    spike_phase_coupling[f'{prefix}_amps'] = np.append(
                        spike_phase_coupling[f'{prefix}_amps'],
                        amps[idx])
                    spike_phase_coupling[f'{prefix}_times'] = np.append(
                        spike_phase_coupling[f'{prefix}_times'],
                        times[idx]
                    )

                bc, ph = bin_phases(
                    spike_phase_coupling[f'{prefix}_phases'],
                    spike_phase_coupling[f'{prefix}_amps'],
                    nb=nbins,
                )
                results = get_circular_stats(spike_phase_coupling[f'{prefix}_phases'])
                spike_phase_coupling[f'{prefix}_phases_binned'] = ph

                for k in results.keys():
                    spike_phase_coupling[f'{prefix}_{k}'] = results[k]
        with open(dump_savename, 'wb') as f:
            pickle.dump(spike_phase_coupling, f)


def get_spc_by_modality(input_data_filename, unit_id, lfp_id, lfp_state, modality_times, o=False):
    """

    Parameters
    ----------
    d = data
    s = session

    Returns
    -------

    """
    dump_savename = f'{picklesavedir}/{unit_id}-{lfp_id}-{lfp_state}_modality.pkl'

    if not os.path.isfile(dump_savename) or o:
        with open(input_data_filename, 'rb') as f:
            data = pickle.load(f)
        lbands = list(data['bands'].keys())

        # # Trial selection
        # events_of_interest = [modalities_def[mod]['event'] for mod in modalities_def.keys()]

        spike_phase_coupling = dict(input_data_filename=input_data_filename,
                                    unit_id=unit_id, lfp_id=lfp_id, lfp_state=lfp_state)
        for lfp_band in lbands:
            for modality in modalities_def.keys():
                start_times = modality_times[modality]['start']
                stop_times = modality_times[modality]['stop']

                times = data[f'times_{lfp_band}']
                phases = data[f'phases_{lfp_band}']
                amps = data[f'amps_{lfp_band}']

                prefix = f'{lfp_band}_{modality}'

                spike_phase_coupling[f'{prefix}_phases'] = np.empty(0)
                spike_phase_coupling[f'{prefix}_amps'] = np.empty(0)
                spike_phase_coupling[f'{prefix}_times'] = np.empty(0)

                for e_start, e_stop in zip(start_times, stop_times):
                    # Find spike times this trial
                    idx = find_times_idx(times * qt.s, e_start, e_stop)
                    spike_phase_coupling[f'{prefix}_phases'] = np.append(
                        spike_phase_coupling[f'{prefix}_phases'],
                        phases[idx])
                    spike_phase_coupling[f'{prefix}_amps'] = np.append(
                        spike_phase_coupling[f'{prefix}_amps'],
                        amps[idx])
                    spike_phase_coupling[f'{prefix}_times'] = np.append(
                        spike_phase_coupling[f'{prefix}_times'],
                        times[idx],
                    )

                bc, ph = bin_phases(
                    spike_phase_coupling[f'{prefix}_phases'],
                    spike_phase_coupling[f'{prefix}_amps'],
                    nb=nbins,
                )
                results = get_circular_stats(spike_phase_coupling[f'{prefix}_phases'])
                spike_phase_coupling[f'{prefix}_phases_binned'] = ph

                for k in results.keys():
                    spike_phase_coupling[f'{prefix}_{k}'] = results[k]

        with open(dump_savename, 'wb') as f:
            pickle.dump(spike_phase_coupling, f)


def bin_phases(phases, amps, nb=25):
    counts_per_bin = np.empty(nb)
    bin_edges = np.linspace(-np.pi, np.pi, num=nb + 1, endpoint=True)
    bin_centres = np.array([(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(nb)])
    if phases is None:
        return bin_centres

    amp_mask = np.where(amps >= min_phase_amplitude)[0]
    phases_masked = phases[amp_mask]

    for i in range(nb):
        idx = find_array_idx(phases_masked, bin_edges[i], bin_edges[i + 1])
        counts_per_bin[i] = len(idx)

    return bin_centres, counts_per_bin


def get_circular_stats(phases):
    """

    Parameters
    ----------
    phases - np.array with phase values between -pi and pi

    Returns
    -------
    pval - p value for rayleightest for uniformity
    mean_phase - mean phase of the von misle distribution
    kappa - concentration parameter of the von misle distribution
    n_counts - number of observations


    """
    pval = asstat.rayleightest(phases)
    mean_astropy, kappa_astropy = asstat.vonmisesmle(phases)
    n_counts = np.sum(len(phases))
    try:
        kappa_scipy, mean_scipy, scale = vonmises.fit(phases, fscale=1)
    except:
        kappa_scipy = None
        mean_scipy = None
        scale = None
    results = dict(
        pval=pval,
        mean_astropy=mean_astropy,
        kappa_astropy=kappa_astropy,
        n_counts=n_counts,
        kappa_scipy=kappa_scipy,
        mean_scipy=mean_scipy,
    )
    return results


io = LightIo(path=lfp_config.cluster_datadir)
stp_stats = pd.DataFrame()
observation_id = 0

lfp_bands = list(bands_def.keys())
modalities = list(modalities_def.keys())
epochs = list(epochs_def.keys())

# Setup dask
cluster = SLURMCluster(
    name='STP',
    cores=1,
    walltime='90:00:00',
    local_directory='/home/truikes/slurmoutput/STP',
    job_cpu=1,
    memory='4GB',
    job_extra=['- /data/truikes/slurm/worker/STP-worker-%j.out', '--mem-per-cpu=4G']
)
cluster.scale(cluster_scaling)

client = Client(cluster)


# For every spc file (1 per unit, per preprocessing condition (raw/reref))
tasks = []
pkdir = Path(stp1_pickledir)
for file in pkdir.iterdir():

    # Strip file name parts
    uid = file.name.split('_')[3]
    lid = file.name.split('_')[5] + '_' + file.name.split('_')[6]
    # state = file.name.split('-')[-1].split('_')[0]

    # Find Session id
    session = None
    for sid in io.session_ids:
        if uid[:6] in sid:
            session = sid
    assert session is not None, f'{uid}, {uid[:6]}, {io.session_ids}'
    io.load_session(session)

    epoch_aligned_times = dict()
    for ep in epochs_def.keys():
        events_of_interest = [epochs_def[ep]['event'] for ep in epochs_def.keys()]
        trial_ids = io.select_trials(all=True, events_of_interest=events_of_interest)
        epoch_start, epoch_stop, epoch_trial_ids = io.get_aligned_times(
            trial_ids=trial_ids,
            **epochs_def[ep],
            return_ids=True
        )
        epoch_aligned_times[ep] = dict(start=epoch_start, stop=epoch_stop)

    epochs_args = [
        file.as_posix(),
        uid,
        lid,
        'clean',
        epoch_aligned_times,
        True,
    ]

    task = client.submit(get_spc_by_epochs, *epochs_args)
    # tasks.append(task)
    fire_and_forget(task)

    modality_aligned_times = dict()
    for mod in modalities_def.keys():
        events_of_interest = [modalities_def[mod]['event'] for mod in modalities_def.keys()]
        trial_ids = io.select_trials(modality=mod, events_of_interest=events_of_interest)
        mod_start, mod_stop, mod_trial_ids = io.get_aligned_times(
            trial_ids=trial_ids,
            **modalities_def[mod],
            return_ids=True
        )
        modality_aligned_times[mod] = dict(start=mod_start, stop=mod_stop)

    mod_args = [
        file.as_posix(),
        uid,
        lid,
        'clean',
        modality_aligned_times,
        True
    ]
    task = client.submit(get_spc_by_modality, *mod_args)
    # tasks.append(task)
    fire_and_forget(task)
# for task in tasks:
#     task.result()

while True:
    print(time.time())
    time.sleep(60)
print('Done!')




