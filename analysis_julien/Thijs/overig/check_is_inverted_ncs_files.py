import pandas as pd
from pathlib import Path

HEADER_SIZE = 2 ** 14  # file have a txt header of 16kB


def scan_faulty_channels(rec_path):

    channel_overview = pd.DataFrame()
    # rec_files = os.listdir(rec_path)
    rec_path = Path(rec_path)
    for animal in rec_path.iterdir():
        print(animal)

        for session in animal.iterdir():
            if 'meta_information' in session.name:
                continue
            print(session)

            for file in (session / 'ephys').iterdir():
                if not file.suffix == '.ncs':
                    continue

                with open(file.as_posix(), 'rb') as f:
                    header = f.read(HEADER_SIZE)
                header = header.strip(b'\x00').decode('latin-1')

                headerlines = header.split('\n')
                info = dict()
                for line in headerlines:
                    if not line.startswith('-'):
                        continue
                    if len(line.split()) < 2:
                        continue
                    key = line.split()[0]
                    value = line.split()[1]
                    info[key[1:]] = value

                channel_overview.at[file.as_posix(), 'InputInverted'] = info['InputInverted']

    print(channel_overview.groupby('InputInverted').size())


if __name__ == '__main__':
    scan_faulty_channels(r'E:\hbp-data-002061\data-raw')
