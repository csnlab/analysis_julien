"""
This code is used to generate 'clean' lfps. I leave it in this repository, as we might need to know what happend
during processing.
"""

import neo
import numpy as np
import nixio
import elephant
from analysis_julien import Io
import analysis_julien.Thijs.config as cfg
import pickle
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_cfg
import analysis_julien.Thijs.tools as tools

swapped_leads = {
    'rat-16': [('Hpc7', '1'), ('Hpc8', '4')]
}

bandstop_lowerbound = 48
bandstop_upperbound = 52
bandpass_lowerbound = 1
bandpass_upperbound = 250

io = Io()

# TODO channels with no signal?


def get_clean_lfp_ids(lfp_dataframe):
    """
    Annotate the lfpframe with an 'is_noise' column and an 'best_on_tetrode' column

    Parameters
    ----------
    lfpframe - pandas dataframe, the io.LFPFrame one

    Returns
    -------

    """
    lfp_dataframe['LowCut freq float'] = lfp_dataframe['LowCut freq'].astype(float)
    lfp_dataframe['is_noise'] = False  # By default all channels are
    lfp_dataframe['best_on_tetrode'] = False  # By default none are best on the tetrode

    # Filter based on 50hz noise
    lfp_dataframe.loc[np.log(lfp_dataframe.amplitude_50hz) > lfp_cfg.max_50hz, 'is_noise'] = True
    # Filter on deviation
    lfp_dataframe.loc[np.abs(lfp_dataframe['deviation']) > lfp_cfg.max_deviation, 'is_noise'] = True
    # Filter on amlitude
    lfp_dataframe.loc[np.abs(1 - lfp_dataframe['amplitude']) > lfp_cfg.max_amplitude, 'is_noise'] = True
    # Filter on variance
    lfp_dataframe.loc[lfp_dataframe.variance > lfp_cfg.max_variance, 'is_noise'] = True
    # Filter out accidentally bandpassed filtered channels
    lfp_dataframe.loc[lfp_dataframe.is_bp_filtered, 'is_noise'] = True
    # This should filter out the same channels as the bandpassed fitered (might be redundant)
    lfp_dataframe.loc[
        (lfp_dataframe['Dsp delay'] != '0') & (lfp_dataframe['Dsp delay compensation'] is False), 'is_noise'] = True
    lfp_dataframe.loc[
        (lfp_dataframe['LowCut Enabled'] == True) & (lfp_dataframe['LowCut freq float'] > 0.1), 'is_noise'
    ] = True

    # Filter swapped leads
    for i_df, r in lfp_dataframe.iterrows():
        for rat in lfp_dataframe.keys():
            if rat in i_df:
                for tetrode, channel in swapped_leads[rat]:
                    if tetrode in i_df and channel in i_df:
                        lfp_dataframe.at[i_df, 'is_noise'] = True

    for tetrode in lfp_dataframe.tetrode.unique():
        if tetrode not in lfp_dataframe.loc[lfp_dataframe.is_noise == False].tetrode.values:
            continue
        best_idx = lfp_dataframe[
            (lfp_dataframe.tetrode == tetrode) & (lfp_dataframe.is_noise == False)].amplitude_50hz.idxmin()
        lfp_dataframe.at[best_idx, 'best_on_tetrode'] = True

    for rec_group in lfp_dataframe.recording_group.unique():
        assert rec_group in lfp_dataframe.loc[lfp_dataframe.is_noise == False].tetrode_area.values, \
            f'{rec_group} not in {lfp_dataframe.loc[lfp_dataframe.is_noise == False].tetrode_area.values}'
    return lfp_dataframe


if __name__ == '__main__':
    from pathlib import Path
    print('Getting those lfps...',io.dataset_path)
    sessions = ['rat-16_181012']
    # sessions = io.session_ids
    for sid in sessions:
        print(sid)

        # Create holder for cleaned lfps
        savename = io.dataset_path / f'neo_{sid}_lfps_clean.nio'

        # if savename.is_file():
        #     print(f'{savename} allready exists!')
        #     continue

        io.load_session(sid, read_lfp=True)
        lfpframe = get_clean_lfp_ids(io.lfp_df)
        # to_write_idx = lfpframe.loc[lfpframe.best_on_tetrode].index
        for i in lfpframe.index:
            if not lfpframe.at[i, 'is_noise']:
                print(i)

        for i, r in lfpframe.iterrows():
            print(i, r)
        # writer = neo.io.nixio.NixIO(filename=savename.as_posix())
        # fake_segment = neo.Segment(name='lfpholder')
        # fake_block = neo.Block(name='lfpholder',
        #                        channel_names=[f'{idx}_clean' for idx in to_write_idx.to_list()],
        #                        )
        #
        # test_sig = io.get_object(to_write_idx[0])
        # anasig_write = neo.AnalogSignal(
        #     name='lfp-clean',
        #     signal=np.zeros((test_sig.shape[0], len(to_write_idx)), dtype=test_sig.dtype),
        #     units=test_sig.units,
        #     sampling_rate=test_sig.sampling_rate,
        #     channel_names=[f'{idx}_clean' for idx in to_write_idx.to_list()],
        # )
        # fake_block.segments.append(fake_segment)
        # fake_block.segments[0].analogsignals.append(anasig_write)
        # writer.write_block(fake_block, use_obj_names=True)
        # writer.close()

        # nio_file = nixio.File.open(savename.as_posix())
        # block = nio_file.blocks[0]
        # lfpframe_clean = lfpframe.loc[lfpframe.is_noise == False]
        # # lfpframe_towrite = lfpframe.loc[to_write_idx]
        #
        # progbar = tools.PBar(text='getting lfps', n_ticks=40, n_counts=len(to_write_idx))
        # for i, idx in enumerate(to_write_idx):
        #     recording_group = lfpframe.loc[idx, 'recording_group']
        #
        #     # Area = bundel in drive (HC/V2/PRH/BR)
        #     lead_ids_in_area = lfpframe_clean.loc[(lfpframe_clean.recording_group == recording_group) & lfpframe_clean.best_on_tetrode]
        #     this_lfp = io.get_object(idx)
        #
        #     # Common Average Rereferencing
        #     lfps_in_area = [io.get_object(lid) for lid in lead_ids_in_area.index.to_list()]
        #     CA = lfps_in_area[0]
        #     for ii in range(1, len(lfps_in_area)):
        #         CA += lfps_in_area[ii]
        #     CA = CA / len(lfps_in_area)
        #     CAR = this_lfp - CA
        #
        #     # Bandpass filter
        #     lfpframe_towrite.loc[idx, 'bandstop_lowerbound'] = bandstop_lowerbound
        #     lfpframe_towrite.loc[idx, 'bandstop_upperbound'] = bandstop_upperbound
        #     lfpframe_towrite.loc[idx, 'bandpass_lowerbound'] = bandpass_lowerbound
        #     lfpframe_towrite.loc[idx, 'bandpass_upperbound'] = bandpass_upperbound
        #     lfpframe_towrite.loc[idx, 'channel_index'] = i
        #
        #     CAR = elephant.signal_processing.butter(CAR.flatten(), lowpass_frequency=bandpass_upperbound,
        #                                             highpass_frequency=bandpass_lowerbound,
        #                                             sampling_frequency=this_lfp.sampling_rate)
        #     CAR = elephant.signal_processing.butter(CAR, lowpass_frequency=bandstop_lowerbound,
        #                                             highpass_frequency=bandstop_upperbound,
        #                                             sampling_frequency=this_lfp.sampling_rate)
        #     CAR.name = idx
        #     for c in lfpframe.columns:
        #         CAR.annotations[c] = lfpframe.loc[idx, c]
        #
        #     block.data_arrays[i][:] = np.round(CAR.as_array()).astype(test_sig.dtype).flatten()
        #
        #     progbar.print(i)
        # progbar.close()
        #
        # nio_file.close()
        #
        # block = io.block
        # block.annotations['channel_names'] = [f'{idx}_clean' for idx in to_write_idx.to_list()]
        # block.segments[0].analogsignals = [sig for sig in block.segments[0].analogsignals if 'lfp' not in sig.name]
        #
        # lfpframe_clean_dict = dict()
        # for i in lfpframe_towrite.index:
        #     lfp_id = f'{i}_clean'
        #     if i not in lfpframe_clean_dict.keys():
        #         lfpframe_clean_dict[lfp_id] = dict()
        #
        #     for c in lfpframe_towrite.columns:
        #         lfpframe_clean_dict[lfp_id][c] = lfpframe_towrite.at[i, c]
        #
        # io.block.annotations['lfpframe_clean_dict'] = lfpframe_clean_dict
        # with open(f'{io.dataset_path}/neo_{io.session_id}.pkl', 'wb') as f:
        #     pickle.dump(io.block, f)
