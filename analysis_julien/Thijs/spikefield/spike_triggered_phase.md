### Overview of files

#### Computing spike triggered phase
* `spike_triggered_phase_1` - run on cluster (using `csn_spike_triggered_phase_1.slurm`, compute the phase per frequency bandand save 
as pickle objects
* `spike_triggered_phase_check_output_stp1` - check the progress of stp1 
* `spike_triggered_phase_2` - run on cluster (using `csn_sipke_triggered_phase_2.slurm`), group phases computed in 'spike_triggere_phase_1'
per epoch or modality and compute statistics on them
* `spike_triggered_phase_check_output_stp2` - check the progress of stp2
* `spike_triggered_phase_get_df` - save results from stp1 and stp2 in a table

