from analysis_julien import LightIo
import os

# Settings while running on cluster
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_config
from pathlib import Path

stp1_pickledir = lfp_config.cluster_pickle_stp1  # Path containing output from spike triggered phase 1
picklesavedir = lfp_config.cluster_pickle_stp2

pkdir = Path(stp1_pickledir)

pkfiles = []
outfiles = []
foundfiles = []
for file in pkdir.iterdir():
    pkfiles.append(file)

    # Strip file name parts
    uid = file.name.split('_')[3]
    lid = file.name.split('_')[5] + '_' + file.name.split('_')[6]
    state = 'clean'

    outfiles.append(f'{picklesavedir}/{uid}-{lid}-{state}_epoch')
    if os.path.isfile(outfiles[-1]):
        foundfiles.append(outfiles[-1])

    outfiles.append(f'{picklesavedir}/{uid}-{lid}-{state}_modality')
    if os.path.isfile(outfiles[-1]):
        foundfiles.append(outfiles[-1])

print(len(pkfiles))
print(len(outfiles))
print(len(foundfiles))