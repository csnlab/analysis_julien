"""
Then prints undividual units and their metrics

02/10/20

"""

import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from analysis_julien import LightIo
import pandas as pd
import os
from pathlib import Path
from analysis_julien.Thijs.tools import load_data, save_data, PBar
from plotly.colors import sequential as seq
from analysis_julien.Thijs import config as cfg

green_col = seq.YlGn[-3]
red_col = seq.Reds[-3]
alpha = 0.3
green_col_alpha = f'rgba({green_col.split("(")[1].split(")")[0]},{alpha})'
red_col_alpha = f'rgba({red_col.split("(")[1].split(")")[0]},{alpha})'

# -------------------------------- SETUP -------------------------------
include_artefacts = False  # also show metrics for artefacts
session_name = 'rat-16-181012'
session = LightIo()
workdir = cfg.resultdir_quality
if not workdir.is_dir():
    os.makedirs(workdir)

# Generate units frame for all sessions
unitsframe = load_data('evaluate_unit_metrics-unitsframe')
# unitsframe = None
if unitsframe is None:
    print('gatering unit data for all sessions...')
    all_unit_frames = []
    for sessionid in session.session_ids:
        print('\t-', sessionid)
        session.load_session(sessionid, read_lfp=False)
        session.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
        uf = session.unit_df
        all_unit_frames.append(uf)
    unitsframe = pd.concat(all_unit_frames)
    save_data(unitsframe, 'evaluate_unit_metrics-unitsframe')

unitsframe.max_drift = unitsframe.max_drift/unitsframe.max_drift.max()
# Boundaries for platting
metrics_boundaries = dict(
    waveform_amplitude=[unitsframe.waveform_amplitude.min(), unitsframe.waveform_amplitude.max()],
    num_spikes=[0, 64000],
    isi_violation=[0, 0.169],
    isolation_distance=[0, 25],
    sq_iso_dist=[unitsframe.sq_iso_dist.min(), 50],
    sq_isiv=[unitsframe.sq_isiv.min(), 0.76],
    sq_cr=[unitsframe.sq_cr.min(), unitsframe.sq_cr.max()],
    snr=[unitsframe.snr.min(), 9],
    l_ratio=[unitsframe.l_ratio.min(), 0.166],
    d_prime=[unitsframe.d_prime.min(), 1.2],
    presence_ratio=[0.6, unitsframe.presence_ratio.max()],
    amplitude_cutoff=[unitsframe.amplitude_cutoff.min(), 0.05],
    # max_drift=[unitsframe.max_drift.min(), unitsframe.max_drift.max()],  # always 0
    # silhouette_score=[unitsframe.silhouette_score.min(), 0.055],
    # cumulative_drift=[unitsframe.cumulative_drift.min(), unitsframe.cumulative_drift.max()],
    firing_rate=[unitsframe.firing_rate.min(), 9.2],
    nn_miss_rate=[unitsframe.nn_miss_rate.min(), unitsframe.nn_miss_rate.max()],
    nn_hit_rate=[unitsframe.nn_hit_rate.min(), unitsframe.nn_hit_rate.max()],
)
# metrics_boundaries = dict(
#     num_spikes=[0, unitsframe.num_spikes.max()],
#     isi_violation=[0, unitsframe.isi_violation.max()],
#     isolation_distance=[0, unitsframe.isolation_distance.max()],
#     sq_iso_dist=[unitsframe.sq_iso_dist.min(), unitsframe.sq_iso_dist.max()],
#     sq_isiv=[unitsframe.sq_isiv.min(), unitsframe.sq_isiv.max()],
#     sq_cr=[unitsframe.sq_cr.min(), unitsframe.sq_cr.max()],
#     snr=[unitsframe.snr.min(), unitsframe.snr.max()],
#     l_ratio=[unitsframe.l_ratio.min(), unitsframe.l_ratio.max()],
#     d_prime=[unitsframe.d_prime.min(), unitsframe.d_prime.max()],
#     presence_ratio=[0, unitsframe.presence_ratio.max()],
#     amplitude_cutoff=[unitsframe.amplitude_cutoff.min(), unitsframe.amplitude_cutoff.max()],
#     # max_drift=[unitsframe.max_drift.min(), unitsframe.max_drift.max()],  # always 0
#     # silhouette_score=[unitsframe.silhouette_score.min(), unitsframe.silhouette_score.max()],
#     # cumulative_drift=[unitsframe.cumulative_drift.min(), unitsframe.cumulative_drift.max()],
#     firing_rate=[unitsframe.firing_rate.min(), unitsframe.firing_rate.max()],
#     nn_miss_rate=[unitsframe.nn_miss_rate.min(), unitsframe.nn_miss_rate.max()],
#     nn_hit_rate=[unitsframe.nn_hit_rate.min(), unitsframe.nn_hit_rate.max()],
# )

# metrics_thresholds = dict(
#     isi_violation=[None, 0.5],
#     # sq_iso_dist=[10, None],
#     # isolation_distance=[5, None],
#     sq_isiv=[None, 5],
#     snr=[2.5, None]
# )

# -------------------------------- FUNCTIONS -------------------------------


# Tools
def filter_df(df, col, min_val=None, max_val=None):
    n_orig = len(df)
    df = df.loc[df[col].notna()]
    n_nan = n_orig - len(df)
    if min_val is not None:
        df = df.loc[df[col] >= min_val]
    if max_val is not None:
        df = df.loc[df[col] <= max_val]
    n_out = len(df)
    # print(f'Excluded for {col} (min: {min_val} max: {max_val}): {n_orig - n_out} (n nan: {n_nan})')

    if not include_artefacts:
        df = df.loc[df.is_artefact == False]
    return df


def plot_metric_overview(metrics_to_plot):
    uf_to_plot = unitsframe.copy()
    for m in metrics_to_plot:
        uf_to_plot = filter_df(uf_to_plot, m,
                               metrics_boundaries[m][0],
                               metrics_boundaries[m][1])

    fig = make_subplots(len(metrics_to_plot), len(metrics_to_plot))
    for ri, row_metric in enumerate(metrics_to_plot):
        row_idx = ri + 1
        for ci, col_metric in enumerate(metrics_to_plot):
            col_idx = ci + 1
            if row_idx != col_idx:
                plot_type = 'scatter'
                plot_data_good = go.Scatter(
                    mode='markers',
                    x=uf_to_plot[col_metric].loc[uf_to_plot.good_unit],
                    y=uf_to_plot[row_metric].loc[uf_to_plot.good_unit],
                    hovertext=uf_to_plot.index[uf_to_plot.good_unit],
                    marker=dict(
                        color=green_col_alpha,
                        line=dict(
                            color='DarkSlateGrey',
                            width=1,
                        ),
                        size=7,
                    ),
                    showlegend=False,
                    # hovertext=uf_to_plot.loc[uf_to_plot.good_unit].excluding_reason,
                )

                plot_data_bad = go.Scatter(
                    mode='markers',
                    x=uf_to_plot[col_metric].loc[uf_to_plot.good_unit == False],
                    y=uf_to_plot[row_metric].loc[uf_to_plot.good_unit == False],
                    marker=dict(
                        color=red_col_alpha,
                        line=dict(
                            color='DarkSlateGrey',
                            width=1,
                        ),
                        size=7,
                    ),
                    showlegend=False,
                    # hovertext=uf_to_plot.loc[uf_to_plot.good_unit == False].excluding_reason,
                )

                fig.add_trace(
                    plot_data_good,
                    row=row_idx,
                    col=col_idx,
                )
                fig.add_trace(
                    plot_data_bad,
                    row=row_idx,
                    col=col_idx,
                )
            elif row_idx == col_idx:
                plot_type = 'hist'
                hvals, edges = np.histogram(
                    uf_to_plot[col_metric],
                    bins=20,
                    range=metrics_boundaries[col_metric]
                )
                xpts = [(edges[i] + edges[i + 1]) / 2 for i in range(len(edges) - 1)]
                plot_data = go.Scatter(
                    x=xpts,
                    y=hvals,
                    line=dict(
                        color='rgba(0, 0, 200, 0.6)'
                    ),
                    fill='tozeroy',
                    fillcolor='rgba(0, 0, 200, 0.3)',
                    showlegend=False,
                )

                fig.add_trace(
                    plot_data,
                    row=row_idx,
                    col=col_idx,
                )

            if col_idx == 1:
                fig.update_yaxes(
                    title_text=row_metric,
                    row=row_idx,
                    col=col_idx,
                    tickmode='array',
                    tickvals=[f'{metrics_boundaries[row_metric][0]:.2f}',
                              f'{metrics_boundaries[row_metric][1]:.2f}'],
                    range=metrics_boundaries[row_metric]
                    # metrics_boundaries[row_metric],
                )

            else:
                fig.update_yaxes(
                    range=metrics_boundaries[row_metric],
                    row=row_idx,
                    col=col_idx,
                    tickmode='array',
                    tickvals=[],
                )

            if row_idx == len(metrics_to_plot):
                fig.update_xaxes(
                    title_text=col_metric,
                    row=row_idx,
                    col=col_idx,
                    tickmode='array',
                    tickvals=[f'{metrics_boundaries[col_metric][0]:.2f}',
                              f'{metrics_boundaries[col_metric][1]:.2f}'],
                    range=metrics_boundaries[col_metric],
                )

            else:
                fig.update_xaxes(
                    range=metrics_boundaries[col_metric],
                    row=row_idx,
                    col=col_idx,
                    tickmode='array',
                    tickvals=[],
                )

            if plot_type == 'hist':
                fig.update_yaxes(
                    range=[0, np.max(hvals) + 5],
                    row=row_idx,
                    col=col_idx,
                )

    fig.update_layout(
        margin=dict(t=50, b=0, l=50, r=0),

    )
    savename = f'{metrics_to_plot[0]}'
    for i in range(1, len(metrics_to_plot)):
        savename += f'_{metrics_to_plot[i]}'
    savename += '.html'
    savename = workdir / savename
    fig.write_html(savename.as_posix())
    # plotly.offline.plot(fig)
    print(f'Saved: {savename}')


def plot_unit(unit_id, savepath):
    assert unit_id[4:10] in session.session_id
    imsavename = (savepath / f'{uid}.png')
    if imsavename.is_file():
        return
    f = make_subplots(rows=3, cols=3,
                      column_widths=[0.3, 0.3, 0.4],
                      specs=[
                          [{'type': 'scatter'}, {'type': 'scatter'}, {'type': 'table', 'rowspan': 3}],
                          [{'type': 'scatter'}, {'type': 'scatter'}, None],
                          [{'type': 'scatter'}, {'type': 'table'}, None]
                      ],
                      horizontal_spacing=0.01,
                      vertical_spacing=0.01,
                      )
    wv_mean = -session.get_meta(unit_id, 'waveform_mean')[0]
    wv_std = session.get_meta(unit_id, 'waveform_std')

    time = np.arange(0, wv_mean.shape[1]) / 32
    pkidx = np.argmin(np.min(wv_mean, axis=0))
    time = time - time[pkidx]
    pmax = np.max(np.max(wv_mean + wv_std))
    pmin = np.min(np.min(wv_mean - wv_std))
    f.add_scatter(mode='lines', x=time, y=wv_mean[0, :], line=dict(color='rgba(0, 0, 200, 0.8)'), row=1, col=1,
                  showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[0, :] + wv_std[0, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=1, col=1, showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[0, :] - wv_std[0, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=1, col=1, showlegend=False,
                  fill='tonexty', fillcolor='rgba(0, 0, 200, 0.6)')

    f.add_scatter(mode='lines', x=time, y=wv_mean[1, :], line=dict(color='rgba(0, 0, 200, 0.8)'), row=1, col=2,
                  showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[1, :] + wv_std[1, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=1, col=2, showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[1, :] - wv_std[1, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=1, col=2, showlegend=False,
                  fill='tonexty', fillcolor='rgba(0, 0, 200, 0.6)')

    f.add_scatter(mode='lines', x=time, y=wv_mean[2, :], line=dict(color='rgba(0, 0, 200, 0.8)'), row=2, col=1,
                  showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[2, :] + wv_std[2, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=2, col=1, showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[2, :] - wv_std[2, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=2, col=1, showlegend=False,
                  fill='tonexty', fillcolor='rgba(0, 0, 200, 0.6)')

    f.add_scatter(mode='lines', x=time, y=wv_mean[3, :], line=dict(color='rgba(0, 0, 200, 0.8)'), row=2, col=2,
                  showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[3, :] + wv_std[3, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=2, col=2, showlegend=False)
    f.add_scatter(mode='lines', x=time, y=wv_mean[3, :] - wv_std[3, :], line=dict(color='rgba(0, 0, 200, 0.8)'),
                  row=2, col=2, showlegend=False,
                  fill='tonexty', fillcolor='rgba(0, 0, 200, 0.6)')

    f.update_yaxes(range=[pmin, pmax], tickmode='array', tickvals=[''])
    f.update_xaxes(tickmode='array', tickvals=[], range=[-1.5, 2])
    f.update_yaxes(tickmode='array', tickvals=[pmin+5, pmax-5], ticktext=[f'{pmin:.0f}', f'{pmax:.0f}'], range=[pmin, pmax], row=1, col=1)
    f.update_yaxes(tickmode='array', tickvals=[pmin+5, pmax-5], ticktext=[f'{pmin:.0f}', f'{pmax:.0f}'], range=[pmin, pmax], row=2, col=1)

    hvals, hedges = np.histogram(np.diff(session.get_object(unit_id).times.rescale('ms').magnitude),
                                 bins=50, range=[0, 100])
    hctr = [(hedges[i] + hedges[i + 1]) / 2 for i in range(len(hedges) - 1)]
    f.add_scatter(mode='lines', x=hctr, y=hvals, line=dict(color='rgba(0, 0, 200, 1)'),
                  row=3, col=1, showlegend=False, fill='tozeroy', fillcolor='rgba(0, 0, 200, 0.6)')
    f.update_yaxes(range=[0, np.max(hvals) + 5], tickmode='array', tickvals=[''], row=3, col=1)
    f.update_xaxes(tickmode='array', tickvals=[''], range=[0, 100], row=3, col=1)

    table_names = ['# spikes', 'isi v', 'iso dist', '(sq) iso dist', '(sq) isi v',
                   '(sq) cr', 'snr', 'l ratio', 'd prime', 'pr rat.', 'amp cut', 'f rate',
                   '', 'tetrode', 'is artefact', 'art. cause', 'good unit', 'excl. reason',
                   'kwik id']
    tables_values = [
        f'{session.get_meta(unit_id, "num_spikes")}',
        f'{session.get_meta(unit_id, "isi_violation"):.3f}',
        f'{session.get_meta(unit_id, "isolation_distance"):.1f}',
        f'{session.get_meta(unit_id, "sq_iso_dist"):.1f}',
        f'{session.get_meta(unit_id, "sq_isiv"):.3f}',
        f'{session.get_meta(unit_id, "sq_cr"):.3f}',
        f'{session.get_meta(unit_id, "snr"):.1f}',
        f'{session.get_meta(unit_id, "l_ratio"):.3f}',
        f'{session.get_meta(unit_id, "d_prime"):.3f}',
        f'{session.get_meta(unit_id, "presence_ratio"):.3f}',
        f'{session.get_meta(unit_id, "amplitude_cutoff"):.3f}',
        f'{session.get_meta(unit_id, "firing_rate"):.3f}',
        '',
        f'{session.get_meta(unit_id, "tetrode")}',
        f'{session.get_meta(unit_id, "is_artefact")}',
        f'{session.get_meta(unit_id, "artefact_cause")}',
        f'{unitsframe.at[unit_id, "good_unit"]}',
        f'{session.get_meta(unit_id, "excluding_reason")}',
        f'{session.get_meta(unit_id, "id_in_kwik"):.0f}'

    ]
    f.add_table(columnwidth=[2, 2],
                cells=dict(values=[table_names, tables_values],
                           align=['left', 'center']),
                row=1, col=3,
                )

    table_names = ['ptv', 'half w', 'ptr', 'rep sl', 'rec sl']
    table_values = [
        f'{session.get_meta(unit_id, "peak_to_valley") * 1000:.3f}',
        f'{session.get_meta(unit_id, "halfwidth") * 1000:.3f}',
        f'{session.get_meta(unit_id, "peak_trough_ratio"):.3f}',
        f'{session.get_meta(unit_id, "repolarization_slope") / 1000:.0f}',
        f'{session.get_meta(unit_id, "recovery_slope") / 1000:.0f}'
    ]
    f.add_table(columnwidth=[1.5, 1],
                cells=dict(values=[table_names, table_values],
                           align=['left', 'center']),
                row=3, col=2)

    f.update_layout(margin=dict(t=30, b=5, l=5, r=5),
                    title_text=f'{unit_id}')
    f.write_image(imsavename.as_posix())


# Metric overviews (HTML)
# plot_metric_overview(['num_spikes', 'sq_iso_dist', 'sq_isiv', 'sq_cr', 'snr'])
# plot_metric_overview(['sq_iso_dist', 'sq_isiv', 'sq_cr', 'isolation_distance', 'isi_violation', 'l_ratio', 'd_prime'])
# plot_metric_overview(['num_spikes', 'snr', 'presence_ratio', 'amplitude_cutoff', 'silhouette_score'])
# plot_metric_overview(['num_spikes', 'snr'])

plot_metric_overview(['isi_violation', 'isolation_distance', 'sq_iso_dist', 'sq_isiv'])
plot_metric_overview(['sq_cr', 'l_ratio', 'd_prime'])
plot_metric_overview(['presence_ratio', 'amplitude_cutoff'])
plot_metric_overview(['nn_miss_rate', 'nn_hit_rate', 'snr'])
plot_metric_overview(['snr', 'waveform_amplitude', 'isolation_distance', 'isi_violation', 'sq_isiv', 'firing_rate'])


# Metric overview (CSV)
metr_overview = pd.DataFrame()
for key in metrics_boundaries.keys():
    metr_overview.at[key, 'min'] = f'{unitsframe[key].min():.3f}'
    metr_overview.at[key, 'mean'] = f'{unitsframe[key].mean():.3f}'
    metr_overview.at[key, 'max'] = f'{unitsframe[key].max():.3f}'
    metr_overview.at[key, 'suggested_min'] = metrics_boundaries[key][0]
    metr_overview.at[key, 'suggested_max'] = metrics_boundaries[key][1]

savename = workdir / 'overview_metrics.csv'
try:
    metr_overview.to_csv(savename)
except PermissionError:
    print('cant save csv')

# Metric overview (violin plots) (filtered & raw)
uf_normalized = unitsframe.copy()

fig = make_subplots(rows=1, cols=len(metrics_boundaries.keys()))
for i, metric in enumerate(metrics_boundaries.keys()):
    fig.add_trace(
        go.Violin(
            y=uf_normalized[metric],
            name=metric,
            box_visible=True,
            meanline_visible=True,
        ),
        col=i + 1,
        row=1,
    )
fig.write_html((workdir / 'metric_violins_raw.html').as_posix())
for m in metrics_boundaries.keys():
    uf_normalized = filter_df(uf_normalized, m,
                              metrics_boundaries[m][0],
                              metrics_boundaries[m][1])
fig = make_subplots(rows=1, cols=len(metrics_boundaries.keys()))
for i, metric in enumerate(metrics_boundaries.keys()):
    fig.add_trace(
        go.Violin(
            y=uf_normalized[metric],
            name=metric,
            box_visible=True,
            meanline_visible=True,
        ),
        col=i + 1,
        row=1,
    )
fig.write_html((workdir / 'metric_violins_filtered.html').as_posix())

# Unit waveforms + table with metrics
unitdir = workdir / 'plot_unit_metrics'
good_dir = unitdir / 'good'
bad_dir = unitdir / 'bad'

if not good_dir.is_dir():
    os.makedirs(good_dir)
if not bad_dir.is_dir():
    os.makedirs(bad_dir)

sessions = ['rat-16_181012']
# sessions = session.session_ids
for sid in sessions:
    session.load_session(session=sid, read_lfp=False)
    session.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    prog = PBar(text=f'{sid}: Plotting units', n_ticks=40, n_counts=len(session.unit_df.index))
    for i, uid in enumerate(session.unit_df.index):
        if session.get_meta(uid, 'good_unit'):
            plot_unit(uid, good_dir)
        else:
            plot_unit(uid, bad_dir)
        prog.print(i)
    prog.close()