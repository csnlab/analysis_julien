"""
Print unit metrics in a PDF
02/10/20

"""

from plotly.colors import sequential as seq
import numpy as np
from plotly.subplots import make_subplots
from plotly import graph_objs as go
from analysis_julien import IO
import os
from PyPDF2 import PdfFileMerger
import analysis_julien.Thijs.tools as tools
import analysis_julien.Thijs.config as cfg

xmin = 50  # nr points per waveform before peak
xmax = 162  # nr points per waveform after peak

fig_heigth = 1200  # heigth per page
fig_width = 900  # width per page

# wave_clr = seq.Greens[-3]  # color of waveforms
invert_waveform = True  # Plot inverted waveforms

n_unit_rows_per_page = 4  # nr of unit-rows per page
n_unit_cols_per_page = 2  # nr of unit-cols per page

ms_pre = 1  # ms after peak to plot
ms_post = 2  # ms after peak to plot

# Metrics to plot in table
table_names = [
    'peak_to_valley',
    'halfwidth',
    'peak_trough_ratio',
    'repolarization_slope',
    'recovery_slope',
    'amplitude',
]


#
# namemap = dict(
#     peak_to_valley='ptv',
#     halfwidth='halfwidth',
#     peak_trough_ratio='pt-ratio',
#     recovery_slope='rec-slope',
#     repolarization_slope='rep-slope',
#     amplitude='amp',
# )

# unmap = dict(
#     peak_to_valley='ms',
#     halfwidth='ms',
#     peak_trough_ratio='[]',
#     recovery_slope='dV/dt?',
#     repolarization_slope='dV/dt?',
#     amplitude='mV?'
# )


def _col_widths(n_un):
    cw = [0.33, 0.33, 0.34]
    cwout = []
    for _ in range(n_un):
        for w in cw:
            cwout.append(w)
    return cwout


def _spcs(n_row, n_col):
    spout = []
    r1_base = [{'type': 'scatter'}, {'type': 'scatter'}, {'type': 'scatter'}]
    r2_base = [{'type': 'scatter'}, {'type': 'scatter'}, None]

    for ii in range(n_row):
        r1 = []
        r2 = []
        for jj in range(n_col):
            for r in r1_base:
                r1.append(r)

            for r in r2_base:
                r2.append(r)

        spout.append(r1)
        spout.append(r2)

    return spout


def _get_ax_idx(r0, c0):
    ai = (
        (2 * r0 + 1, 3 * c0 + 1),
        (2 * r0 + 1, 3 * c0 + 2),
        (2 * r0 + 2, 3 * c0 + 1),
        (2 * r0 + 2, 3 * c0 + 2),
        (2 * r0 + 1, 3 * c0 + 3),
    )
    return ai


def _newfig():
    n_subplots = n_unit_cols_per_page * n_unit_rows_per_page

    return make_subplots(rows=n_unit_rows_per_page * n_rows_per_unit,
                         cols=n_unit_cols_per_page * n_cols_per_unit,
                         column_widths=_col_widths(n_unit_cols_per_page),
                         # subplot_titles=['' for i in range(n_subplots)],
                         specs=_spcs(n_unit_rows_per_page, n_unit_cols_per_page),
                         )


resultdir = cfg.resultdir_quality
if not resultdir.is_dir():
    resultdir.mkdir(parents=True)


sess = IO(path=cfg.datadir)

for sid in sess.listsessions():

    sess.load_session(sid, False)
    sess.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)

    n_cols_per_unit = 3
    n_rows_per_unit = 2

    fig = _newfig()

    unit_row = 0
    unit_col = 0

    tmp_pdfdir = resultdir / 'tmp'
    if not tmp_pdfdir.is_dir():
        tmp_pdfdir.mkdir(parents=True)

    saved_pdfs = []
    prog = tools.PBar(f'{sid} plotting units', 20, len(sess.UnitsFrame.index))
    prog.print(0)

    for tckr, uid in enumerate(sess.UnitsFrame.index):
        template = sess.get_meta(uid, 'template')
        ymax = []
        ymin = []
        for i in range(4):
            y = template[0][0][i]
            if invert_waveform:
                y = -y
            ymax.append(np.max(y) + 20)
            ymin.append(np.min(y) - 20)

        ymin = np.min(ymin)
        ymax = np.max(ymax)

        axidx = _get_ax_idx(unit_row, unit_col)

        if sess.get_meta(uid, 'good_unit'):
            wave_clr = seq.Greens[-3]  # color of waveforms
        else:
            wave_clr = seq.Reds[-3]

        for i in range(4):
            y = template[0][0][i]
            if invert_waveform:
                y = -y

            tidx = np.argmin(y)
            time = np.arange(0, template[0][0][0].shape[0]) * (1/32)  # time in ms
            time = time - time[tidx]

            fig.add_scatter(
                x=time,
                y=y,
                row=axidx[i][0],
                col=axidx[i][1],
                line=dict(
                    color=wave_clr
                ),
                showlegend=False,
                hoverinfo='skip'
            )
            fig.update_yaxes(
                range=[ymin, ymax],
                row=axidx[i][0],
                col=axidx[i][1],

            )

            fig.update_xaxes(
                range=[-ms_pre, ms_post],
                row=axidx[i][0],
                col=axidx[i][1],
            )

            if i == 0:
                fig.update_yaxes(
                    title=dict(
                        text=f'{uid[-4:]}'
                    ),
                    row=axidx[i][0],
                    col=axidx[i][1],
                )

            if i == 1 or i == 3:
                fig.update_yaxes(
                    tickmode='array',
                    tickvals=[''],
                    row=axidx[i][0],
                    col=axidx[i][1]
                )
            else:
                fig.update_yaxes(
                    tickmode='array',
                    tickvals=[ymin, 0, ymax],
                    row=axidx[i][0],
                    col=axidx[i][1]
                )

            if i == 0 or i == 1:
                fig.update_xaxes(
                    tickmode='array',
                    tickvals=[''],
                    row=axidx[i][0],
                    col=axidx[i][1],
                )
            else:
                fig.update_xaxes(
                    tickmode='array',
                    tickvals=np.arange(-ms_pre-1, ms_post+1, 1),
                    row=axidx[i][0],
                    col=axidx[i][1],
                )

        # values = []
        #
        # table_vals = []
        # for name in table_names:
        #     if name != 'amplitude':
        #         val = sess.get_meta(uid, name)
        #     else:
        #         val = -(ymin + 20)
        #
        #     if name == 'peak_to_valley' or name == 'halfwidth':
        #         val = f'{val * 1000:.3f}'  # ms
        #     elif name == 'peak_trough_ratio':
        #         val = f'{val:.2f}'
        #     elif 'slope' in name:
        #         val = f'{val / 1000:.2f}'
        #     else:
        #         val = val
        #
        #     table_vals.append(val)
        #
        # values.append(table_vals)
        # header = [f'{uid}']
        #
        # fig.add_trace(
        #     go.Table(
        #         columnwidth=[2, 2, 2, 2, 2, 2],
        #         header=dict(values=header),
        #         cells=dict(values=values)
        #     ),
        #     row=axidx[-1][0],
        #     col=axidx[-1][1]
        # )
        sp = sess.get_by_names(uid)
        fig.add_trace(
            go.Histogram(
                x=np.diff(sp.times.rescale('ms')),
                xbins=dict(
                    start=0,
                    end=100,
                    size=1,
                ),
                marker=dict(
                    color=seq.Blues[-3]  # color of waveforms
                ),

                histnorm='probability',
                showlegend=False,
            ),
            row=axidx[-1][0],
            col=axidx[-1][1],

        )

        fig.update_xaxes(
            tickmode='array',
            tickvals=[''],
            row=axidx[-1][0],
            col=axidx[-1][1]
        )
        fig.update_yaxes(
            tickmode='array',
            tickvals=[''],
            row=axidx[-1][0],
            col=axidx[-1][1]
        )


        unit_col += 1
        prog.print(tckr)
        if unit_col >= n_unit_cols_per_page:
            unit_col = 0
            unit_row += 1

        if unit_row >= n_unit_rows_per_page:
            unit_col = 0
            unit_row = 0

            sname = f'{tmp_pdfdir}/{uid}.pdf'
            # save_figaspdf(fig, sname)
            fig.update_layout(height=fig_heigth, width=fig_width)
            fig.write_image(sname)
            saved_pdfs.append(sname)
            fig = _newfig()

    savename = cfg.resultdir_quality / f'unitmetrics_{sess.session_id}.pdf'

    pdf_merger = PdfFileMerger()
    for file in saved_pdfs:
        pdf_merger.append(file)
    prog.close()
    with open(savename, 'wb') as f:
        pdf_merger.write(f)
        print('saved pdf:', savename)