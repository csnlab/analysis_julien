import pickle
import numpy as np
import quantities as pq
import neo
import operator
import os
import elephant
from scipy.interpolate import interp1d
import pandas as pd
from .files_from_neo_09dev.nixio_fr import NixIO
import analysis_julien.Thijs.config as cfg


# noinspection PyPep8Naming
class SESSION:
    """
    IO class for ViTa dataset

    :param path: path to dataset
    :param session: name of session to load ('rat-16-181012') or integer (to get 'nth'
            avilable session)

    Overview methods:
    -load_session(session) <-- load new session data (inplace)
    -info(datatype)  <--  print available info in dataset (session, unit, trial, event)
    -describe()

    Select methods:
    -selectUnits()  <-- get ids of units with specific criteria
    -selectTrials() <-- select trials based on meta info

    Getter methods (intended for workflows)
    -get_aligned_times()  <-- get times before and after occurence of event
    -bin_spikes_per_epoch()  <-- get binned spkes using a variety of methods

    Other getter methods (used for single cases)
    -bin_spikes()
    -get_cut_spiketrains()
    -get_event_times() <-- return times of occurences of events
    -get_event() <-- get neo event by name
    -get_spiketrain() <-- get neo spiketrain by unit id)
    -get_meta() <-- get meto information on an object (get_meta('18101200003', 'isi_violation')

    example usage:
    datapath = mydatapath  # string with path to dataset
    sess = SESSION(path=datapath, session=0)  # load first available session
    # or when having analysis_julien available:
    from analysis_julien import IO
    sess = IO(path=datapath)

    sess.load_session(0)

    tactile_units = sess.selectUnits(tetrode_area='Barrel')
    tactile_trials = sess.selectTrials(modality='T')

    t_0, t_1 = sess.get_aligned_times(tactile_trials, 'trial_start', 1*qt.s, 1*qt.s)
    bins, centers = sess.bin_spikes_per_epoch(tactile_units, t0, t1, 100*qt.ms, binmethod='standard')

    """
    session_id = None
    animal_id = None
    NEO = None

    unit_ids = None
    trial_ids = None

    unit_metanames = ['tetrode', 'area', 'full_tet_name', 'is_duplicate', 'celltype']
    unit_metricnames = ['num_spikes', 'firing_rate', 'presence_ratio',
                        'isi_violation', 'amplitude_cutoff', 'snr',
                        'max_drift', 'cumulative_drift',  # 'silhouette_score'
                        'isolation_distance', 'l_ratio', 'd_prime', 'tetrode_area',
                        'good_unit',
                        'nn_hit_rate', 'nn_miss_rate',
                        'peak_to_valley', 'halfwidth', 'peak_trough_ratio',
                        'repolarization_slope', 'recovery_slope',
                        'sq_iso_dist', 'sq_isiv', 'sq_cr', 'sq_isigood',
                        'waveform_amplitude'
                        ]
    eventnames = ['trial_start', 'trial_end',
                  'sample_start',
                  'sample_end',
                  'video_trigger',
                  'visual_start', 'visual_end',
                  'tactile_start', 'tactile_end',
                  'left_poke', 'right_poke',
                  'poke', 'trial_meta', 'retraction',
                  'touch', 'touch_L', 'touch_R']

    unit_criteria = None
    LFPFrame = None
    lfp_metanames = None

    def __init__(self, path=None, session=None, read_lfp=True):
        """
        IO class for the ViTa dataset
        :param path: system path containing the dataset
        :param session: name of the session to load (ie 'rat-16-161018')

        methods:
        sess
        """
        # if path is None:
        #     if os.path.isdir(datasetpath):
        #         self.dataset_path = datasetpath
        #     else:
        #         raise ValueError('pass the "path" argument')
        # else:
        #     assert os.path.isdir(path), f'Invalid path given: {path}'
        assert path is not None
        self.dataset_path = path

        if session is not None:
            self.load_session(session, read_lfp=read_lfp)

    def load_session(self, session, read_lfp=True):
        """
        Load session data to class instance (inplace!)
        :param session: name of the sessio nto load (ie 'rat-16-161810')
        :param read_lfp: dont read lfp files
        :return:
        """
        if isinstance(session, int):
            available_sessions = self.listsessions()
            assert session < len(available_sessions), f'only {len(available_sessions)} available'
            session = available_sessions[session]
        else:
            assert len(session) == 13 and 'rat' in session, f'session format: rat-##-YYMMDD'
        self.session_id = session
        self.animal_id, self.session_date = session.split('_')
        self.NEO = self._load_pickle(f'{self.dataset_path}/neo_{session}.pkl')
        self.unit_criteria = self.NEO.annotations['unit_criteria']
        lfpfile = f'{self.dataset_path}/neo_{self.animal_id}-{self.session_date}_lfps.nio'
        lfpfile_clean = f'{self.dataset_path}/neo_{self.animal_id}-{self.session_date}_lfps_clean.nio'
        if os.path.isfile(lfpfile) and read_lfp:
            self._load_lfp(lfpfile)

        if os.path.isfile(lfpfile_clean) and read_lfp:
            self._load_lfp_clean(lfpfile_clean)

        self._get_trial_metanames()
        self._annotate_unit_type()
        self._get_unit_metanames()
        self._get_dataframes()
        self.unit_ids = self.UnitsFrame.index
        self.trial_ids = self.TrialFrame.index

    # ---------------------- SELECT METHODS -------------------------

    def get_by_names(self, names):
        """
        Retreive object from dataset by its name, or multiple objects by a list of
        names:

        spiketrains = sess.get_by_names(['1810120000', '1810120004'])
        spiketrain = sess.get_by_names('1810120000')

        param names: string or list of strings
            names of objects to return

        output:
            object (if only 1 was found) or list of objects
        """
        if not isinstance(names, list):
            res = self.NEO.filter(name=names)
            if len(res) > 1:
                raise ValueError(f'{names} returned multiple objects')
            elif len(res) == 0:
                raise ValueError(f'{names} returned 0 objects')
            return res[0]
        else:
            res = []
            for name in names:
                r = self.NEO.filter(name=name)
                if len(r) > 1:
                    raise ValueError(f'{name} returned multiple objects')
                elif len(r) == 0:
                    raise ValueError(f'{name} returned 0 objects')
                res.append(r[0])
            if len(res) == 1:
                return res[0]
            else:
                return res

    def selectUnits(self, **kwargs):
        """

        :param kwargs: colname=[condition, operator], ie: tetrode_area=['Barrel', '==']
        :return:
        """
        unitids = []
        for i in range(len(self.NEO.segments[0].spiketrains)):
            sp = self.NEO.segments[0].spiketrains[i]
            uid = sp.annotations['unitid']
            if 'all' in kwargs.keys() and kwargs['all']:
                unitids.append(uid)
                continue

            is_valid = True
            for metric in kwargs.keys():
                # assert metric in self.unit_metanames or metric in self.unit_metricnames, \
                #     f'{metric} not in {self.unit_metanames}, {self.unit_metricnames}'

                if isinstance(kwargs[metric], list):
                    vals = kwargs[metric][:-1]
                    opr = kwargs[metric][-1]

                    keep_by_metric_name = False
                    for val in vals:
                        # if metric in self.unit_metanames:
                        #     assert val in self.unit_metavalues[metric], f'{val} not in {self.unit_metavalues[metric]}'
                        if self._get_operator(opr)(sp.annotations[metric], val):
                            keep_by_metric_name = True

                    if not keep_by_metric_name:
                        is_valid = False

                else:
                    val = kwargs[metric]
                    opr = '=='
                    # if metric in self.unit_metanames:
                    #     assert val in self.unit_metavalues[metric], f'{val} not in {self.unit_metavalues[metric]}'

                    if not self._get_operator(opr)(sp.annotations[metric], val):
                        is_valid = False

            if is_valid:
                unitids.append(uid)
        return unitids

    def selectLFPS(self, **kwargs):
        lfpids = []
        for i in self.LFPFrame.index:
            if 'all' in kwargs.keys() and kwargs['all']:
                lfpids.append(i)
                continue

            is_valid = True
            for metric in kwargs.keys():
                assert metric in self.LFPFrame.columns, f'{metric} not in {self.LFPFrame.columns}'

                if isinstance(kwargs[metric], list):
                    vals = kwargs[metric][:-1]
                    opr = kwargs[metric[-1]]

                    keep_by_metric_name = False
                    for val in vals:
                        if self._get_operator(opr)(self.LFPFrame.loc[i][metric], val):
                            keep_by_metric_name = True
                    if not keep_by_metric_name:
                        is_valid = False

                else:
                    val = kwargs[metric]
                    opr = '=='

                    if not self._get_operator(opr)(self.LFPFrame.loc[i][metric], val):
                        is_valid = False
            if is_valid:
                lfpids.append(i)
        return lfpids

    def selectTrials(self, events_of_interest=None, **kwargs):
        """
        Get trial ids meeting your conditions (can take multiple conditions), examples:
        tid = selectTrials(modality='V')  # all visual trails
        tid = selectTrials(modality='V', correct=1)
        tid = selectTrials(modality=['V', 'T', '=='])  # all unimodal trials

        :param events_of_interest: event_name or list of event names which should not be None
        :param kwargs: colname=[condition, operator], ie: modality=['V', '==']
                or colname='V'
        :return:
        """
        if events_of_interest is not None:
            if not isinstance(events_of_interest, list):
                events_of_interest = [events_of_interest]
            for ev_name in events_of_interest:
                assert ev_name in self.eventnames, f'{ev_name} not in {self.eventnames}'

        trialids = []
        trial_meta = self.get_by_names('trial_meta')
        for i in range(len(trial_meta)):
            if 'xxxxx' in trial_meta.labels[i]:  # never return undefined trials
                continue

            valid_id = True  # Flag to keep or throw the trialid
            trial_metainfo = trial_meta.array_annotations_at_index(i)
            trial_id = trial_meta.labels[i]

            if events_of_interest is not None:
                for ev_name in events_of_interest:
                    evtime = self.get_event_times(event_name=ev_name, trial_ids=trial_id,
                                                  verbose=False)
                    if len(evtime) == 0:
                        valid_id = False

            if 'all' in kwargs.keys() and kwargs['all']:  # return all trial ids
                if valid_id:
                    trialids.append(trial_meta.labels[i])
                continue

            for meta_name in kwargs.keys():
                assert meta_name in trial_metainfo.keys(), f'{meta_name} not a meta name'
                if isinstance(kwargs[meta_name], list):
                    vals = kwargs[meta_name][:-1]
                    opr = kwargs[meta_name][-1]

                    keep_by_this_meta_name = False
                    for val in vals:
                        if isinstance(val, int):
                            val = f'{val:1.1f}'
                        # assert val in self.trial_metavalues[meta_name], \
                        #     f'{val} not in {self.trial_metavalues[meta_name]} '
                        if self._get_operator(opr)(trial_metainfo[meta_name], val):
                            keep_by_this_meta_name = True

                    if not keep_by_this_meta_name:
                        valid_id = False

                else:
                    val = kwargs[meta_name]
                    opr = '=='

                    if isinstance(val, int):
                        val = f'{val:1.1f}'
                    # assert val in self.trial_metavalues[meta_name], f'{val} not in {self.trial_metavalues[meta_name]}'

                    if not self._get_operator(opr)(trial_metainfo[meta_name], val):
                        valid_id = False

            if valid_id:
                trialids.append(trial_meta.labels[i])

        return trialids

    # ---------------------- GET METHODS -------------------------
    def get_lfps(self, lfp_ids, t_start=None, t_stop=None):
        if not isinstance(lfp_ids, list):
            lfp_ids = [lfp_ids]

        # Check if being asked for clean lfps
        return_clean_lfp = False
        for lid in lfp_ids:
            if 'clean' in lid:
                return_clean_lfp = True

        if not return_clean_lfp:
            dataname = 'lfps'
            df = self.LFPFrame
            channel_indexes = [int(df.loc[lid].channel_idx) for lid in lfp_ids]

        else:
            dataname = 'lfps_clean'
            df = self.LFPFrame_clean
            channel_indexes = [np.where(df.index == lid)[0][0] for lid in lfp_ids]

        lfps = self.get_by_names(dataname)
        # lfps.array_annotations['lfp_id'] = lfp_ids
        if t_start is None:
            lfp_out = lfps.load(time_slice=None, channel_indexes=channel_indexes)
            lfp_out.array_annotations['lfp_id'] = lfp_ids
            for col in df.columns:
                lfp_out.array_annotations[col] = [df.at[lid, col] for lid in lfp_ids]
            return lfp_out
        else:
            assert t_start is not None and hasattr(t_start, 'units')
            assert t_stop is not None and hasattr(t_stop, 'units')
            return lfps.load(time_slice=(t_start, t_stop), channel_indexes=channel_indexes)

    def get_meta(self, id_in_block, metaname):
        """
        Get annotaitons from object in dataset by object id and metanme

        :param id_in_block: str
            id of object in dataset
        :param metaname: str
            name of annotation to return

        output: the annotation value
        """
        trial_meta = self.get_by_names('trial_meta')
        if id_in_block in trial_meta.labels:
            idx = np.where(trial_meta.labels == id_in_block)[0]
            return trial_meta.array_annotations_at_index(idx)[metaname][0]
        else:
            obj = self.get_by_names(id_in_block)
            assert metaname in obj.annotations.keys(), f'{metaname} not in {obj.annotations.keys()}'
            return obj.annotations[metaname]

    def get_time(self, trial_id, event_name):
        assert isinstance(event_name, str), 'event should be a string'
        assert event_name in self.eventnames, f'{event_name} not in {self.eventnames}'

        times = self.get_event_times(trial_ids=trial_id, event_name=event_name,
                                     verbose=False)
        if len(times) > 0:
            return times[0]
        else:
            return None

    # --------------------- ANALOG SIGNAL METHODS ----------------------
    def sample_signal_at_times(self, sample_times, signalname=None, data=None,
                               data_times=None, interpolation_method='cubic',
                               return_neo_signal=True):
        """
        return the interpolated values of a signal, given sample_times
        fills with nans in case of out-of-bound errors

        param sample_times: times points where from to sample
        param data: data to sample from, should be a 1-D array, or an array
                    with data on only 1 axis (ie data.shape=[1, 1, 80],
                    can have quantities times


        param data_times: if data does not have quantities times, pass
                            data_times, a quantities array of timepoints mathcing
                            data
        param signalname: extract the signal from the class, instead of passing it though
                            data
        param return irreular_signal: return a neo.return_neo_signal

        output: neo.IrregularSampledSignal or np.ndarray with data at sampled times
        """

        # extract arrays from input/check if input is ok
        if signalname is not None:
            signal = self.get_by_names(signalname)
            xdata = signal.times.rescale('us').magnitude
            ydata = signal.magnitude
            signame = signalname

        elif data is not None and hasattr(data, 'times'):
            assert data_times is None, f'either pass a NEO array as data or add data_times'
            xdata = data.times.rescale('us').magnitude
            ydata = data.magnitude
            signame = data.name

        else:
            assert data is not None and data_times is not None, f'either pass a NEO array as data or add data_times'
            assert hasattr(data_times, 'units'), 'data_times should have quantities'
            xdata = data_times.rescale('us').magnitude
            assert len(np.where(np.array(xdata.shape) > 1)[0]) == 1, 'data_times should be one dimensional'
            ydata = data
            assert len(np.where(np.array(ydata.shape) > 1)[0]) == 1, 'data should be one dimensional'
            signame = 'unkown'

        assert hasattr(sample_times, 'units'), 'sample_times should have units'
        assert len(np.where(np.array(xdata.shape) > 1)[0]) == 1, 'data_times should be one dimensional'
        sample_times = sample_times.rescale('us').magnitude
        sample_times = sample_times.flatten()

        xdata = xdata.flatten()
        ydata = ydata.flatten()

        # compute a margin for datacutting
        margin = 10 * pq.s
        margin = margin.rescale('us').magnitude

        # cut data to save time on fitting
        keep_index = np.where((xdata >= sample_times[0] - margin) &
                              (xdata <= sample_times[-1] + margin))[0]
        if keep_index.shape[0] == 0:
            return None
        xdata = xdata[keep_index]
        ydata = ydata[keep_index]

        # fit data and extract data at given sample_times
        interp_func = interp1d(x=xdata, y=ydata,
                               kind=interpolation_method,
                               bounds_error=False,
                               fill_value=np.nan)
        sampled_data = interp_func(sample_times)

        # return data in specified format
        if return_neo_signal:
            new_sig = neo.IrregularlySampledSignal(
                times=sample_times,
                signal=sampled_data,
                time_units='us',
                units='',
                name=signame,
            )
            return new_sig
        else:
            return sampled_data

    def get_analog_signal_by_id(self, name=None, signal=None, trialid=None):
        """
        Return the named analogsignal, returns full signal if trialid is not given.
        name: name of the analogsignal
        trialid: None or trialid

        return:
        analogsignal; full signal if trialid is none, else the analogsignal during the trial
        """
        assert signal is not None or name is not None, f'either pass "signal" or "name"'
        assert signal is None or name is None, f'pass either "signal" or "name"'

        if signal is None:
            signal = self.get_by_names(name)

        if trialid is not None:
            idx = np.where(np.array(signal.annotations['trial_id']) == trialid)[0]
            new_sig = neo.IrregularlySampledSignal(
                times=signal.times[idx],
                time_units=signal.times.units,
                signal=signal.magnitude[idx],
                units='',
                idx_in_video=signal.annotations['idx_in_video'][idx],
                trial_id=np.array(signal.annotations['trial_id'])[idx],
                video_names=np.array(signal.annotations['video_names'])[idx],
            )
            return new_sig
        else:
            return signal

    # ------------------------ EVENT METHODS ---------------------------
    def get_event_times(self, trial_ids, event_name, verbose=True, return_ids=False):
        """
        Retrun event times, specified by trial ids and event name.
        If verbose is true, the method will report cases of trials where
        the event did not occur

        :param trial_ids: str or list of str
            trial ids to return event for
        :param event_name: str
            name of event to get time for
        :param verbose: bool
            if true, will report trials where event did not occur
        :param return_ids: bool
            if true, will return als the triald ids matching the times

        Output:
            quantities, list
                quantities array with even times and corresponding trial_ids

        """
        assert isinstance(event_name, str), 'event should be a string'
        assert event_name in self.eventnames, f'{event_name} not in {self.eventnames}'
        if not isinstance(trial_ids, list):
            trial_ids = [trial_ids]

        trial_meta = self.get_by_names('trial_meta')
        times_out, labels_out = [], []
        event = self.get_by_names(event_name)

        for tid in trial_ids:
            assert tid in trial_meta.labels, f'{tid} is not a valid trial id'
            idx = np.where(event.labels == tid)[0]
            if len(idx) == 0:
                if verbose:
                    print(f'{tid} not in {event_name}')
                continue

            for i in idx:
                times_out.append(event[i].magnitude)
                labels_out.append(tid)

        if not return_ids:
            return np.asarray(times_out) * event.units
        else:
            return np.asarray(times_out) * event.units, labels_out

    def get_aligned_times(self, trial_ids, event, time_before, time_after,
                          return_ids=False, verbose=False):
        """
        Get times in session, per selected trial, around en event of interest

        Note: if an event time for a given trial is None; it will not include the trial_is

        :param trial_ids: trial_ids: list of trial_ids (as in TrialFrame.index or .array_annotations['trialid'])
        :param event: event: string or neo.Event
        :param time_before: [quantities] time before event
        :param time_after: [quantities] time after event
        :param return_ids: [bool] if True, function returns trial ids
        :param verbose: [bool] if True, print errors
        :return: [neo.Event] times before, [neo.Event] times after
        """
        assert hasattr(time_before, 'units'), 'time_before should be quantities'
        assert hasattr(time_after, 'units'), 'time_after should be quantities'
        assert isinstance(event, str), 'eventname needs to be a string'

        event_times = self.get_event_times(trial_ids, event, verbose=verbose)
        ev_times = event_times.rescale('us')
        if return_ids:
            return ev_times - time_before, ev_times + time_after, trial_ids
        else:
            return ev_times - time_before, ev_times + time_after

    # ---------------------- BIN METHODS -------------------------

    def bin_spikes(self, unit_ids, t_start, t_stop, binmethod,
                   binsize=None, slide_by=None, sigma=None, sampling_period=None):
        """
        Return binned spikes between t_start and t_stop, using a 'sliding window' and
        given binsize). Methods:
        - standard (not working right now)
        - sliding_window, params:
            -slide_by
            -binsize
        - convoluted, params:
            -sigma
            -sampling_period

        :param unit_ids: list of ids as in UnitsFrame.index or spiketrain.annotations['unitid']
        :param binsize: [quantities] width of bin
        :param t_start: [quantities]
        :param t_stop: [quantities]
        :param binmethod: string: either 'standard', 'sliding_window', 'convoluted'
        :param slide_by: stepsize for sliding window (only for 'sliding_window')
        :param sigma: width of kernel (only fo convoluted)
        :param sampling_period: assumed resolution of spiketrain
        :return: [np.array] binned spikes, [quantities TODO:check] spike bin centers
        """
        assert hasattr(t_start, 'units'), 'add quantititesf'
        assert hasattr(t_stop, 'units'), '.'
        assert len(t_start.magnitude.shape) == 0
        assert len(t_stop.magnitude.shape) == 0

        t_start = t_start.rescale('us')
        t_stop = t_stop.rescale('us')

        assert binmethod in ['standard', 'sliding_window', 'convoluted'], f'invalid binmethod: {binmethod}'
        binned_spikes, spike_bin_centers = (None, None)

        if binmethod == 'standard':
            binned_spikes, spike_bin_centers = self._bin_spikes(
                unit_ids=unit_ids, t_start=t_start, t_stop=t_stop, binsize=binsize,
                overlapping=False)
            # raise NameError('standard not working, use "sliding" or "convoluted"')

        elif binmethod == 'sliding_window':
            assert slide_by is not None, 'add "slide_by (slide step) keyword'
            assert hasattr(slide_by, 'units'), 'should have quantities'
            assert binsize is not None, 'add "slide_by (slide step) keyword'
            assert hasattr(binsize, 'units'), 'should have quantities'
            binned_spikes, spike_bin_centers = self._bin_spikes(
                unit_ids=unit_ids, t_start=t_start, t_stop=t_stop, binsize=binsize,
                slide_by=slide_by, overlapping=True)

        elif binmethod == 'convoluted':
            assert sigma is not None, 'add "sigma" keyword'
            assert hasattr(sigma, 'units'), 'add quantities'
            assert sampling_period is not None, 'add "sampling_period" keyword'
            assert hasattr(sampling_period, 'units'), 'add quantities'
            binned_spikes, spike_bin_centers = self._bin_spikes_elephant_instantaneous(
                unit_ids=unit_ids, t_start=t_start, t_stop=t_stop, sampling_period=sampling_period,
                sigma=sigma)
        return binned_spikes, spike_bin_centers

    def bin_spikes_per_epoch(self, unit_ids, t_start, t_stop, binsize=None,
                             binmethod=None, slide_by=None, as_array=False,
                             sigma=None, sampling_period=None):
        """
        given binsize). Methods:
        - standard (not working right now)
        - sliding_window, params:
            -slide_by
            -binsize
        - convoluted, params:
            -sigma
            -sampling_period

        :param unit_ids: list of ids as in UnitsFrame.index or spiketrain.annotations['unitid']
        :param binsize: [quantities] width of bin
        :param t_start: neo.Event or np.array*qt.s of times to start cutting
        :param t_stop: as t_start
        :param binsize: width of window
        :param binmethod: string: either 'standard', 'sliding_window', 'convoluted'
        :param sliding_window: old, use binmethod instead
        :param slide_by: stepsize for sliding window (only for 'sliding_window')
        :param sigma: width of kernel (only fo convoluted)
        :param as_array: return array instaed of list
        :param sampling_period: underlying resolution of spiketrain (used with convoluted)
        :return:
        """
        if 'sliding' in binmethod:
            binmethod = 'sliding_window'
        elif 'conv' in binmethod:
            binmethod = 'convoluted'
        elif 'stand' in binmethod:
            binmethod = 'standard'
        else:
            print('invalid param, use "standard", "sliding" or "convoluted" ')
            return None, None

        binned_spikes, spike_bin_centers = [], []

        for i, (t0, t1) in enumerate(zip(t_start, t_stop)):
            bs, sbc = self.bin_spikes(unit_ids=unit_ids,
                                      binsize=binsize, binmethod=binmethod,
                                      t_start=t0, t_stop=t1,
                                      slide_by=slide_by, sigma=sigma,
                                      sampling_period=sampling_period)
            binned_spikes.append(bs)
            spike_bin_centers.append(sbc)
        if as_array:
            sbunits = spike_bin_centers[0].units
            binned_spikes = np.stack(binned_spikes, axis=0)
            binned_spikes = np.moveaxis(binned_spikes, 1, 0)
            spike_bin_centers = np.stack(spike_bin_centers, axis=0) * sbunits
        return binned_spikes, spike_bin_centers

    def _bin_spikes(self, unit_ids, binsize, t_start, t_stop, overlapping, slide_by=None):

        spiketrains = self.get_by_names(unit_ids)
        if len(unit_ids) == 1:
            spiketrains = [spiketrains]

        spiketrains = [sp.time_slice(t_start=t_start, t_stop=t_stop) for sp
                       in spiketrains]

        if overlapping:

            bin_edges, bin_centers = self.make_overlapping_bins(binsize=binsize,
                                                                slide_by=slide_by,
                                                                t_start=t_start,
                                                                t_stop=t_stop)

        else:
            bin_edges, bin_centers = self.make_non_overlapping_bins(binsize=binsize,
                                                                    t_start=t_start,
                                                                    t_stop=t_stop)

        num_bins = len(bin_edges)  # Number of bins
        num_neurons = len(spiketrains)  # Number of neuron_inds
        binned_spikes = np.empty([num_neurons, num_bins])

        for i, train in enumerate(spiketrains):
            spike_times = train.times.rescale('us')
            for t, edges in enumerate(bin_edges):
                binned_spikes[i, t] = np.histogram(spike_times, edges)[0]

        binned_spikes = binned_spikes.astype(int)

        return binned_spikes, bin_centers

    def _bin_spikes_elephant_instantaneous(self, unit_ids, t_start, t_stop, sigma, sampling_period):
        """
        Use elephant.statistics.instantaneous_rate to estimate firing rate
        """
        sigma = sigma.rescale('ns')
        sampling_period = sampling_period.rescale('ns')
        kernel = elephant.kernels.GaussianKernel(sigma=sigma)
        spiketrains = self.get_by_names(unit_ids)
        if len(unit_ids) == 1:
            spiketrains = [spiketrains]
        spiketrains = [sp.time_slice(t_start=t_start, t_stop=t_stop) for sp in spiketrains]

        binned_spikes = []
        spike_bin_centers = []
        for i, sp in enumerate(spiketrains):
            bs = elephant.statistics.instantaneous_rate(spiketrain=sp.rescale('us'),
                                                        sampling_period=sampling_period,
                                                        kernel=kernel,
                                                        t_start=t_start,
                                                        t_stop=t_stop,
                                                        )
            binned_spikes.append(bs.magnitude.flatten())
            spike_bin_centers.append(bs.times.rescale('us'))

        binned_spikes = np.stack(binned_spikes, axis=0)
        sbc_units = spike_bin_centers[0].units
        spike_bin_centers = np.stack(spike_bin_centers, axis=0) * sbc_units
        return binned_spikes, spike_bin_centers

    def bin_signal(self, signal_name, t_start, t_stop, binmethod, aggregate_method,
                   binsize, slide_by=None):

        """
        :param signal_name:
        :param t_start:
        :param t_stop:
        :param binmethod: standard or sliding
        :param aggregate_method: average or sum (what to do with the signal in each bin)
        :param binsize:
        :param slide_by:
        :return:
        """
        analog_signal = self.get_by_names(signal_name)
        signal_times = analog_signal.times
        signal = analog_signal.magnitude
        assert signal.shape[1] == 1
        signal = signal.flatten()

        if binmethod == 'sliding':
            assert slide_by is not None
            bin_edges, bin_centers = self.make_overlapping_bins(
                binsize=binsize,
                slide_by=slide_by,
                t_start=t_start,
                t_stop=t_stop)
        elif binmethod == 'standard':
            bin_edges, bin_centers = self.make_non_overlapping_bins(
                binsize=binsize,
                t_start=t_start,
                t_stop=t_stop)

        binned_signal = np.zeros(bin_centers.shape[0])

        for indx, edges in enumerate(bin_edges):

            mask = np.logical_and(signal_times >= edges[0],
                                  signal_times < edges[1])

            if aggregate_method == 'average':
                if mask.sum() > 1:
                    val = np.nanmean(signal[mask])
                else:
                    val = np.nan
            elif aggregate_method == 'sum':
                if mask.sum() > 0:
                    val = np.nansum(signal[mask])
                else:
                    val = np.nan

            binned_signal[indx] = val

        return binned_signal, bin_centers

    def bin_signal_per_epoch(self, signal_name, t_start, t_stop, binsize=None,
                             binmethod=None, aggregate_method=None, slide_by=None):

        binned_signal, signal_bin_centers = [], []

        for i, (t0, t1) in enumerate(zip(t_start, t_stop)):
            bs, sbc = self.bin_signal(signal_name=signal_name,
                                      t_start=t0, t_stop=t1,
                                      binmethod=binmethod,
                                      aggregate_method=aggregate_method,
                                      binsize=binsize,
                                      slide_by=slide_by)

            binned_signal.append(bs)
            signal_bin_centers.append(sbc)
        # if as_array :
        #     sbunits = spike_bin_centers[0].units
        #     binned_spikes = np.stack(binned_spikes, axis=0)
        #     binned_spikes = np.moveaxis(binned_spikes, 1, 0)
        #     spike_bin_centers = np.stack(spike_bin_centers, axis=0) * sbunits
        return binned_signal, signal_bin_centers

    @staticmethod
    def make_overlapping_bins(binsize, slide_by, t_start, t_stop):
        left_edges = np.arange(t_start.rescale('us'), t_stop.rescale('us'), slide_by.rescale('us')) * pq.us
        bin_edges = [(e, e + binsize.rescale('us')) for e in left_edges]

        # make sure that all the bin centers are within the event
        bin_edges = [b for b in bin_edges if (b[0] + b[1]) / 2 <= t_stop.rescale('us')]
        # make sure that bins are fully within
        bin_edges = [b for b in bin_edges if b[1] <= t_stop.rescale('us')]

        # prepare the bin centers (to return)
        bin_centers = [(b1 + b2) / 2 for b1, b2 in bin_edges]
        bin_centers = np.array(bin_centers) * pq.us

        return bin_edges, bin_centers

    @staticmethod
    def make_non_overlapping_bins(binsize, t_start, t_stop):

        left_edges = np.arange(t_start.rescale('us'),
                               t_stop.rescale('us'), binsize.rescale('us')) * pq.us
        bin_edges = [(e, e + binsize.rescale('us')) for e in left_edges]

        # make sure that all the bin centers are within the event
        bin_edges = [b for b in bin_edges if
                     (b[0] + b[1]) / 2 <= t_stop.rescale('us')]
        # make sure that bins are fully within
        # bin_edges = [b for b in bin_edges if b[1] <= t_stop.rescale('us')]

        # prepare the bin centers (to return)
        bin_centers = [(b1 + b2) / 2 for b1, b2 in bin_edges]
        bin_centers = np.array(bin_centers) * pq.us
        return bin_edges, bin_centers

    # ---------------------- PREPROCESSING  -------------------------

    def set_unit_criteria(self, verbose=True, **kwargs):
        if verbose:
            print('Updating unit criteria (removing existing):')
        self.unit_criteria = dict()
        for kwarg in kwargs.items():
            if verbose:
                print(f'\t{kwarg[0]} - {kwarg[1]}')
            self.unit_criteria[kwarg[0]] = kwarg[1]

        self._annotate_good_unit()
        self._get_dataframes()

    def _annotate_unit_type(self):
        for sp in self.NEO.segments[0].spiketrains:
            celltype = 'unkown'
            for label, thresholds in cfg.unit_classification_thresholds.items():
                is_this_label = True

                for key, (thr, opr) in thresholds.items():
                    if key == 'peak_to_valley':
                        thr = thr.rescale('s').magnitude
                    if not self._get_operator(opr)(sp.annotations[key], thr):
                        is_this_label = False

                if is_this_label:
                    celltype = label

            sp.annotate(celltype=celltype)

    def _annotate_good_unit(self):
        for sp in self.NEO.segments[0].spiketrains:
            is_good = True
            excluding_reason = ''
            for uc in self.unit_criteria.items():
                name = uc[0]
                val = uc[1][0]
                opr = uc[1][1]

                if not self._get_operator(opr)(sp.annotations[name], val):
                    is_good = False
                    excluding_reason += '_' + f'{name}'

            # TODO
            # if sp.annotations['is_artefact']:
            #     is_good = False

            # if 'merged' in sp.annotations.keys():
            #     is_good = True

            sp.annotate(good_unit=is_good)
            sp.annotate(excluding_reason=excluding_reason)

    def _get_dataframes(self):
        tm = self.get_by_names('trial_meta')

        self.TrialFrame = pd.DataFrame()
        for i, tid in enumerate(tm.labels):
            if 'xxx' in tid:
                continue
            for name in self.trial_metanames:
                self.TrialFrame.at[tid, name] = tm.array_annotations_at_index(i)[name]

        self.UnitsFrame = pd.DataFrame()
        for sp in self.NEO.segments[0].spiketrains:
            uid = sp.annotations['unitid']
            for name in self.unit_metanames:
                if name in sp.annotations.keys():  # fix for old data Noa
                    if sp.annotations[name] is not None:
                        self.UnitsFrame.at[uid, name] = sp.annotations[name]
            for name in self.unit_metricnames:
                if name == 'merged' and name not in sp.annotations.keys():
                    continue
                self.UnitsFrame.at[uid, name] = sp.annotations[name]

            if 'excluding_reason' in sp.annotations.keys():
                self.UnitsFrame.at[uid, 'excluding_reason'] = sp.annotations['excluding_reason']

    def _get_trial_metanames(self):
        self.trial_metanames = []
        # self.trial_metavalues = dict()
        tm = self.get_by_names('trial_meta')
        for name in tm.array_annotations.keys():
            if 'video' in name:
                continue
            self.trial_metanames.append(name)

            # self.trial_metavalues[name] = []
            # for i in range(len(tm)):
            #     if 'xxx' in tm.array_annotations[name][i]:
            #         continue
            #     if tm.array_annotations[name][i] not in self.trial_metavalues[name]:
            #         self.trial_metavalues[name].append(tm.array_annotations[name][i])

    def _get_unit_metanames(self):
        self.unit_metavalues = dict()
        for name in self.unit_metanames:
            self.unit_metavalues[name] = []

            for sp in self.NEO.segments[0].spiketrains:
                # if name in sp.annotations.keys():  # fix for old data Noa
                    # if sp.annotations[name] not in self.unit_metavalues[name]:
                    #     self.unit_metavalues[name].append(sp.annotations[name])
                tetname = sp.annotations['tetrode']
                if 'Bar' in tetname:
                    sp.annotate(tetrode_area='BR')
                elif 'Vis' in tetname:
                    sp.annotate(tetrode_area='VIS')
                elif 'Hpc' in tetname:
                    sp.annotate(tetrode_area='HPC')
                elif 'Per' in tetname:
                    sp.annotate(tetrode_area='PRH')
                else:
                    sp.annotate(tetrode_area='unkown')

    def _load_lfp(self, lfpfile):
        # Load raw LFPS
        reader = NixIO(filename=lfpfile)
        block = reader.read_block(lazy=True)
        lfpsig = block.segments[0].analogsignals[0]
        lfpsig.name = 'lfps'
        for i in range(lfpsig.shape[1]):
            lfpsig.array_annotations['channel_names'][i] = block.annotations['channel_names'][i]
            lfpsig.array_annotations['channel_ids'][i] = block.annotations['channel_ids'][i]
        self.NEO.segments[0].analogsignals.append(lfpsig)

        lfpmeta = self.NEO.annotations['lfpframe']

        lfps = self.get_by_names('lfps')
        self.LFPFrame = pd.DataFrame()
        for i, chname in enumerate(lfps.array_annotations['channel_names']):
            res = chname.split('_')
            lfp_id = f'lfp_{self.session_id}_{res[1]}_{res[2]}'
            self.LFPFrame.at[lfp_id, 'channelname'] = chname
            self.LFPFrame.at[lfp_id, 'channel_idx'] = int(i)
            self.LFPFrame.at[lfp_id, 'tetrode'] = res[1]
            self.LFPFrame.at[lfp_id, 'channel'] = res[2]

            if 'Bar' in chname:
                self.LFPFrame.at[lfp_id, 'tetrode_area'] = 'BR'
            elif 'Vis' in chname:
                self.LFPFrame.at[lfp_id, 'tetrode_area'] = 'VIS'
            elif 'Hpc' in chname:
                self.LFPFrame.at[lfp_id, 'tetrode_area'] = 'HPC'
            elif 'Per' in chname:
                self.LFPFrame.at[lfp_id, 'tetrode_area'] = 'PRH'
            else:
                self.LFPFrame.at[lfp_id, 'tetrode_area'] = 'unkown'

            for c in lfpmeta[chname].keys():
                self.LFPFrame.at[lfp_id, c] = lfpmeta[chname][c]

        for i, r in self.LFPFrame.iterrows():
            if int(r['Dsp delay']) > 0 and r['Dsp delay compensation'] == 'Disabled':
                self.LFPFrame.at[i, 'is_bp_filtered'] = True
            else:
                self.LFPFrame.at[i, 'is_bp_filtered'] = False

        self.lfp_metanames = self.LFPFrame.columns

    def _load_lfp_clean(self, lfpfile):
        reader = NixIO(filename=lfpfile)
        block = reader.read_block(lazy=True)
        lfpsig = block.segments[0].analogsignals[0]
        lfpsig.name = 'lfps_clean'
        for i in range(lfpsig.shape[1]):
            lfpsig.array_annotations['channel_names'][i] = block.annotations['channel_names'][i]
        self.NEO.segments[0].analogsignals.append(lfpsig)

        lfpmeta_clean = self.NEO.annotations['lfpframe_clean']
        lfps = self.get_by_names('lfps_clean')
        self.LFPFrame_clean = pd.DataFrame()

        for i, chname in enumerate(lfps.array_annotations['channel_names']):
            res = chname.split('_')
            lfp_id = chname
            self.LFPFrame_clean.at[lfp_id, 'channelname'] = chname
            self.LFPFrame_clean.at[lfp_id, 'channel_idx'] = int(i)
            # self.LFPFrame_clean.at[lfp_id, 'tetrode'] = res[1]
            self.LFPFrame_clean.at[lfp_id, 'channel'] = res[2]

            if 'Bar' in chname:
                self.LFPFrame_clean.at[lfp_id, 'tetrode_area'] = 'BR'
            elif 'Vis' in chname:
                self.LFPFrame_clean.at[lfp_id, 'tetrode_area'] = 'VIS'
            elif 'Hpc' in chname:
                self.LFPFrame_clean.at[lfp_id, 'tetrode_area'] = 'HPC'
            elif 'Per' in chname:
                self.LFPFrame_clean.at[lfp_id, 'tetrode_area'] = 'PRH'
            else:
                self.LFPFrame_clean.at[lfp_id, 'tetrode_area'] = 'unkown'

            for c in lfpmeta_clean.columns:
                self.LFPFrame_clean.at[lfp_id, c] = lfpmeta_clean.loc[chname.split('_clean')[0], c]

        for i, r in self.LFPFrame_clean.iterrows():
            if int(r['Dsp delay']) > 0 and r['Dsp delay compensation'] == 'Disabled':
                self.LFPFrame_clean.at[i, 'is_bp_filtered'] = True
            else:
                self.LFPFrame_clean.at[i, 'is_bp_filtered'] = False

    # ---------------------- JUNK  -------------------------

    def _get_print_text(self):
        text = 'Session from ViTa dataset\n' \
               f'\tID: {self.session_id}\n' \
               f'\t# Trails: {len(self.TrialFrame)}\n' \
               f'\t# Units : {len(self.UnitsFrame)}\n\n' \
               f'Methods (read docstring for usage) :\n' \
               f'\t-load_session(session)  #session = int or string [full name]\n' \
               f'\t-selectUnits\n' \
               f'\t-selectTrials\n' \
               f'\t-get_aligned_times()\n' \
               f'\t-bin_spikes_per_epoch\n\n' \
               f'Read class docstring for usage, or refer to example_scripts'
        return text

    def _repr_pretty_(self, pp, _):
        text = self._get_print_text()
        lines = text.split('\n')
        for ln in lines:
            pp.text(f'{ln}\n')

    def describe(self):
        text = self._get_print_text()
        lines = text.split('\n')
        for ln in lines:
            print(ln)

    def listsessions(self, verbose=False):
        """
        Summarize the available data

        :param verbose: print detected sessions
        :return: List of available session in dataset
        """
        sessions = []

        for file in os.listdir(self.dataset_path):
            if file.startswith('neo_rat') and file.endswith('.pkl'):
                sessions.append(file.split('neo_')[1].split('.pkl')[0])

        if verbose:
            print(f'Datasetpath: {self.dataset_path}')
            print('Sessions in dataset:')
            for s in sessions:
                print(f'\t{s}')

        return sessions

    def info(self, datatype):
        """
        :param datatype choose out of ['session', 'unit', 'event', 'trial']
        """
        if 'session' in datatype:
            self.listsessions(verbose=True)
        elif 'unit' in datatype:
            print('Available units meta information:')
            for i in self.unit_metanames:
                print(f'\t{i}')
            print('available unit metric info:')
            for i in self.unit_metricnames:
                print(f'\t{i}')
            print('Unit criteria:')
            for i in self.unit_criteria.items():
                print(f'\t{i[0]} - {i[1]}')
        elif 'trial' in datatype:
            print('Available trial meta info')
            for i in self.trial_metanames:
                print(f'\t{i}')
        elif 'event' in datatype:
            print('Available events:')
            for i in self.eventnames:
                print(f'\t{i}')
        elif 'lfp' in datatype:
            print('Available lfp meta:')
            for i in self.lfp_metanames:
                print(f'\t{i}')
        else:
            print('this takes session/trial/unit/event')

    @staticmethod
    def _load_pickle(file):
        # Snippet to load data dumped in pickle
        with open(file, 'rb') as f:
            data = pickle.load(f)
        return data

    @staticmethod
    def _get_operator(op):
        # Method to select an operator using a string
        operator_dict = {
            '<': operator.lt,
            '<=': operator.le,
            '==': operator.eq,
            '!=': operator.ne,
            '>': operator.gt,
            '>=': operator.ge}
        assert op in operator_dict.keys(), '{0} is not a valid operator!'.format(
            op)
        return operator_dict[op]
