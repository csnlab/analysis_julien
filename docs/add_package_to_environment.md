### Setup the analysis julien project
1. Add the `analysis_julien` folder to your python path:

        Windows 10:        
            * open 'edit system environment variables`
            * under the tab `advanced` select `Environment Variables`
            * either edit the user `path` or `pythonpath` variable, add the full
                path to the code repository
                
         Pycharm:
            * Go to interpreter settings
            * Next to the interpreter selection dropdown, click the three dots and
                select 'show all'
            * Select the environment you want to add the path to, then click
                 the 'file tree button' on the right
            * Add the full path to the list.
