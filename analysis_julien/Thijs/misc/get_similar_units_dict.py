"""
Read manual annotations csv file, naming units which should be merged

Thijs, 22/09/20
"""

import pandas as pd
from analysis_julien import LfpIO
import analysis_julien.Thijs.config as cfg

file = r'C:\surfdrive\PhD\Code\analysis_julien\analysis_julien\Thijs\unit_metrics\siliar_units.csv'
savefile = r'C:\surfdrive\PhD\Code\analysis_julien\analysis_julien\Thijs\unit_metrics\units_to_merge.py'
df = pd.read_csv(file, header=None)


units_to_merge = dict()
io = LfpIO(path=cfg.datadir)
for i, r in df.iterrows():
    date = f'{r[0]:.0f}'[:6]
    if pd.isna(r[0]):  # there are empty rows between sessions
        continue
    sid = None
    for s in io.listsessions():
        if date in s:
            sid = s

    assert sid is not None

    if sid not in units_to_merge.keys():
        units_to_merge[sid] = []

    to_merge = []

    for ii in range(4):  # max 3 units are combined in csv
        if pd.notna(r[ii]):
            to_merge.append(f'{r[ii]:.0f}')
    units_to_merge[sid].append(to_merge)

with open(savefile, 'w') as f:
    f.write('units_to_merge = {\n')
    for key in units_to_merge.keys():
        f.write(f'\t"{key}":\n')
        f.write('\t\t[\n')
        for val in units_to_merge[key]:
            f.write(f'{val}, \n')
        f.write('],\n')
    f.write('}')
