import unicodedata
from analysis_julien.Thijs import config as cfg
import os
import pickle
from analysis_julien import LightIo
from pathlib import Path
from analysis_julien.Thijs.LFP.plot_lib import plot_spc_epochs, plot_spc_modalities, plot_population_spc_epochs
import analysis_julien.Thijs.LFP.spike_triggered_phase_config as lfp_cfg
import pandas as pd
import analysis_julien.Thijs.tools as tools

min_phase_amplitude = 10  # only included phases which amplitude higher than this
bands = lfp_cfg.bands_def
epochs = lfp_cfg.epochs_def
modalities = lfp_cfg.modalities_def
pi_str = unicodedata.lookup("GREEK SMALL LETTER PI")

sess = LightIo(path=cfg.datadir)

# ----------------------------------------------------------------------
# PREPARE OUTPUT DIRECTORIES
# -----------------------------------------------------------------------
resultdir = Path(cfg.resultdir) / 'spike_triggered_phase'
printdir_modality = resultdir / 'spc_modality'
printdir_modality_bad = printdir_modality / 'bad'
printdir_epochs = resultdir / 'spc_epochs'
printdir_epochs_bad = printdir_epochs / 'bad'
pickledir = resultdir / 'pickle_stp2'
stp_table_savename = resultdir / 'stp_metrics_epochs.csv'




plot_population_spc_epochs(stp_table_savename)

