"""
For all units in the dataset, plot their intra areal locking values on theta. Also plot the spike counts in
a separate figure.
"""


import pandas as pd
import analysis_julien.Thijs.config as cfg
import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def _cap_value(val):
    if val > 1:
        val = 1
    elif val < - 1:
        val = -1
    return val


savename_fig_1 = cfg.resultdir / 'spikefield' / 'spc_epochs' / 'intra_areal_locking_kappa'
savename_fig_2 = cfg.resultdir / 'spikefield' / 'spc_epochs' / 'intra_areal_locking_counts'

table = cfg.datadir / 'results' / 'spikefield' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
df = pd.read_csv(table, header=0, index_col=0)


areas = df.tetrode_area.unique()
axes = [(1,1), (1,2), (2,1), (2,2)]

fig = make_subplots(rows=3, cols=1,
                    subplot_titles=['baseline', 'sampling', 'sampling - baseline'],
                    vertical_spacing=0.1,
                    )
fig2 = make_subplots(rows=3, cols=1,
                     subplot_titles=['baseline', 'sampling', 'sampling - baseline'],
                     vertical_spacing=0.1,
                     )

# Do the same trick for each area
for i, area in enumerate(areas):

    # Filter units
    df_cond = df.loc[
        (df.tetrode_area == area) &  # lfp must be same area a tetrode
        (df.lfp_area == area) &
        (df.theta_sampling_n > 50) &  # at least n spikes in baseline and sampling epochs
        (df.theta_baseline_n > 50) &
        (df.theta_sampling_p < 0.05) &  # only include units which showed significant locking during sampling
        (df.good_unit)  # and only good units!
    ].copy()

    # compute 'delta kappa', cap at 1 for visualization purposes
    for index, r in df_cond.iterrows():
        dk = r.theta_sampling_k - r.theta_baseline_k
        df_cond.at[index, 'dk'] = _cap_value(dk)

    # Remove weird data points
    df_cond = df_cond.loc[
        (~np.isinf(df_cond.dk)) &
        (pd.notna(df_cond.dk))
    ]

    # for every remaining unit compute the stats to visualize
    df_final = pd.DataFrame()
    for uid in df_cond.unit_id.unique():
        idx = df_cond.loc[df.unit_id == uid].dk.argmax()

        # Cap at 1 and -1 for visualization purposes
        sk = df_cond.loc[df.unit_id == uid].theta_sampling_k.values[idx]
        bk = df_cond.loc[df.unit_id == uid].theta_baseline_k.values[idx]
        df_final.at[uid, 'sampling_k'] = _cap_value(sk)
        df_final.at[uid, 'baseline_k'] = _cap_value(bk)
        df_final.at[uid, 'baseline_n'] = df_cond.loc[df.unit_id == uid].theta_baseline_n.values[idx]
        df_final.at[uid, 'sampling_n'] = df_cond.loc[df.unit_id == uid].theta_sampling_n.values[idx]
        df_final.at[uid, 'dk'] = df_cond.loc[df.unit_id == uid].dk.values[idx]

    # also get the 'delta spikecounts' per epoch
    df_final['dn'] = df_final.sampling_n - df_final.baseline_n

    # Figure 1: kappa in baseline, kappa in sampling period, delta kappa
    fig.add_trace(
        go.Violin(
            name=area,
            x=df_final.baseline_k,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Violin(
            name=area,
            x=df_final.sampling_k,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=2,
        col=1,
    )
    fig.add_trace(
        go.Violin(
            name=area,
            x=df_final.dk,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=3,
        col=1,
    )

    # Figure 2: spike count in baseline, spikecount in smapling, delta spikecount
    fig2.add_trace(
        go.Violin(
            name=area,
            x=df_final.baseline_n,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=1,
        col=1,
    )
    fig2.add_trace(
        go.Violin(
            name=area,
            x=df_final.sampling_n,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=2,
        col=1,
    )
    fig2.add_trace(
        go.Violin(
            name=area,
            x=df_final.dn,
            line_color=cfg.area_colours[area],
            box_visible=True,
        ),
        row=3,
        col=1,
    )


# Style Figure 1
fig.update_traces(orientation='h',
                   side='positive',
                   width=5,
                   points=False,
                   meanline_visible=True,
                   scalemode='count')

fig.update_xaxes(
    title_text='kappa',
)

fig.update_xaxes(
    title_text='delta Kappa (Kappa_sampling - Kappa_baseline)',
    row=3, col=1,
    # range=[-0.5, 1]
)

fig.update_layout(
    title_text=f'Spike triggered phase during epochs (Baseline and Sampling), per area <br>'
               f'intra areal locking on Theta, only good units (with significant locking during sampling and at least 50 spikes per epoch) included',
    xaxis_showgrid=False,
    xaxis_zeroline=False,
    width=1000,
    height=1200,
    # scalemode='normalized'
)
fig.update_yaxes(
    tickmode='array',
    tickvals=[],
)


# Style Figure 2
fig2.update_traces(orientation='h',
                   side='positive',
                   width=5,
                   points=False,
                   meanline_visible=True,
                   scalemode='count')

fig2.update_layout(
    title_text=f'Spike counts during epochs (Baseline and Sampling), per area',
    xaxis_showgrid=False,
    xaxis_zeroline=False,
    width=1000,
    height=1200,
)
fig2.update_xaxes(
    title_text='d-counts (counts_sampling - counts_baseline)',
    row=3, col=1
)
fig2.update_yaxes(
    tickmode='array',
    tickvals=[],
)
plotly.offline.plot(fig, filename=f'{savename_fig_1}.html')
plotly.offline.plot(fig2, filename=f'{savename_fig_2}.html')
fig.write_image(f'{savename_fig_1}.png')
fig2.write_image(f'{savename_fig_2}.png')

print('saved:')
print(savename_fig_1)
