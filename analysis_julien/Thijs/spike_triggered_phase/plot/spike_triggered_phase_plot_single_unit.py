from analysis_julien.Thijs.spike_triggered_phase.plot.plot_lib import get_plot_args, plot_spc_epochs, plot_spc_modalities
import analysis_julien.Thijs.config as cfg

pickledir = cfg.resultdir / 'spike_triggered_phase' / 'pickle_stp2'
to_plot = [
    # ('uid_1901310013', 'Barrel3', 'epoch'),
    # ('uid_1901310031', 'Hpc6', 'epoch'),
    # ('uid_1901310029', 'Hpc1', 'epoch'),
    # ('1810210020', 'Visual4_1', 'modality.pkl'),
    ('1810250018', 'Hpc5_3', 'epoch')
]

for uid, lfpid, gtype in to_plot:
    args = get_plot_args(pickledir, uid, lfpid, gtype)
    plot_spc_epochs(*args)
    # plot_spc_modalities(*args)