### ViTa - analysis
All code used for analyzing Julien's data lives in this repository. In order to develop
new code, check the `analysis_julien/example_scripts` folder for code snippets on
loading and manipulating the data.

#### Installation
Setup environment:
* `conda create -n MYENVNAME python==3.7`
* `conda activate MYENVNAME`
* `git clone git@gitlab.com:csnlab/analysis_julien.git`
* `cd analysis_julien`
* `pip install -r requirements.txt`
* `pip install -e .`

#### Code overview
Unit quality:
* ~/Noa/cell_classification_analysis.ipynb

Misc:
* ~/Thijs/plotly_meta_events.py - Overview of events in
a session
* ~/Thijs/evaluate_unit_metrics.py - Generate plots to inspect unit quality
* ~/Thijs/show_lead_correlations.py - Visualize correlations between leads
of a tetrode