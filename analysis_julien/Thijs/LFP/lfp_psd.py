from analysis_julien import LfpIO
from analysis_julien.Thijs import config as cfg
import quantities as qt
from plotly.subplots import make_subplots
from scipy import signal
import numpy as np
import plotly.graph_objects as go
from plotly.colors import sequential as seq
import os

savedir = cfg.resultdir + '/lfp_psd'
if not os.path.isdir(savedir):
    os.makedirs(savedir)

session = LfpIO(path=cfg.datadir)
session.load_session_lfp(0)

# ---------------------------------------------------------------------------
# LFP MODULATION AROUND SAMPLE START, PER MODALITY
# --------------------------------------------------------------------------
t_pre_stim = 2 * qt.s
t_post_stim = 3 * qt.s
align_event = 'sample_start'
modalities = ['T', 'V', 'M']

for sid in session.listsessions():
    print('Running: ', sid)
    session.load_session_lfp(sid)

    all_PSD = dict()

    # Generate axis titles
    ax_titles = []
    for row_idx, lfp_id in enumerate(session.LFPFrame_filtered.index):
        for col_idx, modality in enumerate(modalities):
            ax_titles.append(f'area: {session.LFPFrame_filtered.at[lfp_id, "tetrode_area"]}, mod: {modality}')
    fig = make_subplots(rows=len(session.LFPFrame_filtered.index), cols=3, subplot_titles=ax_titles,
                        vertical_spacing=0.02,
                        horizontal_spacing=0.02)

    for lfp_id in session.LFPFrame_filtered.index:

        # Compute PSD'd
        for i, modality in enumerate(modalities):
            trial_ids = session.selectTrials(modality=modality)

            # Get LFP, cut at aligned times
            aligned_lfps = session.get_aligned_lfp(
                trial_ids=trial_ids, event='trial_start',
                time_before=t_pre_stim, time_after=t_post_stim,
                lfp_id=lfp_id
            )

            # Compute PSD
            f_test, t_test, Sxx_test = signal.spectrogram(
                x=aligned_lfps[0].magnitude.flatten(),
                fs=aligned_lfps[0].sampling_rate.magnitude)
            Sxx = np.zeros((len(aligned_lfps), Sxx_test.shape[0], Sxx_test.shape[1]))
            for ii, lfp in enumerate(aligned_lfps):
                fs = lfp.sampling_rate.magnitude
                x = lfp.magnitude.flatten()
                f, t, S = signal.spectrogram(x.flatten(), fs)
                Sxx[ii, :, :] = S

            Sxx_mean = np.mean(Sxx, axis=0)
            all_PSD[f'{lfp_id}-{modality}'] = 10*np.log10(Sxx_mean)
            ev_align_idx = np.argmin(np.abs(t - t_pre_stim.magnitude))
            y_range_max_idx = np.where(f >= 100)[0][0]

    # Find minimum and maximum datapoints
    cmin, cmax = None, None
    for key in all_PSD.keys():
        if cmin is None:
            cmin = np.min(np.min(all_PSD[key]))
            cmax = np.max(np.max(all_PSD[key]))

        if np.min(np.min(all_PSD[key])) < cmin:
            cmin = np.min(np.min(all_PSD[key]))

        if np.max(np.max(all_PSD[key])) > cmax:
            cmax = np.max(np.max(all_PSD[key]))

    # Plot PSD's
    for row_idx, lfp_id in enumerate(session.LFPFrame_filtered.index):
        for col_idx, modality in enumerate(modalities):
            ax_titles.append(f'area: {session.LFPFrame_filtered.at[lfp_id, "tetrode_area"]}, mod: {modality}')
            fig.add_trace(go.Heatmap(
                z=all_PSD[f'{lfp_id}-{modality}'],
                colorscale='Viridis', zmin=cmin, zmax=cmax, colorbar=dict(
                    title='PSD', tickmode='array', tickvals=[]
                )), row=row_idx+1, col=col_idx+1)

            # Style figure
            fig.add_trace(go.Scatter(
                x=[ev_align_idx, ev_align_idx],
                y=[0, y_range_max_idx + 2],
                mode='lines',
                marker=dict(color=seq.Reds[-3],),
                showlegend=False,
            ), row=row_idx+1, col=col_idx + 1)

            if col_idx == 0:
                fig.update_yaxes(
                    tickmode='array',
                    tickvals=np.arange(f.shape[0]),
                    ticktext=np.round(f),
                    range=[0, y_range_max_idx],
                    title_text='Frequency (Hz)',
                    row=row_idx+1, col=col_idx+1
                )
            else:
                fig.update_yaxes(
                    tickmode='array',
                    tickvals=[],
                    ticktext=[],
                    range=[0, y_range_max_idx],
                    row=row_idx+1, col=col_idx+1
                )

            xtickvals = [0, ev_align_idx, t.shape[0]-1]
            if row_idx == len(session.LFPFrame_filtered.index)-1:
                fig.update_xaxes(
                    tickmode='array',
                    tickvals=xtickvals,
                    ticktext=[-t_pre_stim, 0, t_post_stim],
                    title_text=f'time from {align_event} (s)',
                    row=row_idx+1, col=col_idx+1,
                )
            else:
                fig.update_xaxes(
                    tickmode='array',
                    tickvals=[],
                )

    fig.update_layout(
        title_text=f'LFP modulation {session.session_id}',
        width=2000,
        height=2000,
    )

    savename = savedir + f'/{session.session_id}-modality-{align_event}.png'
    fig.write_image(savename)
    # plotly.offline.plot(fig)

