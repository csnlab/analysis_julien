import pandas as pd
import analysis_julien.Thijs.config as cfg
import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots

areas = ['BR', 'VIS', 'HPC', 'PRH']
bands = ['theta', 'alpha', 'beta', 'low_gamma', 'high_gamma']
table = cfg.datadir / 'results' / 'spike_triggered_phase' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
df = pd.read_csv(table, header=0, index_col=0)

titles = []
for i in range(5):
    if i == 0:
        for area in areas:
            titles.append(f'lfp: {area}')
    else:
        titles.append('')
fig = make_subplots(rows=5, cols=4, subplot_titles=titles)

savename_fig_1 = cfg.resultdir / 'spike_triggered_phase' / 'stp_all_area_bars.png'


for row_i, band in enumerate(bands):
    for col_i, lfp_area in enumerate(areas):
        for j, tetrode_area in enumerate(areas):

            # Filter units
            df_cond = df.loc[
                (df.tetrode_area == tetrode_area) &  # lfp must be same area a tetrode
                (df.lfp_area == lfp_area) &
                (df[f'{band}_sampling_n'] > 50) &  # at least n spikes in baseline and sampling epochs
                (df[f'{band}_baseline_n'] > 50) &
                (df[f'{band}_sampling_p'] < 0.05) &  # only include units which showed significant locking during sampling
                (df.good_unit)  # and only good units!
                ].copy()

            # compute 'delta kappa', cap at 1 for visualization purposes
            for index, r in df_cond.iterrows():
                dk = r.theta_sampling_k - r.theta_baseline_k
                df_cond.at[index, 'dk'] = dk # _cap_value(dk)

            # Remove weird data points
            df_cond = df_cond.loc[
                (~np.isinf(df_cond.dk)) &
                (pd.notna(df_cond.dk))
                ]

            # for every remaining unit compute the stats to visualize
            df_final = pd.DataFrame()
            for uid in df_cond.unit_id.unique():
                idx = df_cond.loc[df.unit_id == uid].dk.argmax()

                # Cap at 1 and -1 for visualization purposes
                sk = df_cond.loc[df.unit_id == uid][f'{band}_sampling_k'].values[idx]
                bk = df_cond.loc[df.unit_id == uid][f'{band}_baseline_k'].values[idx]
                df_final.at[uid, 'sampling_k'] = sk  # _cap_value(sk)
                df_final.at[uid, 'baseline_k'] = bk  # _cap_value(bk)
                df_final.at[uid, 'baseline_n'] = df_cond.loc[df.unit_id == uid][f'{band}_baseline_n'].values[idx]
                df_final.at[uid, 'sampling_n'] = df_cond.loc[df.unit_id == uid][f'{band}_sampling_n'].values[idx]
                df_final.at[uid, 'dk'] = df_cond.loc[df.unit_id == uid].dk.values[idx]

            if col_i == 0 and row_i == 0:
                showlegend = True
            else:
                showlegend = False

            fig.add_trace(
                go.Box(
                    name=f'{tetrode_area}_baseline',
                    # x=col_i,
                    y=df_final.baseline_k,
                    line_color=cfg.area_colours[tetrode_area],
                    showlegend=showlegend,
                ),
                row=row_i+1,
                col=col_i+1,

            )
            fig.add_trace(
                go.Box(
                    name=f'{tetrode_area}_sampling',
                    # x=col_i,
                    y=df_final.sampling_k,
                    line_color=cfg.area_colours[tetrode_area],
                    showlegend=False,
                ),
                row=row_i+1,
                col=col_i+1,
            )

fig.update_yaxes(range=[0,1], tickmode='array', tickvals=[0, 1])
fig.update_xaxes(tickmode='array', tickvals=[])

for i in range(5):
    fig.update_yaxes(
        title_text=bands[i],
        row=i+1,
        col=1,
    )

fig.update_layout(
    width=2000,
    height=2000,
)
# plotly.offline.plot(fig)
print('Saving:', savename_fig_1)
fig.write_image(savename_fig_1.as_posix())
fig.write_html(savename_fig_1.as_posix().split('.')[0] + '.html')