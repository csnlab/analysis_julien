import pandas as pd
import os
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_config

loaddir = lfp_config.cluster_csv_ppc
# loaddir = r'C:/users/truikes/downloads'
savedir = lfp_config.cluster_csv_ppc_all

if not os.path.isdir(savedir):
    os.makedirs(savedir)

for band in lfp_config.bands_def.keys():
    print(band)
    df = pd.DataFrame()
    saveidx = 0
    for file in os.listdir(loaddir):
        sid = file.split('-CSC')[0]
        if band in file:
            load_df = pd.read_csv(loaddir + '//' + file, index_col=0, header=0)

            for idx, r in load_df.iterrows():
                lfp_id = idx.split('_uid')[0]
                uid = idx.split('clean_')[1]

                for key, item in r.items():
                    # df.at[lfp_id, f'{uid}_{key}'] = item
                    df.at[saveidx, 'ppc0'] = item
                    df.at[saveidx, 'epoch'] = key.split('_')[0]
                    df.at[saveidx, 'session'] = sid
                    df.at[saveidx, 'lfp'] = lfp_id
                    df.at[saveidx, 'uid'] = uid
                    saveidx += 1
    savename = savedir + f'/{band}.csv'
    df.to_csv(savename)
    print('saved')



