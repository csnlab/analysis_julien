import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import analysis_julien.Thijs.tools as tools
from analysis_julien import Io
import analysis_julien.Thijs.config as cfg
import pandas as pd


io = Io(path=cfg.datadir)

df = tools.load_data('uf_area')
if df is None:
    df = pd.DataFrame()
    for sid in io.session_ids:
        print(sid)
        io.load_session(sid)
        io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
        dfn = io.unit_df.copy()
        dfn['session_id'] = sid
        dfn['animal_id'] = io.animal_id
        df = pd.concat([df, dfn])
    tools.save_data(name='uf_area', data=df)

fig = make_subplots(rows=5, cols=4,
                    specs=[[{'type': 'domain'} for _ in range(4)] for _ in range(5)],
                    subplot_titles=['all', 'rat-14', 'rat-16', ''] + io.session_ids,
                    horizontal_spacing=0.05, vertical_spacing=0.05)

# Chart for all units
df2 = df.loc[df.good_unit]
labels, values, marker_colors = [], [], []
for lbl, val in df2.groupby(['recording_group']).size().items():
    labels.append(lbl)
    values.append(val)
    marker_colors.append(cfg.area_colours[lbl])
fig.add_trace(go.Pie(
    labels=labels,
    values=values,
    marker_colors=marker_colors,
    textinfo='value',
), row=1, col=1)

# Chart for all units from rat 14
df2 = df.loc[df.good_unit & (df.animal_id == 'rat-14')]
labels, values, marker_colors = [], [], []
for lbl, val in df2.groupby(['recording_group']).size().items():
    labels.append(lbl)
    values.append(val)
    marker_colors.append(cfg.area_colours[lbl])
fig.add_trace(go.Pie(
    labels=labels,
    values=values,
    marker_colors=marker_colors,
    textinfo='value',
), row=1, col=2)

# Chart for all units from rat 16
df2 = df.loc[df.good_unit & (df.animal_id == 'rat-16')]
labels, values, marker_colors = [], [], []
for lbl, val in df2.groupby(['recording_group']).size().items():
    labels.append(lbl)
    values.append(val)
    marker_colors.append(cfg.area_colours[lbl])
fig.add_trace(go.Pie(
    labels=labels,
    values=values,
    marker_colors=marker_colors,
    textinfo='value',
), row=1, col=3)


# Charts per session
row, col = 2, 1
for sid in df.session_id.unique():
    df2 = df.loc[(df.session_id == sid) & df.good_unit]
    labels, values, marker_colors = [], [], []
    for lbl, val in df2.groupby(['recording_group']).size().items():
        labels.append(lbl)
        values.append(val)
        marker_colors.append(cfg.area_colours[lbl])
    fig.add_trace(go.Pie(
        labels=labels,
        values=values,
        marker_colors=marker_colors,
        textinfo='value',
    ), row=row, col=col)

    col += 1
    if col > 4:
        col = 1
        row += 1
plotly.offline.plot(fig)

fig.write_html((cfg.resultdir_quality / f'unit_counts_per_area.html').as_posix())