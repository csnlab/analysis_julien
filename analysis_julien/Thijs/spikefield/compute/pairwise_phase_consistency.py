import pandas as pd
from analysis_julien import Io
from distributed import Client
from dask_jobqueue import SLURMCluster
import os
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_config
from dask.distributed import fire_and_forget
import time
from elephant.signal_processing import butter, hilbert
from elephant.phase_analysis import spike_triggered_phase, pairwise_phase_consistency
import analysis_julien.Thijs.config as cfg


cluster_scaling = 50
# savedir = cfg.resultdir.as_posix()  # for local

savedir = lfp_config.cluster_csv_ppc

if not os.path.isdir(savedir):
    os.makedirs(savedir)

if not os.path.isdir('/home/truikes/slurmoutput/PPC'):
    os.makedirs('/home/truikes/slurmoutput/PPC')


def get_ppc(session_id, lfp_id, freq_band, outputname):
    io_worker = Io(path=lfp_config.cluster_datadir, read_lfp=True)
    # io_worker = Io(path=cfg.datadir, read_lfp=True) # for local
    io_worker.load_session(session_id, read_lfp=True)
    io_worker.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)

    lfp = io_worker.get_object(lfp_id)
    lfp_bp = butter(lfp,
                    lowpass_frequency=lfp_config.bands_def[freq_band]['highcut'],
                    highpass_frequency=lfp_config.bands_def[freq_band]['lowcut'])
    lfp_hb = hilbert(lfp_bp)

    epoch_times = dict()
    for epoch, specs in lfp_config.epochs_def.items():
        times_pre, times_post = io_worker.get_aligned_times(
            trial_ids=io_worker.trial_ids,  # use all trials
            **specs,
        )
        epoch_times[epoch] = dict(times_pre=times_pre, times_post=times_post)

    ppc_df = pd.DataFrame()
    for uid in io_worker.unit_ids:
        sp = io_worker.get_object(uid)

        for epoch in lfp_config.epochs_def.keys():
            # print(uid, epoch)

            lfp_cut = []
            sp_cut = []
            for t0, t1 in zip(epoch_times[epoch]['times_pre'], epoch_times[epoch]['times_post']):
                sp_loop = sp.time_slice(t0, t1)
                if sp_loop.shape[0] > 0:
                    sp_cut.append(sp_loop)
                    lfp_cut.append(lfp_hb.time_slice(t0, t1))

            if len(sp_cut) == 0:
                ppc = -999
            else:
                try:
                    phases, amps, times = spike_triggered_phase(
                        hilbert_transform=lfp_cut,
                        spiketrains=sp_cut,
                        interpolate=False,
                    )
                except:
                    phases = None

                try:
                    if phases is not None:
                        ppc = pairwise_phase_consistency(phases, 'ppc0')
                    else:
                        ppc = -998
                except:
                    ppc = -997
            ppc_df.at[f'{lfp_id}_{uid}', f'{epoch}_ppc0'] = ppc
            # print(ppc)

    ppc_df.to_csv(outputname)


# Setup dask
cluster = SLURMCluster(
    name='PPC',
    cores=1,
    walltime='2:00:00',
    local_directory='/home/truikes/slurmoutput/PPC',
    job_cpu=1,
    memory='8GB',
    job_extra=['- /data/truikes/slurm/worker/PPC-worker-%j.out', '--mem-per-cpu=8G']
)
cluster.scale(cluster_scaling)

client = Client(cluster)

io = Io(path=lfp_config.cluster_datadir, read_lfp=True)

# io = Io(path=cfg.datadir, read_lfp=True)  # for local
for sid in io.session_ids:
    io.load_session(sid, read_lfp=True)

    for lid in io.lfp_clean_ids:
        for band in lfp_config.bands_def.keys():
            savename = savedir + f'/{sid}-{lid}-{band}.csv'

            task_args = [sid, lid, band, savename]
            task = client.submit(get_ppc, *task_args)
            fire_and_forget(task)
            # get_ppc(*task_args)  # for local

while True:
    print(time.time())
    time.sleep(60)

print('Done')