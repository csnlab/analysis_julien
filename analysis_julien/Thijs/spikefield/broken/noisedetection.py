# BROKEN

# """
#
# """
# from analysis_julien import IO
# from analysis_julien import LightIo
# from analysis_julien.Thijs import config as cfg
# from analysis_julien.Thijs.tools import load_data, save_data
# import quantities as qt
# import numpy as np
# from elephant.spectral import welch_psd
# import plotly.graph_objects as go
# import os
# import plotly
# savedir = cfg.resultdir / 'lfp_noisedetection'
# from plotly.subplots import make_subplots
#
#
# freq_resolution = 5*qt.Hz
# max_freq = 120*qt.Hz
# winlen = 200*qt.ms
# step_size = 20*qt.ms
# zmax = 1000  # amplitude cutoff for PSD plotting
#
#
# def get_bins(t_start, t_stop, winlength, stepsize):
#     assert winlength.magnitude % 2 == 0
#     win_halfwidth = (winlength.rescale('ms') / 2)
#     bin_centres = np.arange(
#         start=t_start.rescale('ms') + win_halfwidth,
#         stop=t_stop.rescale('ms') - win_halfwidth,
#         step=stepsize.rescale('ms')
#     ) * qt.ms
#     bin_starts = bin_centres - win_halfwidth
#     bin_ends = bin_centres + win_halfwidth
#     return bin_starts, bin_centres, bin_ends
#
#
# print('Loading io')
# io = LightIo(path=cfg.datadir, read_lfp=True)
# for sid in io.session_ids:
#     print('Running session: ', sid)
#     io.load_session(sid)
#
#     for lfp_id in io.lfp_clean_df.index:
#         dumpname = f'noise_detetion-{lfp_id}-{zmax}'
#         dumpdata = None
#
#         times_pre, times_post = io.get_aligned_times(
#             trial_ids=io.select_trials(all=True),
#             time_before=1*qt.s,
#             time_after=2*qt.s,
#             event='trial_start'
#         )
#
#         lfp = io.get_object(lfp_id)
#         lfps = [lfp.time_slice(t0, t1) for t0, t1 in zip(times_pre, times_post)]
#
#         if dumpdata is None:
#             test_lfp = lfps[0]
#
#             # Do 1 time run to get expected output shapes
#             bs, bc, be = get_bins(test_lfp.t_start, test_lfp.t_stop,
#                                   winlength=winlen, stepsize=step_size)
#             nbins = bc.shape[0]
#             freqs, test_amps = welch_psd(
#                 signal=test_lfp.time_slice(bs[0], be[0]),
#                 frequency_resolution=freq_resolution
#             )
#
#             # output in form trials x time x freq
#             amplitudes = np.empty((len(lfps), nbins, test_amps.shape[1]))
#             for i_lfp, lfp in enumerate(lfps):
#                 for i_bin in range(nbins):
#                     bs, bc, be = get_bins(lfp.t_start, lfp.t_stop,
#                                           winlength=winlen,
#                                           stepsize=step_size,)
#                     freqs, amps = welch_psd(
#                         signal=lfp.time_slice(bs[i_bin], be[i_bin]),
#                         frequency_resolution=freq_resolution,
#                     )
#                     amplitudes[i_lfp, i_bin, :] = amps
#             save_data(data=[freqs, amplitudes], name=dumpname)
#         else:
#             freqs, amplitudes = dumpdata
#
#         amp_plot = amplitudes.flatten()
#         # amp_plot = amp_plot[amp_plot > 50]
#         # fig = go.Figure(
#         #     data=go.Histogram(
#         #         x=amp_plot,
#         #         xbins=dict(
#         #             start=0,
#         #             end=1000,
#         #             size=50,
#         #         ),
#         #         autobinx=False,
#         #     )
#         # )
#         print(amp_plot.shape)
#         fig = make_subplots(rows=3, cols=1)
#         fig.add_trace(go.Histogram(
#                 x=amp_plot,
#                 xbins=dict(
#                     start=0,
#                     end=1000,
#                     size=50,
#                 ),
#                 autobinx=False,
#             ), row=1, col=1)
#
#         fig.add_trace(
#             go.Scatter(y=np.max(amp_plot, axis=0)), row=2, col=1,
#         )
#         fig.add_trace(
#             go.Scatter(y=np.max(amp_plot, axis=1)), row=3, col=1)
#
#         # plotly.offline.plot(fig)
#         # zmax = np.amax(amplitudes)
#         if not os.path.isdir(savedir):
#             os.makedirs(savedir)
#         for i in range(len(lfps)):
#             amps_plot = np.squeeze(amplitudes[i, :, :].T)
#             fig = go.Figure(data=go.Heatmap(
#                     z=amps_plot,
#                     colorscale='Viridis',
#                     zmin=0, zmax=zmax
#             ))
#             max_idx = np.where(freqs <= 120)[0][-1]
#             fig.update_yaxes(
#                 tickmode='array',
#                 tickvals=np.arange(freqs.shape[0]),
#                 ticktext=freqs,
#                 range=[0, max_idx]
#             )
#             s1 = int(amps_plot.shape[1] * 0.6)
#             noise_val = np.max(np.mean(amps_plot, axis=1))
#             subsavedir = f'{savedir}/{io.session_id}-{zmax}'
#             if noise_val > 100000:
#                 subsavedir += '/bad'
#             else:
#                 subsavedir += '/good'
#             fig.update_layout(title_text=noise_val)
#
#             if not os.path.isdir(subsavedir):
#                 os.makedirs(subsavedir)
#             savename = f'{subsavedir}/{lfp_id}-{i}-{noise_val:.0f}.png'
#             fig.write_image(savename)
#             print(savename)
#             # plotly.offline.plot(fig)
