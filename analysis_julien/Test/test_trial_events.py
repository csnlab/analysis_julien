from analysis_julien import Io
import pandas as pd
from analysis_julien.Thijs import tools
# --------------------------- DATA REQUIREMENTS --------------------------------------

# Definitions the data should meet

# io = Io(path=cfg.datadir)
io = Io(path=r'E:\hbp-data-002061\dataset')

modalities = ['T', 'V', 'M', 'P']
standard_events = ['trial_start', 'sample_start', 'retraction', 'poke',
                   'video_trigger', 'trial_end']
outcome = pd.DataFrame()

pbar = tools.PBar('reading data', 40, len(io.session_ids))

for sid in io.session_ids:
    try:
        io.load_session(sid)
    except:
        print(f'cant load {sid}')
        continue

    try:
        P_tids = io.select_trials(modality='P')
    except:
        print(f'cant read P ({sid})')
        continue

    _, ts_tids = io.get_event_times(io.trial_ids, 'trial_start',
                                    return_ids=True, verbose=False)
    outcome.at[io.session_id, 'n_trials'] = len(ts_tids)

    for se in standard_events:
        # Add modality independent evaluations
        _, ev_tids = io.get_event_times(io.trial_ids, se,
                                        return_ids=True, verbose=False)
        outcome.at[io.session_id, f'n_{se}'] = len(ev_tids)
        if se == 'sample_start':
            outcome.at[io.session_id, f'n_{se}_missing'] = len(ts_tids) - len(ev_tids) - len(P_tids)
        else:
            outcome.at[io.session_id, f'n_{se}_missing'] = len(ts_tids) - len(ev_tids)

    # Add modality dependent evaluations
    for mod in modalities:
        tids = io.select_trials(modality=mod)
        outcome.at[io.session_id, f'n_{mod}_trials'] = int(len(tids))
        res = io.get_event_times(tids, 'tactile_start',
                                 return_ids=True, verbose=False)
        if mod in ['T', 'M']:
            outcome.at[io.session_id, f'n_{mod}_tactile_start_missing'] = int(len(tids) - res[0].shape[0])
        elif mod in ['V', 'P']:
            outcome.at[io.session_id, f'n_{mod}_tactile_start_found'] = int(res[0].shape[0])

        # res = io.get_event_times(tids, 'tactile_end',
        #                      return_ids=True, verbose=False)
        # if mod in ['T', 'M']:
        #     outcome.at[io.session_id, f'n_{mod}_tactile_end_missing'] = int(len(tids) - res[0].shape[0])
        # elif mod in ['V', 'P']:
        #     outcome.at[io.session_id, f'n_{mod}_tactile_end_found'] = int(res[0].shape[0])

        res = io.get_event_times(tids, 'visual_start',
                                 return_ids=True, verbose=False)
        if mod in ['T', 'P']:
            outcome.at[io.session_id, f'n_{mod}_visual_start_found'] = int(res[0].shape[0])
        elif mod in ['V', 'M']:
            outcome.at[io.session_id, f'n_{mod}_visual_start_missing'] = int(len(tids) - res[0].shape[0])

    pbar.print_1()

c_missing = [c for c in outcome.columns if 'missing' in c]


savefile = io.dataset_path / 'test' / 'test_trial_event_missing.csv'
if not savefile.parent.is_dir():
    savefile.parent.mkdir(parents=True)
outcome[c_missing].to_csv(savefile.as_posix())
savefile = io.dataset_path / 'test' / 'test_trial_event.csv'
outcome.to_csv(savefile.as_posix())