"""
STP processing pipeline:
First, on the cluster, run:
* spike_triggered_phase_1 (using csn_spike_triggered_phase_1.slurm)
* spike_triggered_phase_2 (using csn_spike_triggered_phase_2.slurm)

Then run the plot function (locally):
* plot_spike_triggered_phase

executed with ~/Thijs/slurm/csn_spike_triggered_phase_1.slurm
22/09/20
dataset: V3_WIP
"""

# from analysis_julien.Thijs.config import datadir, resultdir, unit_quality_thresholds
from analysis_julien import LightIo
import os


# Settings while running on cluster
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_config

datadir = lfp_config.cluster_datadir
pickledir_lfp = lfp_config.cluster_pickle_lfp
pickledir_stp = lfp_config.cluster_pickle_stp1
cluster_scaling = 30  # nr of parallel jobs on cluster


# --------------------- DEFINITIONS ----------------------
# spike_triggered_phase frequency bands
bands = lfp_config.bands_def
min_phase_amplitude = lfp_config.min_phase_amplitude


# --------------------- MAIN ---------------------------
io = LightIo(path=datadir, read_lfp=True)


# sessions = [session.listsessions()[0]]
sessions = io.session_ids
missing = []
found = []
for sid in sessions:
    print(sid, len(sessions))
    io.load_session(sid, read_lfp=False)
    io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    lfpframe_reref_lfps = io.lfp_clean_df

    for unit_id in io.unit_df.index:

        if not io.get_meta(unit_id, 'good_unit'):
            continue

        for lfp_id in lfpframe_reref_lfps.index:
            # Create stp job
            savename = pickledir_stp + f'/{sid}_{unit_id}_{lfp_id}_stp.pkl'
            if not os.path.isfile(savename):
                # print(savename)
                missing.append(savename)
            else:
                found.append(savename)

print('n missing: ', len(missing))
print('n found: ', len(found))