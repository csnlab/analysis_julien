"""
Visual recorded neuralynx events in a session using the
plotly library, shows event times as saved in dataset

Thijs 10/06/20
"""
from plotly.colors import sequential as seq
from analysis_julien import IO
import plotly
import os
import plotly.graph_objects as go
import numpy as np
import pandas as pd
import seaborn as sns
import analysis_julien.Thijs.config as cfg

events_palette = {'trial_start': sns.xkcd_rgb['light red'],
                  'sample_start': sns.xkcd_rgb['amber'],
                  'sample_end': sns.xkcd_rgb['amber'],
                  'retraction': sns.xkcd_rgb['aqua marine'],
                  'video_trigger': sns.xkcd_rgb['light blue'],
                  'tactile_start': sns.xkcd_rgb['light green'],
                  'tactile_end': sns.xkcd_rgb['light green'],
                  'visual_start': sns.xkcd_rgb['light purple'],
                  'visual_end': sns.xkcd_rgb['light purple'],
                  'reward_start': sns.xkcd_rgb['light orange'],
                  'lick_start': sns.xkcd_rgb['bright cyan'],
                  'poke': sns.xkcd_rgb['bright cyan'],
                  'left_poke': sns.xkcd_rgb['bright cyan'],
                  'right_poke': sns.xkcd_rgb['bright cyan'],
                  'sample_1_restored':  sns.xkcd_rgb['amber'],
                  'sample_1_broken':   sns.xkcd_rgb['amber'],
                  'sample_2_restored': sns.xkcd_rgb['amber'],
                  'sample_2_broken': sns.xkcd_rgb['amber'],

}


events = [
    'trial_start',
    # 'sample_start',
    'tactile_start',
    'visual_start',
    'video_trigger',
    'tactile_end',
    'visual_end',
    # 'sample_end',
    # 'left_poke',
    # 'right_poke',
    # 'poke',
    'retraction',
    # 'sample_1',
    'sample_2',
]


# session = IO(path='C:/tmp')
session = IO(path=cfg.datadir)

for i, sname in enumerate(session.listsessions()):
    print(f'Starting {sname} ({i+1}/{len(session.listsessions())})')
    session.load_session(sname)
    Block = session.NEO
    animal = session.animal_id
    sid = session.session_id
    plotdir = f'{session.dataset_path}/data_overview/'
    if not os.path.isdir(plotdir):
        os.mkdir(plotdir)

    names = pd.DataFrame()

    i = 0
    plotdata = []
    for ev in Block.segments[0].events:
        if ev.name not in events:
            continue

        if ev.name in ['sample_1', 'sample_2']:
            if not ev.annotations['valid']:
                continue
            ev.name = f'{ev.name}_{ev.annotations["state"]}'

        if ev.name == 'left_poke':
            mltpl = 1.2
        elif ev.name == 'right_poke':
            mltpl = 0.8
        else:
            mltpl = 1

        times = ev.times.magnitude
        if ev.name == 'trial_end':
            times = times[:-1]

        if ev.name == 'trial_start':
            htext = []
            for i, l in enumerate(ev.labels):
                hstr = f'{ev.times[i]:.2f}s: {ev.name} ({l})<br>'
                for tmname in session.trial_metanames:
                    mval = session.get_meta(l, tmname)
                    hstr += f'{tmname}: {mval}<br>'
                htext.append(hstr)

        else:
            htext = []
            for i in range(len(times)):
                if 'xxxx' in ev.labels[i] or 'sample' in ev.name:
                    tstart = times[i]
                else:
                    tstart = session.get_event_times(event_name='trial_start', trial_ids=ev.labels[i])
                    tstart = tstart.magnitude[0]
                htext.append(
                    f'{(times[i] - tstart) * 1000:.0f}ms : {ev.name} {ev.labels[i]}'
                )

        plotdata.append(
            go.Scatter(
                x=times,
                y=np.ones(times.shape)*mltpl,
                mode='markers',
                hoverinfo='text',
                hovertext=htext,
                name=f'{ev.name}',
                marker=dict(
                    size=100,
                    symbol='line-ns',
                    line_width=2,
                    color=events_palette[ev.name],
                    line_color=events_palette[ev.name]
                )
            )
        )
    layout = go.Layout(
        plot_bgcolor=seq.Greys[-1],
        paper_bgcolor=seq.Greys[-4],
        title=f"Meta events in block {animal} {sname}!",
        xaxis=dict(
            title='time (s)',
            showgrid=False,
        ),
        yaxis=dict(
            showgrid=False,
            fixedrange=True,
            range=[0.5, 1.5],
        ),
    )
    fig = go.Figure(data=plotdata, layout=layout)
    plotly.offline.plot(fig, filename=f'{plotdir}meta_events_{animal}_{sname}.html')