from analysis_julien import IO
from analysis_julien.Thijs import config as cfg
from analysis_julien.Thijs.tools import load_data, save_data
import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from plotly.colors import sequential as seq


savedir = cfg.resultdir_quality
if not savedir.is_dir():
    savedir.mkdir(parents=True)

# Retrieve unit dataframes for all sessions
unitsdataframe = load_data('all_units_dataframe')

# unitsdataframe = None
if unitsdataframe is None:
    dataset = IO(path=cfg.datadir)
    all_unitframes = []
    for sid in dataset.listsessions():
        dataset.load_session(sid)
        # Update unit criteria to be looser
        dataset.set_unit_criteria(**cfg.unit_quality_thresholds)
        all_unitframes.append(dataset.UnitsFrame)
    unitsdataframe = pd.concat(all_unitframes)
    save_data(unitsdataframe, 'all_units_dataframe')

# Only analyse 'good' units
unitsdataframe = unitsdataframe.loc[unitsdataframe.good_unit]

clr_pu = seq.Purples[-3]
clr_red = seq.Reds[-4]

fig = make_subplots(
    rows=3,
    cols=1,
    subplot_titles=['Peak to Valley', 'Repolarization Slope', 'Firing Rate']
)

# Plot peak to valley distribution
fig.add_histogram(
    x=unitsdataframe.peak_to_valley * 1000,  # to ms
    xbins=dict(
        start=0,
        size=0.02,
    ),
    row=1,
    col=1,
    showlegend=False,
    marker_color=clr_pu,
    opacity=0.9,
)

fig.add_trace(go.Scatter(
    x=np.ones(2) * cfg.short_broad_spike_threshold,
    y=[0, 40],
    showlegend=False,
    line=dict(
        color=clr_red,
    ),
), row=1, col=1)

fig.update_xaxes(
    tickmode='array',
    tickvals=[0, 0.25, cfg.short_broad_spike_threshold, 0.5, 0.75, 1],
    title_text='time (ms)',
    row=1,
    col=1,
)
fig.update_yaxes(
    range=[0, 40],
    tickmode='array',
    tickvals=[],
    row=1,
    col=1,
)

# --------------------------------
# Plot repolarization slope
# ------------------------------
fig.add_histogram(
    x=unitsdataframe.repolarization_slope / 1000000,  # idk units
    xbins=dict(
        start=0,
        size=0.02,
        end=1.5
    ),
    row=2,
    col=1,
    showlegend=False,
    marker_color=clr_pu,
    opacity=0.9,
)

# fig.add_trace(go.Scatter(
#     x=np.ones(2) * cfg.short_broad_spike_threshold,
#     y=[0, 40],
#     showlegend=False,
# ), row=1, col=1)

fig.update_xaxes(
    tickmode='array',
    # tickvals=[0, 0.25, cfg.short_broad_spike_threshold, 0.5, 0.75, 1],
    title_text='?',
    row=2,
    col=1,
)
fig.update_yaxes(
    # range=[0, 40],
    tickmode='array',
    tickvals=[],
    row=2,
    col=1,
)

# --------------------------------
# Plot firing_rate
# ------------------------------
fig.add_histogram(
    x=unitsdataframe.firing_rate,
    xbins=dict(
        start=0,
        size=0.5,
        end=20,
    ),
    row=3,
    col=1,
    showlegend=False,
    marker_color=clr_pu,
    opacity=0.9,
)

fig.update_xaxes(
    tickmode='array',
    # tickvals=[0, 0.25, cfg.short_broad_spike_threshold, 0.5, 0.75, 1],
    title_text='Firing Rate (Hz)',
    row=3,
    col=1,
)
fig.update_yaxes(
    range=[0, 120],
    tickmode='array',
    tickvals=[],
    row=3,
    col=1,
)

fig.add_trace(go.Scatter(
    x=np.ones(2) * cfg.slow_fast_firing_threshold,
    y=[0, 120],
    showlegend=False,
    line=dict(
        color=clr_red,
    ),
), row=3, col=1)

fig.update_layout(
    title_text=f'Metric distributions for {len(unitsdataframe)} units',
    autosize=False,
    width=1200,
    height=1200,
)

savename = savedir / 'celltype_classification_distributions_1.png'
fig.write_image(savename.as_posix())


# --------------------------------------------------------
# PLOT CLASSIFICATION DISTRIBUTION
# --------------------------------------------------------

for i, r in unitsdataframe.iterrows():
    if r.peak_to_valley * 1000 <= cfg.short_broad_spike_threshold and \
        r.firing_rate >= cfg.slow_fast_firing_threshold:
        unitsdataframe.at[i, 'cell_type'] = 'I'

    elif r.peak_to_valley * 1000 > cfg.short_broad_spike_threshold and \
        r.firing_rate < cfg.slow_fast_firing_threshold:
        unitsdataframe.at[i, 'cell_type'] = 'E'

    else:
        unitsdataframe.at[i, 'cell_type'] = 'UK'


col_ex = seq.algae[4]
col_inh = seq.Reds[4]
col_uk = seq.Greys[4]

fig = go.Figure()
idx_I = np.where(unitsdataframe.cell_type.values == 'I')[0]
fig.add_trace(
    go.Scatter(
        name='Inhibitory',
        mode='markers',
        x=unitsdataframe.firing_rate[idx_I],
        y=unitsdataframe.peak_to_valley[idx_I] * 1000,
        hovertext=unitsdataframe.index[idx_I],
        line=dict(color=col_inh)
    )
)

idx_E = np.where(unitsdataframe.cell_type.values == 'E')[0]
fig.add_trace(
    go.Scatter(
        name='Excitatory',
        mode='markers',
        x=unitsdataframe.firing_rate[idx_E],
        y=unitsdataframe.peak_to_valley[idx_E] * 1000,
        hovertext=unitsdataframe.index[idx_E],
        line=dict(color=col_ex)
    )
)

idx_UK = np.where(unitsdataframe.cell_type.values == 'UK')[0]
fig.add_trace(
    go.Scatter(
        name='unkown',
        mode='markers',
        x=unitsdataframe.firing_rate[idx_UK],
        y=unitsdataframe.peak_to_valley[idx_UK] * 1000,
        hovertext=unitsdataframe.index[idx_UK],
        line=dict(color=col_uk)
    )
)

fig.update_xaxes(
    title_text='firing_rate',
    range=[0, 40],
)

fig.update_yaxes(
    title_text='peak_to_valley',
    range=[0, 1]
)

fig.update_layout(
    title_text=f'Metric distributions for {len(unitsdataframe)} units',
    autosize=False,
    width=600,
    height=600,
)

savename = savedir / 'celltype_classification_distributions_2.png'
fig.write_image(savename.as_posix())
