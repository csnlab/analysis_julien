"""
For all units in the dataset, plot their intra areal locking values on theta. Also plot the spike counts in
a separate figure.
"""

import pandas as pd
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_cfg
import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def _cap_value(val):
    if val > 1:
        val = 1
    elif val < - 1:
        val = -1
    return val


table = cfg.datadir / 'results' / 'spikefield' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
df = pd.read_csv(table, header=0, index_col=0)

areas = df.tetrode_area.unique()
bands = lfp_cfg.bands_def.keys()
modalities = ['T', 'V', 'M', 'P']
axes = [(1, 1), (2, 1), (3, 1), (4, 1)]

for band in bands:
    if band == 'delta':
        continue
    savename_fig_1 = cfg.resultdir / 'spikefield' / 'spc_epochs' / f'intra_areal_locking_modality_{band}'

    fig = make_subplots(rows=4, cols=1,
                        subplot_titles=areas,
                        vertical_spacing=0.1,
                        )

    # Do the same trick for each area
    for i, area in enumerate(areas):

        # Filter units
        df_cond = df.loc[
            (df.tetrode_area == area) &  # lfp must be same area a tetrode
            (df.lfp_area == area) &
            # (df.theta_sampling_n > 50) &  # at least n spikes in baseline and sampling epochs
            # (df.theta_baseline_n > 50) &
            # (df.theta_ < 0.05) &  # only include units which showed significant locking during sampling
            (df.good_unit)  # and only good units!
            ].copy()

        # for every remaining unit compute the stats to visualize
        df_final = pd.DataFrame()
        for uid in df_cond.unit_id.unique():
            uid_df = df_cond.loc[df.unit_id == uid]

            all_mod = []
            for mod in modalities:
                all_mod.append(uid_df[f'{band}_{mod}_k'].max())

            best_mod = np.argmax(all_mod) # use hte modality wheron this unit is locking best
            idx = uid_df[f'{band}_{modalities[best_mod]}_k'].argmax()

            # if uid_df[f'{band}_{modalities[best_mod]}_p'].values[idx] < 2:
            for mod in modalities:
                if uid_df[f'{band}_{mod}_p'].values[idx] < 0.05:
                    df_final.at[uid, f'sampling_{mod}'] = _cap_value(uid_df[f'{band}_{mod}_k'].values[idx])

        # Figure 1: kappa in baseline, kappa in sampling period, delta kappa
        for mod in modalities:
            if f'sampling_{mod}' not in df_final.keys():
                continue
            fig.add_trace(
                go.Violin(
                    name=mod,
                    x=df_final[f'sampling_{mod}'],
                    line_color=cfg.modality_colors[mod],
                    box_visible=True,
                ),
                row=axes[i][0],
                col=axes[i][1],
            )

    # Style Figure 1
    fig.update_traces(orientation='h',
                      side='positive',
                      width=5,
                      points=False,
                      meanline_visible=True,
                      scalemode='count')

    fig.update_xaxes(
        title_text='kappa',
        range=[0, 1],
    )

    fig.update_layout(
        title_text=f'Spike triggered phase during epochs (Baseline and Sampling), per area <br>'
                   f'intra areal locking on {band}, only good units (with significant locking during sampling and at least 50 spikes per epoch) included',
        xaxis_showgrid=False,
        xaxis_zeroline=False,
        width=1000,
        height=1200,
        # scalemode='normalized'
    )
    fig.update_yaxes(
        tickmode='array',
        tickvals=[],
    )

    plotly.offline.plot(fig, filename=f'{savename_fig_1}.html')
    fig.write_image(f'{savename_fig_1}.png')
    print(savename_fig_1)
