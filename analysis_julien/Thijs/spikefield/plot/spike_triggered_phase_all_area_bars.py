import pandas as pd
import analysis_julien.Thijs.config as cfg
import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots

areas = ['BR', 'VIS', 'HPC', 'PRH']
bands = ['theta', 'alpha', 'beta', 'low_gamma', 'high_gamma']
table = cfg.datadir / 'results' / 'spikefield' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
units_df_savename = cfg.resultdir / 'units_df.csv'
units_df = pd.read_csv(units_df_savename, header=0, index_col=0)
df = pd.read_csv(table, header=0, index_col=0)

titles = []
for i in range(5):
    if i == 0:
        for area in areas:
            titles.append(f'lfp: {area}')
    else:
        titles.append('')

row_heights = []
for i in range(10):
    if i % 2 == 0:
        row_heights.append(3/40)
    else:
        row_heights.append(1/40)

specs = []
for row in range(10):
    row_specs = []
    for col in range(16):
        if row % 2 == 0 and col % 4 != 0:
            row_specs.append(None)
            continue

        ax_specs = dict()

        if row % 2 == 0:
            ax_specs['rowspan'] = 1
            ax_specs['colspan'] = 4
            ax_specs['type'] = 'scatter'

        else:
            ax_specs['rowspan'] = 1
            ax_specs['colspan'] = 1
            ax_specs['type'] = 'domain'

        row_specs.append(ax_specs)
    specs.append(row_specs)

fig = make_subplots(rows=10, cols=16, subplot_titles=titles,
                    row_heights=row_heights, specs=specs,
                    vertical_spacing=0.01)

savename_fig_1 = cfg.resultdir / 'spikefield' / 'stp_all_area_bars.png'


for row_i, band in enumerate(bands):
    for col_i, lfp_area in enumerate(areas):
        for j, tetrode_area in enumerate(areas):

            # Filter units
            df_cond = df.loc[
                (df.tetrode_area == tetrode_area) &  # lfp must be same area a tetrode
                (df.lfp_area == lfp_area) &
                (df[f'{band}_sampling_n'] > 50) &  # at least n spikes in baseline and sampling epochs
                (df[f'{band}_baseline_n'] > 50) &
                (df[f'{band}_sampling_p'] < 0.05) &  # only include units which showed significant locking during sampling
                (df['unit_tetrode'] != df['lfp_tetrode']) &
                (df.good_unit)  # and only good units!
                ].copy()

            # compute 'delta kappa', cap at 1 for visualization purposes
            for index, r in df_cond.iterrows():
                dk = r.theta_sampling_k - r.theta_baseline_k
                df_cond.at[index, 'dk'] = dk  # _cap_value(dk)

            # Remove weird data points
            df_cond = df_cond.loc[
                (~np.isinf(df_cond.dk)) &
                (pd.notna(df_cond.dk))
                ]

            # for every remaining unit compute the stats to visualize
            df_final = pd.DataFrame()
            for uid in df_cond.unit_id.unique():
                idx = df_cond.loc[df.unit_id == uid].dk.argmax()

                # Cap at 1 and -1 for visualization purposes
                sk = df_cond.loc[df.unit_id == uid][f'{band}_sampling_k'].values[idx]
                bk = df_cond.loc[df.unit_id == uid][f'{band}_baseline_k'].values[idx]
                df_final.at[uid, 'sampling_k'] = sk  # _cap_value(sk)
                df_final.at[uid, 'baseline_k'] = bk  # _cap_value(bk)
                df_final.at[uid, 'baseline_n'] = df_cond.loc[df.unit_id == uid][f'{band}_baseline_n'].values[idx]
                df_final.at[uid, 'sampling_n'] = df_cond.loc[df.unit_id == uid][f'{band}_sampling_n'].values[idx]
                df_final.at[uid, 'dk'] = df_cond.loc[df.unit_id == uid].dk.values[idx]

            if col_i == 0 and row_i == 0:
                showlegend = True
            else:
                showlegend = False

            # BOX PLOTS
            fig.add_trace(
                go.Box(
                    name=f'{tetrode_area}_baseline',
                    # x=col_i,
                    y=df_final.baseline_k,
                    line_color=cfg.area_colours[tetrode_area],
                    showlegend=showlegend,
                ),
                row=2*row_i+1,
                col=4*col_i+1,

            )
            fig.add_trace(
                go.Box(
                    name=f'{tetrode_area}_sampling',
                    # x=col_i,
                    y=df_final.sampling_k,
                    line_color=cfg.area_colours[tetrode_area],
                    showlegend=False,
                ),
                row=2*row_i+1,
                col=4*col_i+1,
            )

            # PIE CHARTS
            n_units_significantly_locking = len(df_final.index)
            uids = units_df.loc[units_df.tetrode_area == tetrode_area]
            n_locking = len(df_final.sampling_k)
            n_units_in_area = len(uids)
            n_not_locking = n_units_in_area - n_locking

            fig.add_trace(
                go.Pie(
                    labels=['locking', 'not locking'],
                    values=[n_locking, n_not_locking],
                    marker=dict(colors=[cfg.area_colours[tetrode_area], cfg.all_colors['gray']]),
                    textinfo='percent + value',
                    textposition='inside'
                ),
                row=2*row_i+2,
                col=col_i*4+1+j,
            )

fig.update_yaxes(range=[0, 1], tickmode='array', tickvals=[0, 1])
fig.update_xaxes(tickmode='array', tickvals=[])

for i in range(10):
    if i % 2 == 0:
        fig.update_yaxes(
            title_text=bands[int(i/2)],
            row=i+1,
            col=1,
        )

fig.update_layout(
    width=2000,
    height=2000,
)
# plotly.offline.plot(fig)
print('Saving:', savename_fig_1)
fig.write_image(savename_fig_1.as_posix())
fig.write_html(savename_fig_1.as_posix().split('.')[0] + '.html')