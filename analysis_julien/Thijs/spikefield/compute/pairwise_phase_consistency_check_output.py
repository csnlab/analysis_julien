from analysis_julien import Io
import os
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_config

savedir = lfp_config.cluster_csv_ppc

io = Io(path=lfp_config.cluster_datadir, read_lfp=True)

n_expected = 0
n_found = 0
for sid in io.session_ids:
    io.load_session(sid, read_lfp=True)

    for lid in io.lfp_clean_ids:
        for band in lfp_config.bands_def.keys():
            savename = savedir + f'/{sid}-{lid}-{band}.csv'
            n_expected += 1

            if os.path.isfile(savename):
                n_found += 1

        print(f'found: {n_found} (out of {n_expected})')