"""
STP processing pipeline:
First, on the cluster, run:
* spike_triggered_phase_1 (using csn_spike_triggered_phase_1.slurm)
* spike_triggered_phase_2 (using csn_spike_triggered_phase_2.slurm)

Then run the plot function (locally):
* plot_spike_triggered_phase

executed with ~/Thijs/slurm/csn_spike_triggered_phase_1.slurm
22/09/20
dataset: V3_WIP
"""

# from analysis_julien.Thijs.config import datadir, resultdir, unit_quality_thresholds
from analysis_julien import LightIo
from elephant.signal_processing import butter, hilbert
from elephant.phase_analysis import spike_triggered_phase
import numpy as np
import os
import pickle
from distributed import Client
from dask_jobqueue import SLURMCluster
from dask.distributed import fire_and_forget
import time


# Settings while running on cluster
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_config

datadir = lfp_config.cluster_datadir
pickledir_stp = lfp_config.cluster_pickle_stp1
cluster_scaling = 100  # nr of parallel jobs on cluster

if not os.path.isdir(pickledir_stp):
    os.makedirs(pickledir_stp)

# --------------------- DEFINITIONS ----------------------
# spike_triggered_phase frequency bands
bands = lfp_config.bands_def
min_phase_amplitude = lfp_config.min_phase_amplitude


# --------------------- COMPUTE FUNCTIONS -----------------------
def compute_stp(export_name, uid, lid, sessid):

    # try:
    io_worker = LightIo(path=datadir, read_lfp=True)
    io_worker.load_session(sessid)
    lfp = io_worker.get_object(lid)
    sp = io_worker.get_object(uid)

    if 'tetrode_area' in lfp.annotations.keys():
        tetrode_area = lfp.annotations['tetrode_area']
    else:
        tetrode_area = 'unk'

    results = dict(sp=sp, lfp_tetrode_area=tetrode_area, bands=bands, uid=uid, lid=lid, sid=sessid)
    for band in bands.keys():
        lfp_bandpassed = butter(lfp, lowpass_freq=bands[band]['highcut'], highpass_freq=bands[band]['lowcut'])
        lfp_hilbert = hilbert(lfp_bandpassed)
        phases, amps, times = spike_triggered_phase(
            hilbert_transform=lfp_hilbert,
            spiketrains=sp,
            interpolate=False,
        )
        results[f'phases_{band}'] = np.array(phases)[0]
        results[f'amps_{band}'] = np.array(amps)[0]
        results[f'times_{band}'] = np.array(times)[0]

    with open(export_name, 'wb') as fl:
        pickle.dump(results, fl, protocol=4)

    print('Saved ', export_name)

    # except Exception as e:
    #     failname = export_name.split('.')[0] + '_failed.txt'
    #     # with open(failname, 'w') as fl:
    #     traceback.print_exc(file=failname)


# --------------------- MAIN ---------------------------
io = LightIo(path=datadir, read_lfp=True)

# Setup dask
cluster = SLURMCluster(
    name='STP',
    cores=1,
    walltime='90:00:00',
    local_directory='/home/truikes/slurmoutput/STP',
    job_cpu=1,
    memory='4GB',
    job_extra=['-o /data/truikes/slurm/worker/STP-worker-%j.out', '--mem-per-cpu=4']
)
cluster.adapt(maximum_jobs=cluster_scaling)

client = Client(cluster)
tasks = []

# sessions = [session.listsessions()[0]]
sessions = io.session_ids
for sid in sessions:
    print('Starting session: ', sid)
    io.load_session(sid)
    io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    lfpframe_reref_lfps = io.lfp_clean_df

    for unit_id in io.unit_df.index:
        print(f'\t{unit_id}')

        if not io.get_meta(unit_id, 'good_unit'):
            continue

        for lfp_id in lfpframe_reref_lfps.index:

            # Create stp job
            savename = pickledir_stp + f'/{sid}_{unit_id}_{lfp_id}_stp.pkl'
            if not os.path.isfile(savename):
                args = [savename, unit_id, lfp_id, sid]
                task = client.submit(compute_stp, *args)
                # tasks.append(task)
                fire_and_forget(task)

# for task in tasks:
#     task.result()

while True:
    print(time.time())
    time.sleep(60)

print('Done!')
