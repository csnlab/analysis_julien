import plotly.graph_objects as go
import numpy as np
from analysis_julien import IO
import analysis_julien.Thijs.config as cfg
from plotly.subplots import make_subplots
import pandas as pd

savename = cfg.resultdir_quality / 'plot_celltype_classification.png'
io = IO(path=cfg.datadir, session=1)

fig = make_subplots(rows=2, cols=3, column_widths=[0.4, 0.4, 0.2],
                    subplot_titles=['Inhitory', 'Excitatory', 'population proportion',
                                    'time from peak to valley', 'firing rate', ''],
                    specs=[
                        [{'type': 'scatter'}, {'type': 'scatter'}, {'type': 'pie'}],
                        [{'type': 'scatter'}, {'type': 'scatter'}, {'type': 'scatter'}]
                    ],
                    vertical_spacing=0.2,
                    )

clr = dict(inhibitory='red', excitatory='green', unkown='gray')

# --------------------- PLOT WAVEFORMS -------------------------------
celltype_df = pd.DataFrame()
for sid in io.listsessions():
    print(sid)
    io.load_session(sid, read_lfp=False)
    io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    celltype_df = pd.concat([celltype_df, io.UnitsFrame])
    for i, uid in enumerate(io.unit_ids):
        if not io.get_meta(uid, 'good_unit'):
            continue
        wv = io.get_meta(uid, 'waveform_mean')[0]
        max_chan = np.argmax(np.max(np.abs(wv), axis=1))
        waveform = wv[max_chan, :]

        if io.get_meta(uid, 'celltype') == 'inhibitory':
            continue

        fig.add_trace(go.Scatter(
            name=uid,
            x=np.arange(0, 192) / 32,
            y=waveform.flatten() / np.max(waveform),
            marker=dict(color=cfg.celltype_colors[io.get_meta(uid, 'celltype')]),
            # legendgroup=io.get_meta(uid, 'celltype')
            showlegend=False,
        ), row=1, col=1)

for sid in io.listsessions():
    print(sid)
    io.load_session(sid, read_lfp=False)
    io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    for i, uid in enumerate(io.unit_ids):
        if not io.get_meta(uid, 'good_unit'):
            continue
        wv = io.get_meta(uid, 'waveform_mean')[0]
        max_chan = np.argmax(np.max(np.abs(wv), axis=1))
        waveform = wv[max_chan, :]

        if io.get_meta(uid, 'celltype') == 'excitatory':
            continue

        fig.add_trace(go.Scatter(
            name=uid,
            x=np.arange(0, 192) / 32,
            y=waveform.flatten() / np.max(waveform),
            marker=dict(color=cfg.celltype_colors[io.get_meta(uid, 'celltype')]),
            # legendgroup=io.get_meta(uid, 'celltype')
            showlegend=False,
        ), row=1, col=1)

fig.update_xaxes(tickmode='array', tickvals=[2.7, 3, 3.4],
                 ticktext=[-0.3, 0, 0.4], title_text='Time from peak (ms)', row=1)
fig.update_yaxes(range=[-0.7, 1], row=1)
fig.update_yaxes(tickmode='array', tickvals=[], row=1)
fig.update_xaxes(range=[2.5, 4], row=1)

# --------------------- PLOT POPULATION STATS -------------------------------
# peak to valley
fig.add_trace(go.Histogram(
    x=celltype_df.loc[celltype_df.celltype == 'inhibitory', 'peak_to_valley'] * 1000,
    marker_color=cfg.celltype_colors['inhibitory'],
    showlegend=False,
    nbinsx=50,
), row=2, col=1)
fig.add_trace(go.Histogram(
    x=celltype_df.loc[celltype_df.celltype == 'excitatory', 'peak_to_valley'] * 1000,
    marker_color=cfg.celltype_colors['excitatory'],
    showlegend=False,
    nbinsx=50,
), row=2, col=1)
fig.update_yaxes(tickmode='array', tickvals=[], row=2, col=1)
fig.update_xaxes(title_text='time (ms)', row=2, col=1)

# mean firing rate
fig.add_trace(go.Box(
    y=celltype_df.loc[celltype_df.celltype == 'inhibitory', 'firing_rate'],
    showlegend=False,
    marker_color=cfg.celltype_colors['inhibitory'],
    boxpoints=False,  # no data points
), row=2, col=2)
fig.add_trace(go.Box(
    y=celltype_df.loc[celltype_df.celltype == 'excitatory', 'firing_rate'],
    showlegend=False,
    marker_color=cfg.celltype_colors['excitatory'],
    boxpoints=False,  # no data points
), row=2, col=2)
fig.update_yaxes(range=[0, 25],
                 tickmode='array', tickvals=[0, 25], row=2, col=2)
fig.update_xaxes(tickmode='array', tickvals=[], row=2, col=2)

# pie chart
labels = ['inhibitory', 'excitatory']
values = [len(celltype_df.loc[celltype_df.celltype == 'inhibitory']),
          len(celltype_df.loc[celltype_df.celltype == 'excitatory'])]
colors = [cfg.celltype_colors['inhibitory'], cfg.celltype_colors['excitatory']]
fig.add_trace(go.Pie(
    labels=labels, values=values,
    showlegend=False,
    marker=dict(colors=colors),
), row=1, col=3)
fig.update_layout(width=1200, height=800,
                  title_text=f'Classification of clusters as Inhibitory or Excitatory <br> based on "time between peak and valley"'
                             f'(threshold: 0.4 ms)')

fig.write_image(savename.as_posix())
