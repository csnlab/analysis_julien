import analysis_julien
import quantities as qt
from plotly.colors import diverging as div
from plotly.colors import sequential as seq
sess = analysis_julien.PlotIO(path=r'C:/Temp', read_lfp=False)
sess.load_session(0, read_lfp=False)

color_C=div.RdYlGn[-2]  # color i use for correct
color_W=div.RdYlGn[1]  # color i use for wrong
marker_C = 'circle'  # marker i use for correct
marker_W = 'triangle-up'  # marker i use for wrong

obj_A_dark=div.PuOr[-2]
obj_B_dark=div.PuOr[2]

mod_M = seq.Peach[5]
mod_V = seq.PuBu[-4]
mod_T = seq.Emrld[3]

col_evmark = div.RdBu[0]
mar_evmark = 'x-thin'

c_obj_corr, p_obj_corr = sess.make_conditions(
    object={
        -1:dict(color=obj_A_dark),
        1:dict(color=obj_B_dark)
    },
    correct={
        0:dict(marker=marker_W),
        1:dict(marker=marker_C)
    }
)


markers = dict(
    retraction=dict(color=col_evmark,
                    symbol=mar_evmark)
)

bin_params = sess.get_bin_params(
    sampling_period=1*qt.ms,
    align_event='sample_start',
    binsize=100*qt.ms,
    t_start=1*qt.s,
    t_stop=2*qt.s,
    binmethod='conv',
    slide_by=10*qt.ms,
    sigma=100*qt.ms,
)

plottypes = sess.get_plot_types(
    firingrate=True,
    rasterplot=False,
)

disp_params = sess.get_display_methods(
    html=True,  # my IDE becomse slow with inline
)
# disp_params['html'] = False
# disp_params['save'] = True
disp_params['savename'] = 'Obj_and_Corr'
# disp_params['save_extension'] = '.png'


uid = sess.selectUnits(area='V2')[4]

sess.plot_response(
    unitid=uid,
    conditions=[c_obj_corr],
    plot_specs=[p_obj_corr],
    bin_params=bin_params,
    plot_types=plottypes,
    disp_params=disp_params,
    event_markers=markers,
    verbose=True
)