"""
Get a table with all the STP metrics

08/12/20
"""
from analysis_julien.Thijs import config as cfg
import pickle
from analysis_julien import LightIo
from pathlib import Path
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_cfg
import pandas as pd
import analysis_julien.Thijs.tools as tools

# Your general setup
bands = lfp_cfg.bands_def
epochs = lfp_cfg.epochs_def
modalities = lfp_cfg.modalities_def

sess = LightIo(path=cfg.datadir)
resultdir = cfg.datadir / 'results' / 'spike_triggered_phase'
stp_table_savename = resultdir / 'stp_metrics_epochs'
pickledir = resultdir / 'pickle_stp2'
# pickledir = Path(lfp_cfg.cluster_pickle_stp2)

if not resultdir.is_dir():
    resultdir.mkdir(parents=True)

stp_dataframe = pd.DataFrame()
files = [f for f in pickledir.iterdir()]
pb = tools.PBar(text='getting stp dataframe', n_counts=len(files), n_ticks=60)
sp = None

# Loop through all files in the STP_2 output directory
counter = 0
print(f'{len(files)} files found')
for file in files:
    with open(file.as_posix(), 'rb') as f:
        data = pickle.load(f)

    # Extracting meta info about this particular file
    uid = data['unit_id']
    if 'uid' not in uid:  # bug fix after uids changed
        uid = f'uid_{uid}'

    session = None
    for sid in sess.session_ids:
        if uid[4:10] in sid:
            session = sid
    assert session is not None

    # Only load the new data if needed
    if session != sess.session_id:
        sess.load_session(session=session)
        sess.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)

    if sp is not None and sp.name != uid:
        sp = sess.get_object(uid)

    # Get more meta info
    tetrode_area = sess.get_meta(uid, 'recording_group')
    group_type = file.as_posix().split('_')[-1].split('.pkl')[0]
    good_unit = sess.get_meta(uid, 'good_unit')
    nspikes = sess.get_meta(uid, 'num_spikes')

    # extract lfp id
    fname = data['input_data_filename'].split('/')[-1]
    # print(fname)
    parts = fname.split('_')
    lfp_id = f'CSC_{parts[5]}_{parts[6]}_clean'

    # try:
    lfp_area = sess.lfp_clean_df.loc[lfp_id].recording_group
    # except:
    #     print(lfp_id)
    #     print(sess.lfp_clean_df.index[:3])
    if 'raw' in data['input_data_filename']:
        state = 'raw'
    else:
        state = 'reref'

    index = f'{uid}_{lfp_id}'
    stp_dataframe.at[index, 'tetrode_area'] = tetrode_area
    stp_dataframe.at[index, 'lfp_area'] = lfp_area
    stp_dataframe.at[index, 'unit_id'] = data['unit_id']
    stp_dataframe.at[index, 'lfp_id'] = lfp_id
    stp_dataframe.at[index, 'animal_id'] = sess.animal_id
    stp_dataframe.at[index, 'session_id'] = sess.session_id
    stp_dataframe.at[index, 'good_unit'] = good_unit

    if group_type == 'epoch':
        for band in bands.keys():
            for epoch in epochs.keys():
                stp_dataframe.at[index, f'{band}_{epoch}_p'] = data[f'{band}_{epoch}_pval']
                stp_dataframe.at[index, f'{band}_{epoch}_k'] = data[f'{band}_{epoch}_kappa_astropy']
                stp_dataframe.at[index, f'{band}_{epoch}_n'] = data[f'{band}_{epoch}_n_counts']
    elif group_type == 'modality':
        for band in bands.keys():
            for modality in modalities.keys():
                stp_dataframe.at[index, f'{band}_{modality}_p'] = data[f'{band}_{modality}_pval']
                stp_dataframe.at[index, f'{band}_{modality}_k'] = data[f'{band}_{modality}_kappa_astropy']
                stp_dataframe.at[index, f'{band}_{modality}_n'] = data[f'{band}_{modality}_n_counts']

    pb.print_1()
    # if pb.count % 500 == 0:
    #     stp_dataframe.to_csv(f'{stp_table_savename}_{counter}.csv')
    #     counter += 1
    #     stp_dataframe = pd.DataFrame()
    #     print('saving')

stp_dataframe.to_csv(f'{stp_table_savename}_{counter}.csv')

# df = pd.DataFrame()
# for file in resultdir.iterdir():
#     if 'stp_metrics_epochs' in file.name:
#         dfloop = pd.read_csv(file.as_posix(), index_col=0)
#         df = pd.concat([df, dfloop])
#
# df.to_csv(f'{stp_table_savename}_all.csv')