"""
For all units in the dataset, plot their intra areal locking values on theta. Also plot the spike counts in
a separate figure.
"""


import pandas as pd
import analysis_julien.Thijs.config as cfg
import numpy as np
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import analysis_julien.Thijs.spike_triggered_phase.spike_triggered_phase_config as lfp_cfg


def _cap_value(val):
    if val > 1:
        val = 1
    elif val < - 1:
        val = -1
    return val


savename_fig_1 = cfg.resultdir / 'spike_triggered_phase' / 'spc_epochs' / 'inter_areal_locking_kappa'
if not savename_fig_1.parent.is_dir():
    savename_fig_1.parent.mkdir()

table = cfg.datadir / 'results' / 'spike_triggered_phase' / 'stp_metrics_epochs_0.csv'  # from stp_get_df
bands = lfp_cfg.bands_def.keys()

# area_sets = [
#     ('HPC', ('VIS', 'BR', 'PRH')),
#     ('BR', ('VIS', 'BR', 'HPC', 'PRH'))
# ]


df = pd.read_csv(table, header=0, index_col=0)
plotnames = ['Baseline', 'Sampling', 'contrast']
areas = df.tetrode_area.unique()


for band in bands:
    if band == 'delta':
        continue

    savename_fig_1 = cfg.resultdir / 'spike_triggered_phase' / 'spc_epochs' / f'inter_areal_locking_kappa_{band}'

    subplot_titles = []
    for pname in plotnames:
        for lfp_area in areas:
            subplot_titles.append(f'LFP: {lfp_area}, during {pname}')

    fig = make_subplots(rows=3, cols=4,
                        subplot_titles=subplot_titles,
                        vertical_spacing=0.1,
                        )

    # Do the same trick for each area
    for i, lfp_area in enumerate(areas):

        # Filter units
        for unit_area in areas:
            df_cond = df.loc[
                (df.tetrode_area == unit_area) &  # lfp must be same area a tetrode
                (df.lfp_area == lfp_area) &
                (df[f'{band}_sampling_n'] > 50) &  # at least n spikes in baseline and sampling epochs
                (df[f'{band}_baseline_n'] > 50) &
                (df[f'{band}_sampling_p'] < 0.05) &  # only include units which showed significant locking during sampling
                (df.good_unit)  # and only good units!
            ].copy()

            # compute 'delta kappa', cap at 1 for visualization purposes
            for index, r in df_cond.iterrows():
                dk = r[f'{band}_sampling_k'] - r[f'{band}_baseline_k']
                df_cond.at[index, 'dk'] = _cap_value(dk)

            # Remove weird data points
            df_cond = df_cond.loc[
                (~np.isinf(df_cond.dk)) &
                (pd.notna(df_cond.dk))
            ]

            # for every remaining unit compute the stats to visualize
            df_final = pd.DataFrame()
            for uid in df_cond.unit_id.unique():
                idx = df_cond.loc[df.unit_id == uid].dk.argmax()
                uid_df = df_cond.loc[df.unit_id == uid]
                # Cap at 1 and -1 for visualization purposes
                sk = uid_df[f'{band}_sampling_k'].values[idx]
                bk = uid_df[f'{band}_baseline_k'].values[idx]
                df_final.at[uid, 'sampling_k'] = _cap_value(sk)
                df_final.at[uid, 'baseline_k'] = _cap_value(bk)
                # df_final.at[uid, 'baseline_n'] = df_cond.loc[df.unit_id == uid].theta_baseline_n.values[idx]
                # df_final.at[uid, 'sampling_n'] = df_cond.loc[df.unit_id == uid].theta_sampling_n.values[idx]
                df_final.at[uid, 'dk'] = df_cond.loc[df.unit_id == uid].dk.values[idx]

            # also get the 'delta spikecounts' per epoch
            # df_final['dn'] = df_final.sampling_n - df_final.baseline_n

            # Figure 1: kappa in baseline, kappa in sampling period, delta kappa
            if i == 0:
                showlegend = True
            else:
                showlegend = False

            # fig.add_trace(
            #     go.Violin(
            #         name=unit_area,
            #         x=df_final.baseline_k,
            #         line_color=cfg.area_colours[unit_area],
            #         box_visible=True,
            #         showlegend=showlegend,
            #         scalemode='count',
            #     ),
            #     row=1,
            #     col=i+1,
            # )
            fig.add_trace(
                go.Histogram(
                    name=unit_area,
                    x=df_final.baseline_k,
                    opacity=0.6,
                    marker=dict(color=cfg.area_colours[unit_area]),
                    legendgroup=unit_area,
                    showlegend=showlegend,
                    xbins=dict(
                        start=0,
                        end=1,
                        size=0.05,
                    ),
                    autobinx=False,
                ),
                row=1,
                col=i+1,
            )
            # fig.add_trace(
            #     go.Violin(
            #         name=unit_area,
            #         x=df_final.sampling_k,
            #         line_color=cfg.area_colours[unit_area],
            #         box_visible=True,
            #         showlegend=False,
            #         scalemode='count',
            #     ),
            #     row=2,
            #     col=i+1,
            # )
            fig.add_trace(
                go.Histogram(
                    name=unit_area,
                    x=df_final.sampling_k,
                    opacity=0.6,
                    marker=dict(color=cfg.area_colours[unit_area]),
                    legendgroup=unit_area,
                    showlegend=False,
                    xbins=dict(
                        start=0,
                        end=1,
                        size=0.05,
                    ),
                    autobinx=False,
                ),
                row=2,
                col=i+1,
            )
            # fig.add_trace(
            #     go.Violin(
            #         name=unit_area,
            #         x=df_final.dk,
            #         line_color=cfg.area_colours[unit_area],
            #         box_visible=True,
            #         showlegend=False,
            #         scalemode='count'
            #     ),
            #     row=3,
            #     col=i+1,
            # )
            # fig.add_trace(
            #     go.Histogram(
            #         name=unit_area,
            #         x=df_final.dk,
            #         opacity=0.6,
            #         marker=dict(color=cfg.area_colours[unit_area]),
            #         legendgroup=unit_area,
            #         showlegend=False,
            #     ),
            #     row=3,
            #     col=i+1,
            # )


    # Style Figure 1
    fig.update_yaxes(
        range=[0, 20],
        tickmode='array',
        tickvals=[0, 20],
        title_text='unit count'
    )
    # fig.update_traces(orientation='h',
    #                    side='positive',
    #                    width=5,
    #                    points=False,
    #                    meanline_visible=True,
    #                    # scalemode='width'
    #                   )

    fig.update_xaxes(
        title_text='kappa',
        range=[0, 1]
    )

    # for i in range(1, 5):
    #     fig.update_xaxes(
    #         title_text='delta Kappa',
    #         row=3, col=i,
    #         # range=[-0.5, 1]
    #     )

    fig.update_layout(
        title_text=f'Spike triggered phase during epochs (Baseline and Sampling) on {band}<br>',
        xaxis_showgrid=False,
        xaxis_zeroline=False,
        width=1800,
        height=1200,
        barmode='overlay',

        # scalemode='normalized'
    )

    print('Saving:', savename_fig_1)
    plotly.offline.plot(fig, filename=f'{savename_fig_1}.html')
    fig.write_image(f'{savename_fig_1}.png')
