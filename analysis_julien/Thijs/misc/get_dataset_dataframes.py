import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import analysis_julien.Thijs.tools as tools
from analysis_julien import Io
import analysis_julien.Thijs.config as cfg
import pandas as pd

units_df_savename = cfg.resultdir / 'units_df.csv'
trials_df_savename = cfg.resultdir / 'trials_df.csv'

io = Io(cfg.datadir)

pbar = tools.PBar('gettings dfs', 40, len(io.session_ids), n_decimals=1)
units_df = pd.DataFrame()
trials_df = pd.DataFrame()


for sid in io.session_ids:
    io.load_session(sid)
    units_df = pd.concat([units_df, io.unit_df])
    trials_df = pd.concat([trials_df])
    pbar.print_1()

pbar.close()


units_df.to_csv(units_df_savename)