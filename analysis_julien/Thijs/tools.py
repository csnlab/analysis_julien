import pickle
import analysis_julien.Thijs.config as cfg
import numpy as np
import math


def _get_fullpath(name):
    return cfg.dumpdir / f'{name}.pkl'


def load_data(name):
    fullname = _get_fullpath(name)
    if cfg.RESET_DUMP:
        return None
    elif not fullname.is_file():
        return None
    else:
        with open(fullname, 'rb') as f:
            return pickle.load(f)


def save_data(data, name):
    fullname = _get_fullpath(name)
    if not cfg.dumpdir.is_dir():
        cfg.dumpdir.mkdir(parents=True)
    with open(fullname, 'wb') as f:
        pickle.dump(data, f, protocol=4)


def find_times_idx(times, t0, t1):
    t0 = t0.rescale('s').magnitude
    t1 = t1.rescale('s').magnitude
    times = times.rescale('s').magnitude
    idx = np.where((times >= t0) & (times <= t1))[0]
    return idx


def find_array_idx(array, value_0=None, value_1=None):
    idx = np.where((array >= value_0) & (array <= value_1))[0]
    return idx


def hex_to_rgb(h, opacity=0.3):
    res = tuple(int(h[i:i+2], 16) for i in (0, 2, 4))
    return f'rgba({res[0]},{res[1]}, {res[2]}, {opacity})'


class PBar:
    def __init__(self, text, n_ticks, n_counts, n_decimals=0):
        self.text = text
        self.n_ticks = n_ticks
        self.n_counts = n_counts
        self.n_chars = len(text) + n_ticks
        self.count = 0
        self.n_decimals = n_decimals

    def print(self, count):
        self.count = count
        perc = self.count/self.n_counts
        tick = math.ceil(perc * self.n_ticks)
        pstr = '\r' + self.text + ': |'

        for _ in range(tick):
            pstr += '+'
        for _ in range(self.n_ticks-tick):
            pstr += '_'

        perc_print = f'{perc * 100:.20f}'
        pstr += '|' + f' {perc_print[:self.n_decimals+1]} %'
        print(pstr, end='')

    def print_1(self):
        self.print(self.count + 1)

    def close(self):
        self.print(count=self.n_counts)
        print('')
