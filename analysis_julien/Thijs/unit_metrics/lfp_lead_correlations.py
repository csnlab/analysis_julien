"""
Visualize correlations between tetrode leads. Outliers indicate noisyness
02/10/20
"""
from analysis_julien import IO
from elephant.signal_processing import butter
import neo
from scipy import stats
import numpy as np
import plotly
import plotly.graph_objects as go
import pandas as pd
import quantities as qt
from plotly.colors import sequential as seq
import analysis_julien.Thijs.config as cfg
import analysis_julien.Thijs.tools as tools

sample_t0 = 300 * qt.s
sample_t1 = 1800 * qt.s
lowpass_freq = 160


def _check_channel(channel):
    if not isinstance(channel, neo.AnalogSignal):
        raise TypeError('Channel must be a Neo.AnalogSignal')


def _check_neighbouring_channels(neighbouring_channels):
    if not isinstance(neighbouring_channels, list):
        raise TypeError('Neighbouring_channels must by a list of Neo.Analogsignals')
    elif len(neighbouring_channels) == 0:
        raise


def _get_flattend_data(channel, neighbouring_channels):
    # Get channel data as array
    channel_data = channel.as_array().flatten()

    # Get neigbouring channel data as array
    neigh_ch_data = []
    if neighbouring_channels is not None:
        for s in neighbouring_channels:
            neigh_ch_data.append(s.as_array().flatten())
        neigh_ch_data = np.vstack(neigh_ch_data)
    else:
        neigh_ch_data = None
    return channel_data, neigh_ch_data


def correlation_coefficient(channel, neighbouring_channels):
    # Check if input was correct
    _check_channel(channel)
    _check_neighbouring_channels(neighbouring_channels)

    n_neighbours = len(neighbouring_channels)  # Number of neighbouring channels

    # Get numpy arrays
    chan_flat, neigh_flat = _get_flattend_data(channel, neighbouring_channels)

    # Compute correlation against all neighbouring channels
    correlation_sum = 0
    for il in range(0, neigh_flat.shape[0]):
        rr, _ = stats.pearsonr(chan_flat, neigh_flat[il, :])
        correlation_sum += rr

    # Average across leads
    ccr = correlation_sum / n_neighbours
    return ccr


io = IO(path=cfg.datadir)
res = pd.DataFrame()

print('Computing lead correlations...')
sessions = ['rat-16-181013']
# sessions = io.listsessions()
for sid in sessions:
    io.load_session(sid)

    progbar = tools.PBar(text=f'{sid} getting correlations', n_ticks=40, n_counts=len(io.LFPFrame.tetrode.unique()))

    for ii, tetrode in enumerate(io.LFPFrame.tetrode.unique()):
        lfp_ids = io.selectLFPS(tetrode=tetrode)

        for i, l0 in enumerate(lfp_ids):
            lfp0 = io.get_lfps(l0, t_start=sample_t0, t_stop=sample_t1)

            # Lowpass filter as some 'raw' channels are allready lowpass filtered while others are not
            lfp0_filt = butter(lfp0, lowpass_freq=lowpass_freq)
            for j, l1 in enumerate(lfp_ids):
                if j > i:
                    lfp1 = io.get_lfps(l1, t_start=sample_t0, t_stop=sample_t1)
                    lfp1_filt = butter(lfp1, lowpass_freq=lowpass_freq)

                    cc = correlation_coefficient(lfp0_filt, [lfp1_filt])

                    sint = sid.split("-")[-1][-2:]
                    match_id = f'{sint}_{tetrode}{l0.split(tetrode)[1]}{l1.split(tetrode)[1]}'
                    match_id_rev = f'{sint}_{tetrode}{l1.split(tetrode)[1]}{l0.split(tetrode)[1]}'

                    res.at[match_id, 'cc'] = np.abs(cc)
                    res.at[match_id, 'tetrode'] = tetrode
                    res.at[match_id, 'session'] = sint
                    res.at[match_id, 'this_ch'] = l0[-1]
                    res.at[match_id, 'other_ch'] = l1[-1]
                    res.at[match_id, 'htext'] = f'sess {res.at[match_id, "session"]}: ' \
                                                f'ch {res.at[match_id, "this_ch"]} & ch {res.at[match_id, "other_ch"]}'

                    # Add inverted correlations for visualization purposes
                    res.at[match_id_rev, 'cc'] = np.abs(cc)
                    res.at[match_id_rev, 'tetrode'] = tetrode
                    res.at[match_id_rev, 'session'] = sid.split("-")[-1][-2:]
                    res.at[match_id_rev, 'this_ch'] = l1[-1]
                    res.at[match_id_rev, 'other_ch'] = l0[-1]
                    res.at[match_id_rev, 'htext'] = f'sess {res.at[match_id_rev, "session"]}: ' \
                                                    f'ch {res.at[match_id_rev, "this_ch"]} & ' \
                                                    f'ch {res.at[match_id_rev, "other_ch"]}'
        progbar.print(ii)
    progbar.close()

data = []
cols = {
    '1': seq.ice[8],
    '2': seq.solar[6],
    '3': seq.Peach[4],
    '4': seq.Darkmint[-1],
}

opacity = '0.8'
for key in cols.keys():
    flatcol = cols[key]
    cols[key] = f'rgba({flatcol.split("(")[1].split(")")[0]}, {opacity})'

for ch in res.this_ch.unique():
    res_plot = res.loc[res.this_ch == ch]
    data.append(go.Scatter(
        x=res_plot.tetrode,
        y=res_plot.cc,
        mode='markers',
        hovertext=res_plot.htext,
        marker=dict(
            color=cols[ch],
            line=dict(
                color='DarkSlateGrey',
                width=1,
            ),
            size=10,
        ),
        name=f'ch {ch}'
    ))

fig = go.Figure(
    data=data,
)
fig.update_layout(title=f'Correlation between channels on tetrode (t0: {sample_t0.magnitude:.0f}s, t1: '
                        f'{sample_t1.magnitude:.0f}s)')

resultdir = cfg.resultdir_quality
if not resultdir.is_dir():
    resultdir.mkdir(parents=True)

savename = resultdir / f'lfp_lead_correlations_{sample_t0.magnitude:.0f}_{sample_t1.magnitude:.0f}.html'
# plotly.offline.plot(fig, filename=savename.as_posix())
print('Saved: ', savename)
# fig.show()
fig.write_html(savename.as_posix())
