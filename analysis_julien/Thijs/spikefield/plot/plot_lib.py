import unicodedata
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np
from analysis_julien.Thijs.tools import hex_to_rgb
from analysis_julien.Thijs import config as cfg
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as stp_cfg
import pickle
from analysis_julien import LightIo
from pathlib import Path
import pandas as pd
import analysis_julien.Thijs.spikefield.spike_triggered_phase_config as lfp_config

resultdir = Path(cfg.resultdir) / 'spikefield'


def bin_phases(phases, amps, nb=25):
    counts_per_bin = np.empty(nb)
    bin_edges = np.linspace(-np.pi, np.pi, num=nb + 1, endpoint=True)
    bin_centres = np.array([(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(nb)])
    if phases is None:
        return bin_centres

    # amp_mask = np.where(amps >= min_phase_amplitude)[0]
    # phases_masked = phases[amp_mask]

    # for i in range(nb):
    #     idx = find_array_idx(phases_masked, bin_edges[i], bin_edges[i + 1])
    #     counts_per_bin[i] = len(idx)

    return bin_centres, counts_per_bin


# nbins = 25  # nr bins for phase histogram
# bin_centres = bin_phases(None, None, nb=nbins)
epoch_colours = cfg.epoch_colours
modality_colours = cfg.modality_colors
# bin_centres = np.append(bin_centres, bin_centres[0])

pi_str = unicodedata.lookup("GREEK SMALL LETTER PI")


def plot_spc_modalities(plotdata, spiketrain, lfpinfo, exportname):
    subplot_titles = []
    column_widths = []
    band_axes = dict()
    row, col = 1, 1
    bands = list(stp_cfg.bands_def.keys())

    for i, b in enumerate(bands):
        subplot_titles.extend([f'{b} ({stp_cfg.bands_def[b]["lowcut"]}-{stp_cfg.bands_def[b]["highcut"]})', ''])

        if row == 1:
            column_widths.extend([0.35, 0.15])
        band_axes[b] = dict(row=row, col=col)
        col += 2
        if col > 6:
            row += 1
            col = 1

    modalities = ['T', 'V', 'M', 'P']

    fig = make_subplots(rows=2, cols=6, column_widths=column_widths,
                        subplot_titles=subplot_titles,
                        specs=[
                            [{'type': 'polar'}, {'type': 'table'}, {'type': 'polar'}, {'type': 'table'},
                             {'type': 'polar'}, {'type': 'table'}],
                            [{'type': 'polar'}, {'type': 'table'}, {'type': 'polar'}, {'type': 'table'},
                             {'type': 'polar'}, {'type': 'table'}]

                        ],
                        horizontal_spacing=0,
                        vertical_spacing=0,
                        )

    ticker = 1
    for band_nr, band in enumerate(bands):
        if band_nr == 0:
            showlegend = True
        else:
            showlegend = False

        table_kappa_astro, table_mean, table_p, table_row_colors = [], [], [], []
        table_kappa_scipy = []

        for modality in modalities:
            binned_phases = plotdata[f'{band}_{modality}_phases_binned']
            binned_phases = np.append(binned_phases, binned_phases[0])
            binned_phases = binned_phases / np.max(binned_phases)

            bin_centres = bin_phases(None, None, nb=lfp_config.nbins)
            bin_centres = np.append(bin_centres, bin_centres[0])

            fig.add_trace(

                go.Scatterpolar(
                    name=modality,
                    theta=bin_centres.flatten(),
                    thetaunit='radians',
                    r=binned_phases,
                    subplot=f'polar{ticker}',
                    showlegend=showlegend,
                    fill='toself',
                    mode='lines',
                    fillcolor=hex_to_rgb(modality_colours[modality][1:], 0.1),
                    line=dict(color=hex_to_rgb(modality_colours[modality][1:], 1),
                              width=2)
                ),
                row=band_axes[band]['row'],
                col=band_axes[band]['col']
            )
            ticker += 1
            table_p.append(plotdata[f'{band}_{modality}_pval'])
            table_kappa_astro.append(plotdata[f'{band}_{modality}_kappa_astropy'])
            table_kappa_scipy.append(plotdata[f'{band}_{modality}_kappa_scipy'])

            table_row_colors.append(hex_to_rgb(modality_colours[modality][1:], 0.5))

        table_p = [f'{tp:.03f}' for tp in table_p]
        if None not in table_kappa_astro:
            table_kappa_astro = [f'{tk:.2f}' for tk in table_kappa_astro]
        if None not in table_kappa_scipy:
            table_kappa_scipy = [f'{tk:.2f}' for tk in table_kappa_scipy]

        fig.add_table(
            columnwidth=[5, 5, 5],
            header=dict(values=['kappa sp', 'kappa astro', 'p'],
                        fill_color=['grey']),
            cells=dict(
                values=[table_kappa_scipy, table_kappa_astro, table_p],
                fill_color=[table_row_colors, table_row_colors, table_row_colors],
            ),

            row=band_axes[band]['row'], col=band_axes[band]['col'] + 1
        )
    for i in [1, 3, 5]:
        fig.update_yaxes(
            tickmode='array',
            tickvals=[],
            row=1, col=i,
        )
        fig.update_yaxes(
            tickmode='array',
            tickvals=[],
            row=2, col=i,
        )
        fig.update_xaxes(
            tickmode='array',
            tickvals=[bin_centres[1], 0, bin_centres[-2]],
            ticktext=[f'-{pi_str}', '0', f'{pi_str}']
        )

    for i in range(1, ticker):
        specs = {f'polar{i}': dict(
            angularaxis=dict(
                type='linear',
                thetaunit='radians',
                dtick=0.5 * np.pi,
            ),
            radialaxis=dict(
                showticklabels=False,
            )
        )}
        fig.update_layout(
            **specs
        )
    fig.update_layout(
        title_text=f'unit :{spiketrain.name} (area: {spiketrain.annotations["tetrode_area"]}, '
                   f'good unit: {spiketrain.annotations["good_unit"]}, nspikes: {len(spiketrain)})<br>'
                   f'lfp: {plotdata["lfp_id"]} (area: {lfpinfo.tetrode_area})',
        width=1800,
        height=1200,
    )

    fig.write_image(exportname)
    print('Saved:', exportname)



def plot_spc_epochs(plotdata, spiketrain, lfpinfo, exportname):
    bands = list(stp_cfg.bands_def.keys())
    subplot_titles = []
    column_widths = []
    band_axes = dict()
    row, col = 1, 1
    for i, b in enumerate(bands):
        subplot_titles.extend([f'{b} ({stp_cfg.bands_def[b]["lowcut"]}-{stp_cfg.bands_def[b]["highcut"]})', ''])

        if row == 1:
            column_widths.extend([0.35, 0.15])
        band_axes[b] = dict(row=row, col=col)
        col += 2
        if col > 6:
            row += 1
            col = 1

    epochs = ['baseline', 'sampling', 'response', 'reward']

    fig = make_subplots(rows=2, cols=6, column_widths=column_widths,
                        subplot_titles=subplot_titles,
                        specs=[
                            [{'type': 'polar'}, {'type': 'table'}, {'type': 'polar'}, {'type': 'table'},
                             {'type': 'polar'}, {'type': 'table'}],
                            [{'type': 'polar'}, {'type': 'table'}, {'type': 'polar'}, {'type': 'table'},
                             {'type': 'polar'}, {'type': 'table'}]

                        ],
                        horizontal_spacing=0,
                        vertical_spacing=0,
                        )

    ticker = 1
    for band_nr, band in enumerate(bands):
        if band_nr == 0:
            showlegend = True
        else:
            showlegend = False

        table_kappa_astro, table_mean, table_p, table_row_colors = [], [], [], []
        table_kappa_scipy, table_n = [], []
        for epoch in epochs:
            binned_phases = plotdata[f'{band}_{epoch}_phases_binned']
            binned_phases = np.append(binned_phases, binned_phases[0])
            binned_phases = binned_phases / np.max(binned_phases)

            bin_centres = bin_phases(None, None, nb=lfp_config.nbins)
            bin_centres = np.append(bin_centres, bin_centres[0])

            fig.add_trace(

                go.Scatterpolar(
                    name=epoch,
                    theta=bin_centres.flatten(),
                    thetaunit='radians',
                    r=binned_phases.flatten(),
                    subplot=f'polar{ticker}',
                    showlegend=showlegend,
                    fill='toself',
                    mode='lines',
                    fillcolor=hex_to_rgb(epoch_colours[epoch][1:], 0.1),
                    line=dict(color=hex_to_rgb(epoch_colours[epoch][1:], 1),
                              width=2)
                ),
                row=band_axes[band]['row'],
                col=band_axes[band]['col']
            )
            ticker += 1
            table_p.append(plotdata[f'{band}_{epoch}_pval'])
            table_kappa_astro.append(plotdata[f'{band}_{epoch}_kappa_astropy'])
            table_kappa_scipy.append(plotdata[f'{band}_{epoch}_kappa_scipy'])
            table_row_colors.append(hex_to_rgb(epoch_colours[epoch][1:], 0.5))
            table_n.append(plotdata[f'{band}_{epoch}_phases'].shape)

        table_p = [f'{tp:.03f}' for tp in table_p]
        if None not in table_kappa_scipy:
            table_kappa_scipy = [f'{tk:.2f}' for tk in table_kappa_scipy]
        if None not in table_kappa_astro:
            table_kappa_astro = [f'{tk:.2f}' for tk in table_kappa_astro]

        fig.add_table(
            columnwidth=[5, 5, 5, 5],
            header=dict(values=['kappa sp', 'kappa ast', 'p', 'n'],
                        fill_color=['grey']),
            cells=dict(
                values=[table_kappa_scipy, table_kappa_astro, table_p, table_n],
                fill_color=[table_row_colors, table_row_colors, table_row_colors, table_row_colors],
            ),

            row=band_axes[band]['row'], col=band_axes[band]['col'] + 1
        )
    for i in [1, 3, 5]:
        fig.update_yaxes(
            tickmode='array',
            tickvals=[],
            row=1, col=i,
        )
        fig.update_yaxes(
            tickmode='array',
            tickvals=[],
            row=2, col=i,
        )
        fig.update_xaxes(
            tickmode='array',
            tickvals=[bin_centres[1], 0, bin_centres[-2]],
            ticktext=[f'-{pi_str}', '0', f'{pi_str}']
        )

    for i in range(1, ticker):
        specs = {f'polar{i}': dict(
            angularaxis=dict(
                type='linear',
                thetaunit='radians',
                dtick=0.5 * np.pi,
            ),
            radialaxis=dict(
                showticklabels=False,
            )
        )}
        fig.update_layout(
            **specs
        )
    fig.update_layout(
        title_text=f'unit :{spiketrain.name} (area: {spiketrain.annotations["tetrode_area"]}, '
                   f'good unit: {spiketrain.annotations["good_unit"]}, nspikes: {len(spiketrain)})<br>'
                   f'lfp: {plotdata["lfp_id"]} (area: {lfpinfo.tetrode_area})',
        width=1800,
        height=1200,
    )

    fig.write_image(exportname)
    print('Saved:', exportname)


def plot_population_spc_epochs(overview_df_filename):
    df = pd.read_csv(overview_df_filename)
    epochs_def = stp_cfg.epochs_def
    bands_def = stp_cfg.bands_def
    p_cutoff = 0.0000001

    epochs = list(epochs_def.keys())

    epoch_colors = cfg.epoch_colours
    df.at[0, 'uid_nr'] = None
    df.loc[df['k'] > 2, 'k'] = 2
    for tetrode_area in df.tetrode_area.unique():
        for phase in bands_def.keys():

            fig = make_subplots(
                rows=4,
                subplot_titles=[f'lfp area: {a}' for a in df.lfp_area.unique()],
                cols=1
            )

            for i, lfp_area in enumerate(df.lfp_area.unique()):
                if i == 0:
                    showlegend = True
                else:
                    showlegend = False

                for epoch in epochs:
                    df_filter = df[(df['tetrode_area'] == tetrode_area) &
                                   # (df['good_unit'] == True) &
                                   (df['band'] == phase) &
                                   (df['lfp_area'] == lfp_area) &
                                   (df['epoch'] == epoch) &
                                   (df['state'] == 'reref')  # &
                                   # (df['p'] <= p_cutoff)
                                   ].copy()
                    uid_map = df_filter.unit_id.unique()

                    for idx, r in df_filter.iterrows():
                        df_filter.loc[idx, 'uid_nr'] = np.where(uid_map == r.unit_id)[0]

                    fig.add_trace(go.Scatter(
                        mode='markers',
                        hovertext=df_filter.unit_id,
                        marker=dict(color=epoch_colors[epoch], size=5),
                        x=df_filter.uid_nr,
                        y=df_filter.k,
                        name=epoch,
                        showlegend=showlegend,
                    ), row=1 + i, col=1)

            fig.update_xaxes(
                tickmode='array',
                tickvals=[],
            )
            fig.update_yaxes(
                range=[0, 2]
            )
            fig.update_layout(
                width=1000,
                height=2000,
                title_text=f'Spike Phase Locking of units in {tetrode_area} on {phase} per brain area, per epoch (p={p_cutoff})'
            )

            savename_png = cfg.resultdir + f'/spikefield/population_stp_p_{tetrode_area}_{phase}.png'
            savename_html = cfg.resultdir + f'/spikefield/population_stp_p_{tetrode_area}_{phase}.html'

            fig.write_image(savename_png)
            fig.write_html(savename_html)
            print('Saved:', savename_html)


def get_plot_args(pickledir, u, lfpid, grouptype):
    fl = f'{u}-{lfpid}-clean_{grouptype}'
    with open((pickledir / fl).as_posix(), 'rb') as f:
        data = pickle.load(f)
    uid = data['unit_id']
    sid = None
    io = LightIo(path=cfg.datadir)
    for test_id in io.session_ids:
        if uid[:6] in test_id:
            sid = test_id
    assert sid is not None
    io.load_session(sid)
    io.set_unit_criteria(**cfg.unit_quality_thresholds, verbose=False)
    sp = io.get_object(f'uid_{u}')
    group_type = data['input_data_filename'].split('_')[-1]

    # extract lfp id
    fname = data['input_data_filename'].split('/')[-1]
    parts = fname.split('_')
    lfp_id = f'CSC_{parts[5]}_{parts[6]}_clean'
    savename = Path(cfg.resultdir) / 'spikefield' / 'spc_epochs' / f'{uid}-{lfp_id}.png'
    if not savename.parent.is_dir():
        savename.parent.mkdir(parents=True)
    return data, sp, io.lfp_clean_df.loc[lfp_id], savename.as_posix()


if __name__ == '__main__':
    printdir_modality = resultdir / 'spc_modality'
    printdir_epochs = resultdir / 'spc_epochs'
    pickledir = resultdir / 'pickle_stp2'
    stp_table_savename = resultdir / 'stp_metrics_epochs.csv'

    # testfile = list(pickle_readdir.iterdir())[0]
    epoch_files = [file for file in pickledir.iterdir() if 'epoch' in file.as_posix()]
    plot_args = get_plot_args(pickledir, '1810130001', 'Hpc1', 'epoch')

    plot_spc_epochs(*plot_args)

    # mod_files = [file for file in pickle_readdir.iterdir() if 'modality' in file.as_posix()]
    # with open(mod_files[10], 'rb') as f:
    #     data = pickle.load(f)
    # uid = data['unit_id']
    # sid = None
    # io = LfpIO(path=cfg.datadir)
    # for test_id in io.listsessions():
    #     if uid[:6] in test_id:
    #         sid = test_id
    # io.load_session(sid)
    # sp = io.get_by_names(uid)
    # group_type = data['input_data_filename'].split('_')[-1]
    # lfp_id = data['input_data_filename'].split(data['unit_id'])[1].split(data['lfp_state'])[0][1:-1]
    # plot_spc_modalities(data, sp, io.LFPFrame.loc[lfp_id], resultdir + '/test_modality.png')
