from setuptools import setup

setup(
    name='Analysis_julien',
    version='',
    packages=['SP3_UC003', 'analysis_julien', 'analysis_julien.core', 'analysis_julien.Test', 'analysis_julien.Julien',
              'analysis_julien.pietro', 'analysis_julien.pietro.pca', 'analysis_julien.pietro.tca',
              'analysis_julien.pietro.other', 'analysis_julien.pietro.decode'],
    url='',
    license='',
    author='Thijs',
    author_email='',
    description=''
)
